package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"ibron/models"
	"ibron/pkg/helper"
	"ibron/pkg/logger"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type bannerRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewBannerRepo(db *pgxpool.Pool, log logger.LoggerI) *bannerRepo {
	return &bannerRepo{
		db:  db,
		log: log,
	}
}

func (u *bannerRepo) Create(ctx context.Context, req *models.BannerCreate) (*models.Banner, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "banner"(id, service_id,url)
		VALUES ($1, $2, $3) 
	`

	if _, err := u.db.Exec(ctx, query,
		id,
		req.ServiceId,
		req.Url,
	); err != nil {
		u.log.Error("insert error" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.BannerPrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id banner" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *bannerRepo) GetByID(ctx context.Context, req *models.BannerPrimaryKey) (*models.Banner, error) {
	var (
		query string

		id         sql.NullString
		service_id sql.NullString
		url        sql.NullString
		createdAt  sql.NullString
		updatedAt  sql.NullString
	)

	query = `
		SELECT 
			id,
			service_id, 
			url,
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "banner"
		WHERE id = $1

	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&service_id,
		&url,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		u.log.Error("error while scanning banner data" + err.Error())
		return nil, err
	}

	return &models.Banner{
		Id:        id.String,
		ServiceId: service_id.String,
		Url:       url.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}, nil
}

func (u *bannerRepo) GetList(ctx context.Context, req *models.BannerGetListRequest) (*models.BannerGetListResponse, error) {
	var (
		resp   = &models.BannerGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			service_id,
			url,
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "banner"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting banner list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			service_id sql.NullString
			url        sql.NullString
			createdAt  sql.NullString
			updatedAt  sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&service_id,
			&url,
			&createdAt,
			&updatedAt,
		)
		if err != nil {
			u.log.Error("error is while getting banner list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Banner = append(resp.Banner, &models.Banner{
			Id:        id.String,
			ServiceId: service_id.String,
			Url:       url.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}
	return resp, nil
}

func (u *bannerRepo) Update(ctx context.Context, req *models.BannerUpdate) (int64, error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"banner"
		SET
			service_id = :service_id,
			url = :url
			updated_at = NOW()
		WHERE id = :id
	`
	params = map[string]interface{}{
		"id":         req.Id,
		"service_id": req.ServiceId,
		"url":        req.Url,
	}

	query, args := helper.ReplaceQueryParams(query, params)
	result, err := u.db.Exec(ctx, query, args...)
	if err != nil {
		u.log.Error("error is while updating banner data", logger.Error(err))
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (u *bannerRepo) Delete(ctx context.Context, req *models.BannerPrimaryKey) error {

	_, err := u.db.Exec(ctx, `DELETE FROM banner  WHERE id = $1`, req.Id)
	if err != nil {
		u.log.Error("error is while deleting banner", logger.Error(err))
		return err
	}

	return nil
}
