package handler

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"

	"ibron/models"
	"ibron/pkg/helper"
)

// Create Users godoc
// @ID create_users
// @Router /ibron/api/v1/user [POST]
// @Summary Create Users
// @Description Create Users
// @Tags User
// @Accept json
// @Procedure json
// @Param user body models.UserCreate true "CreateUserRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateUser(c *gin.Context) {
	var (
		createUser models.UserCreate
		resp       *models.User
	)

	err := c.ShouldBindJSON(&createUser)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error User Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	if !helper.IsValidPhone(createUser.PhoneNumber) {
		h.logger.Error("Invalid Phone Number!")
		c.JSON(http.StatusBadRequest, models.ErrorMessage{Phone: "Invalid Phone Number"})
		return
	}

	if createUser.Birthday > time.Now().String() {
		h.logger.Error("Invalid Birthday!")
		c.JSON(http.StatusBadRequest, models.ErrorMessage{Phone: "Invalid Birthday"})
		return
	}

	resp, err = h.storage.User().GetByPhoneNumber(c.Request.Context(), &models.UserPhoneNumber{PhoneNumber: createUser.PhoneNumber})
	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "error User.GetByPhoneNumber")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}
	if resp != nil {
		err := h.storage.User().UpdateDelete(c.Request.Context(), &models.UserPrimaryKey{PhoneNumber: createUser.PhoneNumber})
		if err != nil {
			h.logger.Error(err.Error() + "  :  " + "error User.UpdateDelete")
			c.JSON(http.StatusInternalServerError, "Server Error!")
			return
		}
	} else {
		resp, err = h.storage.User().Create(c.Request.Context(), &createUser)
		if err != nil {
			h.logger.Error(err.Error() + "  :  " + "error User.Create")
			c.JSON(http.StatusInternalServerError, "Server Error!")
			return
		}
	}

	h.logger.Info("Create User Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID Users godoc
// @ID get_by_id_users
// @Router /ibron/api/v1/user/{id} [GET]
// @Summary Get By ID Users
// @Description Get By ID Users
// @Tags User
// @Accept json
// @Procedure json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdUser(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	user, err := h.storage.User().GetByID(c.Request.Context(), &models.UserPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.User.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID User Response!")
	c.JSON(http.StatusOK, user)
}

// GetList Users godoc
// @ID get_list_users
// @Router /ibron/api/v1/users [GET]
// @Summary Get List Users
// @Description Get List Users
// @Tags User
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param filter query string false "filter"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListUser(c *gin.Context) {
	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListUser INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListUser INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	filter := c.Query("filter")
	search := c.Query("search")

	resp, err := h.storage.User().GetList(c.Request.Context(), &models.UserGetListRequest{
		Offset: offset,
		Limit:  limit,
		Filter: filter,
		Search: search,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.User.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListUser Response!")
	c.JSON(http.StatusOK, resp)
}

// Update Users godoc
// @ID update_users
// @Router /ibron/api/v1/user/{id} [PUT]
// @Summary Update Users
// @Description Update Users
// @Tags User
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Branch body models.UserUpdate true "UpdateUserRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UpdateUser(c *gin.Context) {
	var (
		id         = c.Param("id")
		updateUser models.UserUpdate
	)

	if !helper.IsValidUUID(id) {
		h.logger.Error("is invalid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	err := c.ShouldBindJSON(&updateUser)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error User Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	updateUser.Id = id
	rowsAffected, err := h.storage.User().Update(c.Request.Context(), &updateUser)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.User.Update!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if rowsAffected <= 0 {
		h.logger.Error("storage.User.Update!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
		return
	}

	resp, err := h.storage.User().GetByID(c.Request.Context(), &models.UserPrimaryKey{Id: updateUser.Id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.User.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Update User Successfully!")
	c.JSON(http.StatusAccepted, resp)
}

// Delete Users godoc
// @ID delete_users
// @Router /ibron/api/v1/user/{id} [DELETE]
// @Summary Delete Users
// @Description Delete Users
// @Tags User
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteUser(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.User().Delete(c.Request.Context(), &models.UserPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.User.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("User Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}

// GetByPhoneNumber Users godoc
// @ID get_by_phone_number_users
// @Router /ibron/api/v1/user/by-phone-number [GET]
// @Summary Get User By Phone Number
// @Description Get User By Phone Number
// @Tags User
// @Accept json
// @Procedure json
// @Param phone_number query string false "phone_number"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByPhoneNumberUser(c *gin.Context) {
	phoneNumber := c.Query("phone_number")

	if !helper.IsValidPhone(phoneNumber) {
		h.logger.Error("is valid phone number!")
		c.JSON(http.StatusBadRequest, "invalid phone number")
		return
	}

	user, err := h.storage.User().GetByPhoneNumber(c.Request.Context(), &models.UserPhoneNumber{PhoneNumber: phoneNumber})
	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.User.GetByPhoneNumber!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	fmt.Println(phoneNumber)
	fmt.Println(user)

	if user == nil && err.Error() == "no rows in result set" {
		c.JSON(http.StatusBadRequest, "Bad request")
		return
	}

	h.logger.Info("GetByPhoneNumber User Response!")
	c.JSON(http.StatusOK, user)
}

// upload user photos godoc
// @ID upload_user_photo
// @Router /ibron/api/v1/upload [POST]
// @Summary Upload User Photos
// @Description Upload User Photos
// @Tags User
// @Accept multipart/form-data
// @Procedure json
// @Param file formData file true "Photo to upload"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
// func (h *handler) UploadFile(c *gin.Context) {

// 	file, err := c.FormFile("file")
// 	if err != nil {
// 		h.logger.Error("File error: " + err.Error())
// 		c.JSON(http.StatusBadRequest, "missing file in the request")
// 		return
// 	}

// 	resp, _ := helper.UploadFile(file)

// 	c.JSON(http.StatusOK, resp)
// }
