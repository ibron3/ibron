package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"ibron/models"
	"ibron/pkg/helper"
	"ibron/pkg/logger"
	"ibron/pkg/security"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"golang.org/x/crypto/bcrypt"
)

type merchantRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewMerchantRepo(db *pgxpool.Pool, log logger.LoggerI) *merchantRepo {
	return &merchantRepo{
		db: db,
	}
}

func (u *merchantRepo) Create(ctx context.Context, req *models.MerchantCreate) (*models.Merchant, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	hashedPassword, err := security.HashPassword(req.Password)
	if err != nil {
		u.log.Error("error is while hashing password", logger.Error(err))
		return nil, err
	}

	query = `
		INSERT INTO "merchants"(id, name, login, password, phone_number)
		VALUES ($1, $2, $3, $4, $5) 
	`
	_, err = u.db.Exec(ctx, query,
		id,
		req.Name,
		req.Login,
		hashedPassword,
		req.PhoneNumber,
	)
	if err != nil {
		u.log.Error("create merchant error" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.MerchantPrimaryKey{Id: id})

	if err != nil {
		u.log.Error("get by id merchant error" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *merchantRepo) GetByID(ctx context.Context, req *models.MerchantPrimaryKey) (*models.Merchant, error) {
	var (
		query string

		id          sql.NullString
		name        sql.NullString
		login       sql.NullString
		password    sql.NullString
		phoneNumber sql.NullString
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	query = `
		SELECT 
			id,
			name, 
			login, 
			password, 
			phone_number,
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "merchants" 
		WHERE deleted_at is null AND id =$1
	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&login,
		&password,
		&phoneNumber,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		u.log.Error("get by id merchant error" + err.Error())
		return nil, err
	}

	return &models.Merchant{
		Id:          id.String,
		Name:        name.String,
		Login:       login.String,
		Password:    password.String,
		PhoneNumber: phoneNumber.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}, nil
}

func (u *merchantRepo) GetList(ctx context.Context, req *models.MerchantGetListRequest) (*models.MerchantGetListResponse, error) {
	var (
		resp    = &models.MerchantGetListResponse{}
		query   string
		where   = " WHERE TRUE AND deleted_at is null"
		offset  = " OFFSET 0"
		limit   = " LIMIT 10"
		filter  = " ORDER BY created_at DESC"
		groupBy = " GROUP BY id  "
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name, 
			login, 
			password, 
			phone_number,
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "merchants" 
		
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += "AND deleted_at is null AND (name ILIKE " + "'" + req.Search + " OR phone_number ILIKE " + "'" + req.Search + "%'" + ")"
	}

	if req.Filter == "name" {
		filter = " ORDER BY name "
	}

	query += where + groupBy + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("get list merchant " + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			name        sql.NullString
			login       sql.NullString
			password    sql.NullString
			phoneNumber sql.NullString
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&name,
			&login,
			&password,
			&phoneNumber,
			&createdAt,
			&updatedAt,
		)
		if err != nil {
			u.log.Error("scan merchant data " + err.Error())
			return nil, err
		}

		resp.Merchants = append(resp.Merchants, &models.Merchant{
			Id:          id.String,
			Name:        name.String,
			Login:       login.String,
			Password:    password.String,
			PhoneNumber: phoneNumber.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}
	return resp, nil
}

func (u *merchantRepo) Update(ctx context.Context, req *models.MerchantUpdate) (int64, error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"merchants"
		SET
			name = :name,
			login =:login , 
			phone_number = :phone_number,
			updated_at = NOW()
		WHERE id = :id
	`
	params = map[string]interface{}{
		"id":           req.Id,
		"name":         req.Name,
		"login":        req.Login,
		"phone_number": req.PhoneNumber,
	}

	query, args := helper.ReplaceQueryParams(query, params)
	result, err := u.db.Exec(ctx, query, args...)
	if err != nil {
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (u *merchantRepo) Delete(ctx context.Context, req *models.MerchantPrimaryKey) error {
	_, err := u.db.Exec(ctx, `UPDATE "merchants" set deleted_at = now() WHERE id = $1`, req.Id)
	if err != nil {
		u.log.Error("error is while deleting merchant", logger.Error(err))
		return err
	}

	return nil
}

func (u *merchantRepo) UpdatePassword(ctx context.Context, req *models.UpdatePassword) (int64, string, error) {
	var (
		query string
	)

	hashPassword, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	if err != nil {
		return 0, req.PhoneNumber, err
	}

	query = `
		UPDATE "merchants"
		SET password = $1, updated_at = NOW()
		WHERE email = $2
	`
	result, err := u.db.Exec(ctx, query, hashPassword, req.PhoneNumber)
	if err != nil {
		u.log.Error("error is while updating merchant password", logger.Error(err))
		return 0, req.PhoneNumber, err
	}

	return result.RowsAffected(), req.PhoneNumber, nil
}

func (u *merchantRepo) GetMerchantCredentials(ctx context.Context, request *models.MerchantLoginRequest) (*models.Merchant, error) {
	var (
		id, password sql.NullString
	)

	query := `select id, password from merchants where login = $1`
	if err := u.db.QueryRow(ctx, query, request.Login).Scan(&id, &password); err != nil {
		return nil, err
	}

	return &models.Merchant{
		Id:       id.String,
		Password: password.String,
	}, nil
}
