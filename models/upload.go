package models

type Url struct {
	Url string `json:"url"`
}

type UploadFileResponse struct {
	Url []*Url `json:"url"`
}
