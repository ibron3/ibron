package handler

import (
	"github.com/gin-gonic/gin"
	"ibron/models"
	"net/http"
)

// Create Sms User godoc
// @ID create_sms_user
// @Router /ibron/api/v1/sms-user [POST]
// @Summary Create Sms User
// @Description Create Sms User
// @Tags SmsUser
// @Accept json
// @Procedure json
// @Param sms_user body models.SmsCreate true "CreateSmsUserRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateSmsUser(c *gin.Context) {
	var (
		smsCreate models.SmsCreate
	)

	err := c.ShouldBindJSON(&smsCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Sms User Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	err = h.storage.SmsUser().Create(c.Request.Context(), &smsCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error SmsUser.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create Sms User Successfully!!")
	c.JSON(http.StatusCreated, "created sms user")
}

//func (h *handler) GetByIdSmsUser(c *gin.Context) {
//	id := c.Param("id")
//
//	if !helper.IsValidUUID(id) {
//		h.logger.Error("is valid uuid!")
//		c.JSON(http.StatusBadRequest, "invalid id")
//		return
//	}
//
//	smsUser, err := h.storage.SmsUser().GetByID(c.Request.Context(), &models.SmsPrimaryKey{SenderID: id})
//	if err != nil {
//		h.logger.Error(err.Error() + "  :  " + "storage.SmsUser.GetByID!")
//		c.JSON(http.StatusInternalServerError, "Server Error!")
//		return
//	}
//
//	h.logger.Info("GetByID Sms User Response!")
//	c.JSON(http.StatusOK, smsUser)
//}
