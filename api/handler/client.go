package handler

import (
	"ibron/models"
	"ibron/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Create Clients godoc
// @ID create_clients
// @Router /ibron/api/v1/client [POST]
// @Summary Create Clients
// @Description Create Clients
// @Tags Client
// @Accept json
// @Procedure json
// @Param Client body models.ClientCreate true "CreateClientRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateClient(c *gin.Context) {
	var (
		clientCreate models.ClientCreate
	)

	err := c.ShouldBindJSON(&clientCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Client Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	if !helper.IsValidUUID(clientCreate.MerchantID) {
		h.logger.Error("Invalid Merchant ID!")
		c.JSON(http.StatusBadRequest, models.ErrorMessage{Phone: "Invalid Merchant ID"})
		return
	}

	client, err := h.storage.Client().GetByID(c.Request.Context(), &models.ClientPrimaryKey{PhoneNumber: clientCreate.PhoneNumber, MerchantId: clientCreate.MerchantID})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Client.Create.GetById")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}
	if client != nil {
		h.logger.Info("Client already added!")
		c.JSON(http.StatusBadRequest, "Client already added!")
		return
	}

	resp, err := h.storage.Client().Create(c.Request.Context(), &clientCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Client.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create Client Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID Clients godoc
// @ID get_by_id_clients
// @Router /ibron/api/v1/client/{id} [GET]
// @Summary Get By ID Clients
// @Description Get By ID Clients
// @Tags Client
// @Accept json
// @Procedure json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdClient(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	request, err := h.storage.Client().GetByID(c.Request.Context(), &models.ClientPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Client.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID Client Response!")
	c.JSON(http.StatusOK, request)
}

// GetList Clients godoc
// @ID get_list_clients
// @Router /ibron/api/v1/clients [GET]
// @Summary Get List Clients
// @Description Get List Clients
// @Tags Client
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListClient(c *gin.Context) {
	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListClient INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListClient INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	search := c.Query("search")

	resp, err := h.storage.Client().GetList(c.Request.Context(), &models.ClientGetListRequest{
		Offset: offset,
		Limit:  limit,
		Search: search,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Client.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListClient Response!")
	c.JSON(http.StatusOK, resp)
}

// GetListByMerchantID Clients godoc
// @ID get_list_by_merchant_id_clients
// @Router /ibron/api/v1/by-merchant-clients [GET]
// @Summary Get List By Merchant ID Clients
// @Description Get List By Merchant ID Clients
// @Tags Client
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param merchant_id query string false "merchant_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListByMerchantIDClient(c *gin.Context) {
	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListClient INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListClient INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	resp, err := h.storage.Client().GetListByMerchantID(c.Request.Context(), &models.ClientGetListRequest{
		Offset:     offset,
		Limit:      limit,
		MerchantID: c.Query("merchant_id"),
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Client.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListClient Response!")
	c.JSON(http.StatusOK, resp)
}

// Update Clients godoc
// @ID update_clients
// @Router /ibron/api/v1/client/{id} [PUT]
// @Summary Update Clients
// @Description Update Clients
// @Tags Client
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Client body models.ClientUpdate true "UpdateClientRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UpdateClient(c *gin.Context) {
	var (
		id           = c.Param("id")
		clientUpdate models.ClientUpdate
	)

	if !helper.IsValidUUID(id) {
		h.logger.Error("is invalid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	err := c.ShouldBindJSON(&clientUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Client Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	clientUpdate.Id = id
	rowsAffected, err := h.storage.Client().Update(c.Request.Context(), &clientUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Client.Update!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if rowsAffected <= 0 {
		h.logger.Error("storage.Client.Update!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
		return
	}

	resp, err := h.storage.Client().GetByID(c.Request.Context(), &models.ClientPrimaryKey{Id: clientUpdate.Id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Client.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Update Client Successfully!")
	c.JSON(http.StatusAccepted, resp)
}

// Delete Clients godoc
// @ID delete_clients
// @Router /ibron/api/v1/client/{id} [DELETE]
// @Summary Delete Clients
// @Description Delete Clients
// @Tags Client
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteClient(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.Client().Delete(c.Request.Context(), &models.ClientPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Client.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Client Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}
