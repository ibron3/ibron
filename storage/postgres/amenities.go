package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"ibron/models"
	"ibron/pkg/helper"
	"ibron/pkg/logger"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type amenityRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewAmenityRepo(db *pgxpool.Pool, log logger.LoggerI) *amenityRepo {
	return &amenityRepo{
		db:  db,
		log: log,
	}
}

func (u *amenityRepo) Create(ctx context.Context, req *models.AmenityCreate) (*models.Amenity, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "amenities"(id, name,url)
		VALUES ($1, $2,$3) 
	`

	if _, err := u.db.Exec(ctx, query,
		id,
		req.Name,
		req.Url,
	); err != nil {
		u.log.Error("insert error" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.AmenityPrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id amenity" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *amenityRepo) GetByID(ctx context.Context, req *models.AmenityPrimaryKey) (*models.Amenity, error) {
	var (
		query string

		id        sql.NullString
		name      sql.NullString
		url       sql.NullString
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	query = `
		SELECT 
			id,
			name, 
			url,
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "amenities" 
		WHERE id =$1

	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&url,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		u.log.Error("error while scanning amenity data" + err.Error())
		return nil, err
	}

	return &models.Amenity{
		Id:        id.String,
		Name:      name.String,
		Url:       url.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}, nil
}

func (u *amenityRepo) GetList(ctx context.Context, req *models.AmenityGetListRequest) (*models.AmenityGetListResponse, error) {
	var (
		resp   = &models.AmenityGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			url,
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "amenities"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting amenity list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			name      sql.NullString
			url       sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&name,
			&url,
			&createdAt,
			&updatedAt,
		)
		if err != nil {
			u.log.Error("error is while getting amenity list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Amenity = append(resp.Amenity, &models.Amenity{
			Id:        id.String,
			Name:      name.String,
			Url:       url.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}
	return resp, nil
}

func (u *amenityRepo) Update(ctx context.Context, req *models.AmenityUpdate) (int64, error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"banner"
		SET
			name = :name,
			url = :url
			updated_at = NOW()
		WHERE id = :id
	`
	params = map[string]interface{}{
		"id":   req.Id,
		"name": req.Name,
		"url":  req.Url,
	}

	query, args := helper.ReplaceQueryParams(query, params)
	result, err := u.db.Exec(ctx, query, args...)
	if err != nil {
		u.log.Error("error is while updating amenity data", logger.Error(err))
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (u *amenityRepo) Delete(ctx context.Context, req *models.AmenityPrimaryKey) error {

	_, err := u.db.Exec(ctx, `DELETE FROM banner  WHERE id = $1`, req.Id)
	if err != nil {
		u.log.Error("error is while deleting amenity", logger.Error(err))
		return err
	}

	return nil
}

func (u *amenityRepo) GetListServiceAmenity(ctx context.Context, req *models.ServicePrimaryKey) (*models.AmenityGetListResponse, error) {

	var (
		query string
		resp  = &models.AmenityGetListResponse{}
	)

	query = `
	SELECT 
		a.id,
		a.name, 
		a.url
	FROM "amenities" AS a
	JOIN service_amenities AS sa ON sa.amenity_id = a.id
	WHERE sa.service_id = $1
	`

	rows, err := u.db.Query(ctx, query, req.Id)
	if err != nil {
		u.log.Error("error is while getting amenity list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id   sql.NullString
			name sql.NullString
			url  sql.NullString
		)

		err = rows.Scan(
			&id,
			&name,
			&url,
		)
		if err != nil {
			u.log.Error("error is while getting amenity list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Amenity = append(resp.Amenity, &models.Amenity{
			Id:   id.String,
			Name: name.String,
			Url:  url.String,
		})
	}
	return resp, nil
}
