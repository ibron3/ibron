package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"ibron/models"
	"ibron/pkg/helper"
	"ibron/pkg/logger"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type userRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewUserRepo(db *pgxpool.Pool, log logger.LoggerI) *userRepo {
	return &userRepo{
		db:  db,
		log: log,
	}
}

func (u *userRepo) Create(ctx context.Context, req *models.UserCreate) (*models.User, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "users"(id, first_name, last_name, birthday, gender, phone_number, photo)
		VALUES ($1, $2, $3, $4, $5, $6, $7) 
	`

	if _, err := u.db.Exec(ctx, query,
		id,
		req.FirstName,
		req.LastName,
		req.Birthday,
		req.Gender,
		req.PhoneNumber,
		req.Photo,
	); err != nil {
		u.log.Error("insert error" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.UserPrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id user" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *userRepo) GetByID(ctx context.Context, req *models.UserPrimaryKey) (*models.User, error) {
	var (
		query string

		id          sql.NullString
		firstName   sql.NullString
		lastName    sql.NullString
		birthday    sql.NullString
		gender      sql.NullString
		phoneNumber sql.NullString
		photo       sql.NullString
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	query = `
		SELECT 
			id,
			first_name,
			last_name,
			TO_CHAR(birthday, 'yyyy-mm-dd'),
			gender,
			phone_number,
			photo,
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "users" 
		WHERE deleted_at is null AND id =$1

	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&firstName,
		&lastName,
		&birthday,
		&gender,
		&phoneNumber,
		&photo,
		&createdAt,
		&updatedAt,
	)

	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}

	return &models.User{
		Id:          id.String,
		FirstName:   firstName.String,
		LastName:    lastName.String,
		Birthday:    birthday.String,
		Gender:      gender.String,
		PhoneNumber: phoneNumber.String,
		Photo:       photo.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}, nil
}

func (u *userRepo) GetList(ctx context.Context, req *models.UserGetListRequest) (*models.UserGetListResponse, error) {
	var (
		resp    = &models.UserGetListResponse{}
		query   string
		where   = " WHERE TRUE AND deleted_at is null "
		offset  = " OFFSET 0"
		limit   = " LIMIT 10"
		filter  = " ORDER BY u.created_at DESC"
		groupBy = " GROUP BY u.id  "
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			first_name,
			last_name,
			TO_CHAR(birthday, 'yyyy-mm-dd'),
			gender,
			phone_number,
			photo,
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "users" AS u
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += " AND (u.first_name ILIKE " + "'" + req.Search + "%'" + " OR u.last_name ILIKE " + "'" + req.Search + "%'" + " OR u.phone_number ILIKE " + "'" + req.Search + "%'" + ")"
	}

	if req.Filter == "name" {
		filter = " ORDER BY u.first_name "
	}

	query += where + groupBy + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting user list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			firstName   sql.NullString
			lastName    sql.NullString
			birthday    sql.NullString
			gender      sql.NullString
			phoneNumber sql.NullString
			photo       sql.NullString
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&firstName,
			&lastName,
			&birthday,
			&gender,
			&phoneNumber,
			&photo,
			&createdAt,
			&updatedAt,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Users = append(resp.Users, &models.User{
			Id:          id.String,
			FirstName:   firstName.String,
			LastName:    lastName.String,
			Birthday:    birthday.String,
			Gender:      gender.String,
			PhoneNumber: phoneNumber.String,
			Photo:       photo.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}
	return resp, nil
}

func (u *userRepo) Update(ctx context.Context, req *models.UserUpdate) (int64, error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"users"
		SET
			first_name = :first_name,
			last_name = :last_name,
			birthday = :birthday,
			gender = :gender,
			phone_number = :phone_number,
			photo = :photo,
			updated_at = NOW()
		WHERE id = :id
	`
	params = map[string]interface{}{
		"id":           req.Id,
		"first_name":   req.FirstName,
		"last_name":    req.LastName,
		"birthday":     req.Birthday,
		"gender":       req.Gender,
		"phone_number": req.PhoneNumber,
		"photo":        req.Photo,
	}

	query, args := helper.ReplaceQueryParams(query, params)
	result, err := u.db.Exec(ctx, query, args...)
	if err != nil {
		u.log.Error("error is while updating user data", logger.Error(err))
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (u *userRepo) Delete(ctx context.Context, req *models.UserPrimaryKey) error {
	_, err := u.db.Exec(ctx, `UPDATE users set deleted_at = now()  WHERE id = $1`, req.Id)
	if err != nil {
		u.log.Error("error is while deleting user", logger.Error(err))
		return err
	}

	return nil
}

func (u *userRepo) UpdateDelete(ctx context.Context, req *models.UserPrimaryKey) error {
	_, err := u.db.Exec(ctx, `UPDATE users set deleted_at = NULL  WHERE phone_number = $1`, req.PhoneNumber)
	if err != nil {
		u.log.Error("error is while updating deleted user", logger.Error(err))
		return err
	}

	return nil
}

func (u *userRepo) GetByPhoneNumber(ctx context.Context, req *models.UserPhoneNumber) (*models.User, error) {
	var (
		query string

		id          sql.NullString
		firstName   sql.NullString
		lastName    sql.NullString
		birthday    sql.NullString
		gender      sql.NullString
		phoneNumber sql.NullString
		photo       sql.NullString
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	query = `
		SELECT 
			id,
			first_name,
			last_name,
			TO_CHAR(birthday, 'yyyy-mm-dd'),
			gender,
			phone_number,
			photo,
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "users" 
		WHERE phone_number = $1

	`

	err := u.db.QueryRow(ctx, query, req.PhoneNumber).Scan(
		&id,
		&firstName,
		&lastName,
		&birthday,
		&gender,
		&phoneNumber,
		&photo,
		&createdAt,
		&updatedAt,
	)
	if err != nil {
		u.log.Error("error while scanning data " + err.Error())
		return nil, err
	}

	return &models.User{
		Id:          id.String,
		FirstName:   firstName.String,
		LastName:    lastName.String,
		Birthday:    birthday.String,
		Gender:      gender.String,
		PhoneNumber: phoneNumber.String,
		Photo:       photo.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}, nil
}
