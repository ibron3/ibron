package helper

func WeekDayToEn(weekday string) string {
	var resp string
	switch weekday {
	case "Понедельник":
		resp = "Mon"
	case "Вторник":
		resp = "Tue"
	case "Среда":
		resp = "Wed"
	case "Четверг":
		resp = "Thu"
	case "Пятница":
		resp = "Fri"
	case "Суббота":
		resp = "Sat"
	case "Воскресенье":
		resp = "Sun"
	}
	return resp
}

func WeekDayToRU(weekday string) string {
	var resp string
	switch weekday {
	case "Mon":
		resp = "Понедельник"
	case "Tue":
		resp = "Вторник"
	case "Wed":
		resp = "Среда"
	case "Thu":
		resp = "Четверг"
	case "Fri":
		resp = "Пятница"
	case "Sat":
		resp = "Суббота"
	case "Sun":
		resp = "Воскресенье"
	}
	return resp
}
