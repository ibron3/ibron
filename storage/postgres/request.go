package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"ibron/models"
	"ibron/pkg/helper"
	"ibron/pkg/logger"
	"strconv"
	"time"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type requestRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewRequestRepo(db *pgxpool.Pool, log logger.LoggerI) *requestRepo {
	return &requestRepo{
		db:  db,
		log: log,
	}
}

func (u *requestRepo) GetListApprovedRequestsByServiceId(ctx context.Context, req *models.RequestGetListRequest) (*models.RequestGetListResponse, error) {
	query := `
	SELECT
		COUNT(*) OVER(),
		r.id,
		r.service_id, 
		r.client_id,
		r.user_id, 
		r.merchant_id,
		TO_CHAR(r.start_time, 'HH24:MI'), 
		TO_CHAR(r.end_time, 'HH24:MI'), 
		r.price, 
		r.status,
		TO_CHAR(r.date, 'yyyy-mm-dd'),
		TO_CHAR(r.created_at, 'dd/mm/yyyy HH24:MI:SS'), 
		TO_CHAR(r.updated_at, 'dd/mm/yyyy HH24:MI:SS'),
		c.first_name || ' ' || c.last_name as user_name
	FROM "requests" AS r
	JOIN "clients" AS c ON c.id = r.client_id
	WHERE r.service_id = $1 AND r.date = $2 AND (r.status = 'approved' OR r.status = 'finished')
	`

	query2 := `
	SELECT
		COUNT(*) OVER(),
		r.id,
		r.service_id, 
		r.user_id, 
		r.merchant_id,
		TO_CHAR(r.start_time, 'HH24:MI'), 
		TO_CHAR(r.end_time, 'HH24:MI'), 
		r.price, 
		r.status,
		TO_CHAR(r.date, 'yyyy-mm-dd'),
		TO_CHAR(r.created_at, 'dd/mm/yyyy HH24:MI:SS'), 
		TO_CHAR(r.updated_at, 'dd/mm/yyyy HH24:MI:SS'),
		u.first_name || ' ' || u.last_name as user_name
	FROM "requests" AS r
	JOIN "users" AS u ON u.id = r.user_id
	WHERE r.service_id = $1 AND r.date = $2 AND (r.status = 'approved' OR r.status = 'finished')
	`
	var resp = &models.RequestGetListResponse{}

	rows, err := u.db.Query(ctx, query, req.ServiceId, req.Date)
	if err != nil {
		u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			id         sql.NullString
			serviceID  sql.NullString
			merchantID sql.NullString
			clientID   sql.NullString
			userID     sql.NullString
			startTime  sql.NullString
			endTime    sql.NullString
			price      sql.NullFloat64
			status     sql.NullString
			date       sql.NullString
			user_name  sql.NullString
			createdAt  sql.NullString
			updatedAt  sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&serviceID,
			&userID,
			&merchantID,
			&clientID,
			&startTime,
			&endTime,
			&price,
			&status,
			&date,
			&createdAt,
			&updatedAt,
			&user_name,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Requests = append(resp.Requests, &models.Request{
			Id:         id.String,
			ServiceID:  serviceID.String,
			UserID:     userID.String,
			MerchantID: merchantID.String,
			ClientID:   clientID.String,
			StartTime:  startTime.String,
			EndTime:    endTime.String,
			Price:      price.Float64,
			Status:     status.String,
			Date:       date.String,
			CreatedAt:  createdAt.String,
			UpdatedAt:  updatedAt.String,
			UserName:   user_name.String,
		})
	}

	rows2, err := u.db.Query(ctx, query2, req.ServiceId, req.Date)
	if err != nil {
		u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
		return nil, err
	}
	defer rows2.Close()

	for rows2.Next() {
		var (
			id         sql.NullString
			serviceID  sql.NullString
			merchantID sql.NullString
			userID     sql.NullString
			startTime  sql.NullString
			endTime    sql.NullString
			price      sql.NullFloat64
			status     sql.NullString
			date       sql.NullString
			user_name  sql.NullString
			createdAt  sql.NullString
			updatedAt  sql.NullString
		)

		err = rows2.Scan(
			&resp.Count,
			&id,
			&serviceID,
			&userID,
			&merchantID,
			&startTime,
			&endTime,
			&price,
			&status,
			&date,
			&createdAt,
			&updatedAt,
			&user_name,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Requests = append(resp.Requests, &models.Request{
			Id:         id.String,
			ServiceID:  serviceID.String,
			UserID:     userID.String,
			MerchantID: merchantID.String,
			StartTime:  startTime.String,
			EndTime:    endTime.String,
			Price:      price.Float64,
			Status:     status.String,
			Date:       date.String,
			CreatedAt:  createdAt.String,
			UpdatedAt:  updatedAt.String,
			UserName:   user_name.String,
		})
	}

	return resp, nil
}

func (u *requestRepo) Create(ctx context.Context, req *models.RequestCreate) (*models.Request, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	if len(req.ClientID) != 0 {
		query = `
		INSERT INTO "requests"(id, service_id, client_id, merchant_id, start_time, end_time, price, status, date)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8,$9) 
	`

		if _, err := u.db.Exec(ctx, query,
			id,
			req.ServiceID,
			req.ClientID,
			req.MerchantID,
			req.StartTime,
			req.EndTime,
			req.Price,
			req.Status,
			req.Date,
		); err != nil {
			u.log.Error("insert error" + err.Error())
			return nil, err
		}
	}

	if len(req.UserID) != 0 {
		query = `
		INSERT INTO "requests"(id, service_id, user_id, merchant_id, start_time, end_time, price, status, date)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) 
	`

		if _, err := u.db.Exec(ctx, query,
			id,
			req.ServiceID,
			req.UserID,
			req.MerchantID,
			req.StartTime,
			req.EndTime,
			req.Price,
			req.Status,
			req.Date,
		); err != nil {
			u.log.Error("insert error" + err.Error())
			return nil, err
		}
	}

	resp, err := u.GetByID(context.Background(), &models.RequestPrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id request" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *requestRepo) GetByID(ctx context.Context, req *models.RequestPrimaryKey) (*models.Request, error) {
	var (
		query string

		id        sql.NullString
		serviceID sql.NullString
		userID    sql.NullString
		startTime sql.NullString
		endTime   sql.NullString
		price     sql.NullFloat64
		status    sql.NullString
		date      sql.NullString
		createdAt sql.NullString
		updatedAt sql.NullString

		s = NewServiceRepo(u.db, u.log)
	)

	query = `
		SELECT 
			id,
			service_id, 
			user_id, 
			TO_CHAR(start_time, 'HH24:MI'), 
			TO_CHAR(end_time, 'HH24:MI'), 
			price, 
			status,
			TO_CHAR(date, 'yyyy-mm-dd'),
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "requests" 
		WHERE deleted_at is null AND id =$1

	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&serviceID,
		&userID,
		&startTime,
		&endTime,
		&price,
		&status,
		&date,
		&createdAt,
		&updatedAt,
	)

	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}

	service, err := s.GetByID(ctx, &models.ServicePrimaryKey{Id: serviceID.String})
	if err != nil {
		u.log.Error("error is while getting service", logger.Error(err))
		return nil, err
	}

	timeEnd, err := strconv.Atoi(endTime.String[:2])
	if err != nil {
		return nil, err
	}

	timeStart, err := strconv.Atoi(startTime.String[:2])
	if err != nil {
		return nil, err
	}

	return &models.Request{
		Id:        id.String,
		ServiceID: serviceID.String,
		UserID:    userID.String,
		StartTime: startTime.String,
		Duration:  (timeEnd - timeStart) * 60,
		EndTime:   endTime.String,
		Price:     price.Float64,
		Status:    status.String,
		Date:      date.String,
		Service:   service,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}, nil
}

func (u *requestRepo) GetList(ctx context.Context, req *models.RequestGetListRequest) (*models.RequestGetListResponse, error) {
	var (
		resp   = &models.RequestGetListResponse{}
		query  string
		where  = " WHERE TRUE AND r.deleted_at is null"
		offset = " OFFSET 0"
		limit  = " LIMIT 100"
		filter = " ORDER BY r.created_at DESC"
		s      = NewServiceRepo(u.db, u.log)
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			r.id,
			r.service_id, 
			r.user_id, 
			r.merchant_id,
			TO_CHAR(r.start_time, 'HH24:MI'), 
			TO_CHAR(r.end_time, 'HH24:MI'), 
			r.price, 
			r.status,
			TO_CHAR(r.date, 'yyyy-mm-dd'),
			TO_CHAR(r.created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(r.updated_at, 'dd/mm/yyyy HH24:MI:SS'),
			u.first_name,
			u.last_name,
			c.first_name,
			c.last_name
		FROM "requests" AS r
		LEFT JOIN "users" AS u ON r.user_id = u.id
		LEFT JOIN "clients" AS c ON r.client_id = c.id
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", 1000)
	}

	if len(req.ServiceId) > 0 {
		where += " AND r.service_id = " + "'" + req.ServiceId + "'"
	}

	if len(req.UserId) > 0 {
		where += " AND r.user_id =" + "'" + req.UserId + "'"
		where += " AND (r.status IN ('new', 'approved')"
		where += " OR (r.status = 'canceled' AND r.created_at + INTERVAL '1 day' > NOW())) "
	}

	if len(req.MerchantID) > 0 {
		where += " AND r.merchant_id =" + "'" + req.MerchantID + "'"
	}

	if len(req.Date) > 0 {
		where += " AND r.date =" + "'" + req.Date + "'"
	}

	if len(req.Type) > 0 {
		if req.Type == "new" {
			where += " AND r.status ='new'"
		} else if req.Type == "approved" {
			where += " AND r.status = 'approved'"
		} else if req.Type == "finished" {
			where += " AND r.status = 'finished'"
		}
	}
	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting request list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			serviceID  sql.NullString
			merchantID sql.NullString
			userID     sql.NullString
			startTime  sql.NullString
			endTime    sql.NullString
			price      sql.NullFloat64
			status     sql.NullString
			date       sql.NullString
			createdAt  sql.NullString
			updatedAt  sql.NullString
			first_name sql.NullString
			last_name  sql.NullString
			cFirstName sql.NullString
			cLastName  sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&serviceID,
			&userID,
			&merchantID,
			&startTime,
			&endTime,
			&price,
			&status,
			&date,
			&createdAt,
			&updatedAt,
			&first_name,
			&last_name,
			&cFirstName,
			&cLastName,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		d, err := time.Parse("2006-01-02", date.String)
		if err != nil {
			u.log.Error("error is while parsing time" + err.Error())
		}

		day := d.Weekday().String()

		service, err := s.GetByID(ctx, &models.ServicePrimaryKey{Id: serviceID.String})
		if err != nil && err.Error() != "no rows in result set" {
			u.log.Error("error is while getting service", logger.Error(err))
			return nil, err
		}

		timeEnd, err := strconv.Atoi(endTime.String[:2])
		if err != nil {
			return nil, err
		}

		timeStart, err := strconv.Atoi(startTime.String[:2])
		if err != nil {
			return nil, err
		}
		if req.Type != "" && req.Date != "" && req.ServiceId != "" {
			service = nil
		}
		if first_name.String == "" && last_name.String == "" {
			first_name.String = cFirstName.String
			last_name.String = cLastName.String
		}
		resp.Requests = append(resp.Requests, &models.Request{
			Id:         id.String,
			ServiceID:  serviceID.String,
			UserID:     userID.String,
			MerchantID: merchantID.String,
			StartTime:  startTime.String,
			EndTime:    endTime.String,
			Duration:   (timeEnd - timeStart) * 60,
			Price:      price.Float64,
			Status:     status.String,
			Date:       date.String,
			Day:        day,
			Service:    service,
			UserName:   first_name.String + " " + last_name.String,
			CreatedAt:  createdAt.String,
			UpdatedAt:  updatedAt.String,
		})
	}
	return resp, nil
}

func (u *requestRepo) CheckRequest(ctx context.Context, req *models.CheckRequest) bool {

	var id sql.NullString

	query := `
		SELECT 
			id
		FROM request
		WHERE service_id = $1 AND start_time = $2 AND date = $3 AND deleted_at is NULL
	`
	err := u.db.QueryRow(ctx, query, req.ServiceId, req.Time, req.Date).Scan(&id)
	if err != nil {
		u.log.Error("error is while checking requests" + err.Error())
	}

	if id.String == "" {
		return false
	}

	return true
}

func (u *requestRepo) Update(ctx context.Context, req *models.RequestUpdate) (int64, error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"requests"
		SET
			service_id = :service_id, 
			user_id = :user_id, 
			start_time = :start_time, 
			end_time = :end_time, 
			price = :price, 
			date = :date,
			status = :status,
			updated_at = NOW()
		WHERE id = :id
	`
	params = map[string]interface{}{
		"id":         req.Id,
		"service_id": req.ServiceID,
		"user_id":    req.UserID,
		"start_time": req.StartTime,
		"end_time":   req.EndTime,
		"price":      req.Price,
		"status":     req.Status,
		"date":       req.Date,
	}

	query, args := helper.ReplaceQueryParams(query, params)
	result, err := u.db.Exec(ctx, query, args...)
	if err != nil {
		u.log.Error("error is while updating request data", logger.Error(err))
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (u *requestRepo) UpdateStatus(ctx context.Context, req *models.UpdateStatus) (int64, error) {
	if req.MerchantID != "" {
		query := `
		UPDATE
			"requests"
		SET
			status = $1,
			updated_at = NOW()
		WHERE id = $2 and merchant_id = $3
	`
		result, err := u.db.Exec(ctx, query, req.Status, req.ID, req.MerchantID)
		if err != nil {
			u.log.Error("error is while updating request data by merchant id", logger.Error(err))
			return 0, err
		}

		return result.RowsAffected(), nil
	}

	query1 := `
		UPDATE
			"requests"
		SET
			status = $1,
			updated_at = NOW()
		WHERE id = $2
	`
	result1, err := u.db.Exec(ctx, query1, req.Status, req.ID)
	if err != nil {
		u.log.Error("error is while updating request data", logger.Error(err))
		return 0, err
	}

	return result1.RowsAffected(), nil
}

func (u *requestRepo) Delete(ctx context.Context, req *models.RequestPrimaryKey) error {
	_, err := u.db.Exec(ctx, `UPDATE requests set deleted_at = now()  WHERE id = $1`, req.Id)
	if err != nil {
		u.log.Error("error is while deleting requests", logger.Error(err))
		return err
	}

	return nil
}

func (u *requestRepo) CheckTime(ctx context.Context, request *models.Request) (bool, error) {
	var (
		id          sql.NullString
		timeZone, _ = time.LoadLocation("Asia/Tashkent")
		currenTime  = time.Now().In(timeZone).Format("15:04")
	)

	query := `SELECT id 
              FROM requests 
              WHERE date = $3 and 
                  start_time between $1 and $2 AND 
                  end_time between $1 and $2 
                  and start_time < ($4 - INTERVAL '30 MINUTE')`

	if err := u.db.QueryRow(ctx,
		query,
		request.StartTime,
		request.EndTime,
		request.Date,
		currenTime,
	).
		Scan(&id); err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error is while checking time requests", logger.Error(err))
		return false, err
	}

	if id.String != "" && request.Id != id.String {
		return false, nil
	}

	return true, nil
}

func (u *requestRepo) GetListFinishedRequests(ctx context.Context, req *models.RequestGetListRequest) (*models.RequestGetListResponse, error) {
	var (
		resp    = &models.RequestGetListResponse{}
		query   string
		where   = " WHERE TRUE AND r.deleted_at is null AND status = 'finished'"
		offset  = " OFFSET 0"
		limit   = " LIMIT 10"
		filter  = " ORDER BY r.created_at DESC"
		groupBy = " GROUP BY r.id, u.first_name, u.last_name  "
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			r.id,
			r.service_id, 
			r.user_id, 
			TO_CHAR(r.start_time, 'HH24:MI'), 
			TO_CHAR(r.end_time, 'HH24:MI'), 
			r.price, 
			r.status,
			TO_CHAR(r.date, 'yyyy-mm-dd'),
			TO_CHAR(r.created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(r.updated_at, 'dd/mm/yyyy HH24:MI:SS'),

			u.first_name,
			u.last_name

		FROM "requests" AS r
		JOIN "users" AS u ON r.user_id = u.id
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.UserId != "" {
		where += " AND r.user_id = " + "'" + req.UserId + "'"
	}

	query += where + groupBy + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting request list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			serviceID  sql.NullString
			userID     sql.NullString
			startTime  sql.NullString
			endTime    sql.NullString
			price      sql.NullFloat64
			status     sql.NullString
			date       sql.NullString
			first_name sql.NullString
			last_name  sql.NullString
			createdAt  sql.NullString
			updatedAt  sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&serviceID,
			&userID,
			&startTime,
			&endTime,
			&price,
			&status,
			&date,
			&createdAt,
			&updatedAt,
			&first_name,
			&last_name,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		d, _ := time.Parse("2006-01-02", date.String)
		day := d.Weekday().String()

		resp.Requests = append(resp.Requests, &models.Request{
			Id:        id.String,
			ServiceID: serviceID.String,
			UserID:    userID.String,
			StartTime: startTime.String,
			EndTime:   endTime.String,
			Price:     price.Float64,
			Status:    status.String,
			Date:      date.String,
			UserName:  first_name.String + " " + last_name.String,
			Day:       day,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}
	return resp, nil
}

func (u *requestRepo) GetListApprovedRequests(ctx context.Context, req *models.RequestGetListRequest) (*models.RequestGetListResponse, error) {
	var (
		resp    = &models.RequestGetListResponse{}
		query   string
		where   = " WHERE TRUE AND r.deleted_at is null AND status = 'approved'"
		offset  = " OFFSET 0"
		limit   = " LIMIT 10"
		filter  = " ORDER BY r.created_at DESC"
		groupBy = " GROUP BY r.id, u.first_name, u.last_name  "
		s       = NewServiceRepo(u.db, u.log)
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			r.id,
			r.service_id, 
			r.user_id, 
			TO_CHAR(r.start_time, 'HH24:MI'), 
			TO_CHAR(r.end_time, 'HH24:MI'), 
			r.price, 
			r.status,
			TO_CHAR(r.date, 'yyyy-mm-dd'),
			TO_CHAR(r.created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(r.updated_at, 'dd/mm/yyyy HH24:MI:SS'),
			u.first_name,
			u.last_name

		FROM "requests" AS r
		JOIN "users" AS u ON r.user_id = u.id
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.UserId != "" {
		where += " AND r.user_id = " + "'" + req.UserId + "'"
	}

	query += where + groupBy + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting request list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			serviceID  sql.NullString
			userID     sql.NullString
			startTime  sql.NullString
			endTime    sql.NullString
			price      sql.NullFloat64
			status     sql.NullString
			date       sql.NullString
			first_name sql.NullString
			last_name  sql.NullString
			createdAt  sql.NullString
			updatedAt  sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&serviceID,
			&userID,
			&startTime,
			&endTime,
			&price,
			&status,
			&date,
			&createdAt,
			&updatedAt,
			&first_name,
			&last_name,
		)
		if err != nil {
			u.log.Error("error is while getting user list (scanning data)", logger.Error(err))
			return nil, err
		}

		d, _ := time.Parse("2006-01-02", date.String)
		day := d.Weekday().String()

		service, err := s.GetByID(ctx, &models.ServicePrimaryKey{
			Id: serviceID.String,
		})
		if err != nil {
			u.log.Error("error is while getting service", logger.Error(err))
			return nil, err
		}
		timeEnd, err := strconv.Atoi(endTime.String[:2])
		if err != nil {
			return nil, err
		}

		timeStart, err := strconv.Atoi(startTime.String[:2])
		if err != nil {
			return nil, err
		}

		resp.Requests = append(resp.Requests, &models.Request{
			Id:        id.String,
			ServiceID: serviceID.String,
			UserID:    userID.String,
			StartTime: startTime.String,
			EndTime:   endTime.String,
			Duration:  (timeEnd - timeStart) * 60,
			Price:     price.Float64,
			Status:    status.String,
			Date:      date.String,
			UserName:  first_name.String + " " + last_name.String,
			Day:       day,
			Service:   service,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}
	return resp, nil
}

func (u *requestRepo) Report(ctx context.Context, key *models.MerchantPrimaryKey) (*models.ReportResponse, error) {
	var (
		count          sql.NullInt64
		sum            sql.NullFloat64
		finished_count sql.NullInt64
	)

	query := `SELECT SUM(price) FROM requests WHERE merchant_id = $1 AND date = $2 AND status = 'finished'`
	if err := u.db.QueryRow(ctx, query, key.Id, key.Date).Scan(&sum); err != nil {
		u.log.Error("error is while getting requests report", logger.Error(err))
		return nil, err
	}

	query = ` SELECT COUNT(id),  COUNT(CASE WHEN status = 'finished' THEN 1 END) FROM requests WHERE merchant_id = $1 AND date = $2`
	if err := u.db.QueryRow(ctx, query, key.Id, key.Date).Scan(&count, &finished_count); err != nil {
		u.log.Error("error is while getting requests report", logger.Error(err))
		return nil, err
	}

	requestQuery := `SELECT 
						r.id,      
						r.service_id, 
						r.user_id,    
						r.client_id,  
						TO_CHAR(r.start_time, 'HH24:MI'), 
						TO_CHAR(r.end_time, 'HH24:MI:SS'),   
						r.price,      
						TO_CHAR(r.date, 'YYYY-MM-DD'),       
						r.status,     
						r.created_at,  
						r.merchant_id,
						u.first_name,
						u.last_name,
						c.first_name,
						c.last_name
					FROM requests AS r
			        LEFT JOIN users AS u ON u.id = r.user_id 
					LEFT JOIN clients AS c ON c.id = r.client_id
					WHERE r.merchant_id = $1 and r.date = $2 AND r.status = 'finished'
	`

	requests, err := u.db.Query(ctx, requestQuery, key.Id, key.Date)
	if err != nil {
		u.log.Error("error is while getting requests report", logger.Error(err))
		return nil, err
	}
	resp := []*models.Request{}
	for requests.Next() {
		var (
			id         sql.NullString
			serviceID  sql.NullString
			userID     sql.NullString
			clientID   sql.NullString
			startTime  sql.NullString
			endTime    sql.NullString
			price      sql.NullFloat64
			date       sql.NullString
			status     sql.NullString
			createdAt  sql.NullString
			merchantID sql.NullString
			firstName  sql.NullString
			lastName   sql.NullString
			clientName sql.NullString
			clientLast sql.NullString
		)

		requests.Scan(
			&id,
			&serviceID,
			&userID,
			&clientID,
			&startTime,
			&endTime,
			&price,
			&date,
			&status,
			&createdAt,
			&merchantID,
			&firstName,
			&lastName,
			&clientName,
			&clientLast,
		)
		if firstName.String == "" || lastName.String == "" {
			firstName.String = clientName.String
			lastName.String = clientLast.String
		}
		t, err := time.Parse("2006-01-02", date.String)
		if err != nil {
			return nil, err
		}
		day := t.Weekday().String()
		resp = append(resp, &models.Request{
			Id:         id.String,
			ServiceID:  serviceID.String,
			UserID:     userID.String,
			ClientID:   clientID.String,
			StartTime:  startTime.String,
			EndTime:    endTime.String,
			Price:      price.Float64,
			Date:       date.String,
			Day:        day,
			Status:     status.String,
			CreatedAt:  createdAt.String,
			MerchantID: merchantID.String,
			UserName:   firstName.String + " " + lastName.String,
		})
	}

	return &models.ReportResponse{
		Count:         int(count.Int64),
		Sum:           int(sum.Float64),
		RequestList:   resp,
		FinishedCount: int(finished_count.Int64),
	}, nil
}

func (u *requestRepo) ReportForMonth(ctx context.Context, key *models.MerchantPrimaryKey) (*models.ReportResponse, error) {
	var (
		count          sql.NullInt64
		sum            sql.NullFloat64
		finished_count sql.NullInt64
	)

	query := `SELECT SUM(price) FROM requests WHERE merchant_id = $1 and to_char(date,'mm') =  to_char(now(),'mm') AND status = 'finished'`
	if err := u.db.QueryRow(ctx, query, key.Id).Scan(&sum); err != nil {
		u.log.Error("error is while getting requests report", logger.Error(err))
		return nil, err
	}

	query = ` SELECT COUNT(*), COUNT(CASE WHEN status = 'finished' THEN 1 END) FROM requests WHERE merchant_id = $1 AND to_char(date,'mm') =  to_char(now(),'mm')`
	if err := u.db.QueryRow(ctx, query, key.Id).Scan(&count, &finished_count); err != nil {
		u.log.Error("error is while getting requests report 2", logger.Error(err))
		return nil, err
	}

	requestQuery := `SELECT 
						id,      
						service_id, 
						user_id,    
						client_id,  
						start_time, 
						end_time,   
						price,      
						date,       
						status,     
						created_at,  
						merchant_id
					FROM requests WHERE merchant_id = $1 and to_char(date,'mm') =  to_char(now(),'mm') AND status = 'finished'
	`

	requests, err := u.db.Query(ctx, requestQuery, key.Id)
	if err != nil {
		u.log.Error("error is while getting requests report", logger.Error(err))
		return nil, err
	}
	resp := []*models.Request{}
	for requests.Next() {
		var (
			id         sql.NullString
			serviceID  sql.NullString
			userID     sql.NullString
			clientID   sql.NullString
			startTime  sql.NullString
			endTime    sql.NullString
			price      sql.NullFloat64
			date       sql.NullString
			status     sql.NullString
			createdAt  sql.NullString
			merchantID sql.NullString
		)

		requests.Scan(
			&id,
			&serviceID,
			&userID,
			&clientID,
			&startTime,
			&endTime,
			&price,
			&date,
			&status,
			&createdAt,
			&merchantID,
		)

		resp = append(resp, &models.Request{
			Id:         id.String,
			ServiceID:  serviceID.String,
			UserID:     userID.String,
			ClientID:   clientID.String,
			StartTime:  startTime.String,
			EndTime:    endTime.String,
			Price:      price.Float64,
			Date:       date.String,
			Status:     status.String,
			CreatedAt:  createdAt.String,
			MerchantID: merchantID.String,
		})
	}

	return &models.ReportResponse{
		Count:         int(count.Int64),
		Sum:           int(sum.Float64),
		FinishedCount: int(finished_count.Int64),
		RequestList:   resp,
	}, nil
}

func (u *requestRepo) ReportDiagram(ctx context.Context, key *models.MerchantPrimaryKey) (*models.ReportDiagram, error) {
	var (
		report = &models.ReportDiagram{}
	)
	query := `SELECT TO_CHAR(date, 'MM') AS month_number, SUM(price) AS total_price
	         FROM requests WHERE merchant_id = $1 and status = 'finished'
	         GROUP BY month_number
	         ORDER BY month_number`

	rows, err := u.db.Query(ctx, query, key.Id)
	if err != nil {
		u.log.Error("error is while getting requests report list", logger.Error(err))
		return nil, err
	}
	defer rows.Close()

	// Map of month numbers to Russian month names
	monthNames := map[string]string{
		"01": "янв", "02": "февр", "03": "март", "04": "апр", "05": "май", "06": "июнь",
		"07": "июль", "08": "авг", "09": "сент", "10": "окт", "11": "нояб", "12": "дек",
	}

	previous := 0.0
	sum := 0.0
	count := 0
	for rows.Next() {
		var (
			monthNumber sql.NullString
			totalPrice  sql.NullFloat64
		)
		if err = rows.Scan(&monthNumber, &totalPrice); err != nil {
			u.log.Error("error is while scanning requests report", logger.Error(err))
			return nil, err
		}

		monthName := ""
		if monthNumber.Valid {
			monthName = monthNames[monthNumber.String]
		}

		count++
		result := 0.0
		if count > 1 {
			result = totalPrice.Float64 - previous
		}

		sum += totalPrice.Float64
		report.ReportList = append(report.ReportList, &models.ReportList{
			Month:  monthName,
			Data:   totalPrice.Float64,
			Result: result,
		})
		previous = totalPrice.Float64
	}

	if err := rows.Err(); err != nil {
		u.log.Error("error occurred during row iteration", logger.Error(err))
		return nil, err
	}

	report.TotalSum = sum
	return report, nil
}
