package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"ibron/models"
	"ibron/pkg/logger"
)

type thumbnailsRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewThumbnailsRepo(db *pgxpool.Pool, log logger.LoggerI) *thumbnailsRepo {
	return &thumbnailsRepo{
		db:  db,
		log: log,
	}
}

func (u *thumbnailsRepo) Create(ctx context.Context, req *models.ThumbnailCreate) (*models.Thumbnail, error) {
	var id = uuid.New().String()

	query := `insert into thumbnails (id, service_id, url) values($1, $2, $3)`
	if _, err := u.db.Exec(ctx, query, id, req.ServiceID, req.Url); err != nil {
		u.log.Error("insert error" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.ThumbnailPrimaryKey{ID: id})
	if err != nil {
		u.log.Error("error get by id user" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *thumbnailsRepo) GetByID(ctx context.Context, req *models.ThumbnailPrimaryKey) (*models.Thumbnail, error) {
	var (
		query string

		id        sql.NullString
		serviceID sql.NullString
		url       sql.NullString
	)

	query = `
		SELECT 
			id,
			service_id,
			url
		FROM "thumbnails" 
		WHERE id =$1

	`

	err := u.db.QueryRow(ctx, query, req.ID).Scan(
		&id,
		&serviceID,
		&url,
	)

	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}

	return &models.Thumbnail{
		ID:        id.String,
		ServiceID: serviceID.String,
		Url:       url.String,
	}, nil
}

func (u *thumbnailsRepo) GetList(ctx context.Context, req *models.ThumbnailGetListRequest) (*models.ThumbnailGetListResponse, error) {
	var (
		resp    = &models.ThumbnailGetListResponse{}
		query   string
		where   = " "
		offset  = " OFFSET 0"
		limit   = " LIMIT 10"
		filter  = " ORDER BY th.created_at DESC"
		groupBy = " GROUP BY th.id  "
	)

	query = `
		SELECT
			id,
			service_id,
			url
		FROM "thumbnails" as th
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + groupBy + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting user list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			serviceID sql.NullString
			url       sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&serviceID,
			&url,
		)
		if err != nil {
			u.log.Error("error is while getting thumbnail list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Thumbnails = append(resp.Thumbnails, &models.Thumbnail{
			ID:        id.String,
			ServiceID: serviceID.String,
			Url:       url.String,
		})
	}

	return resp, nil
}
