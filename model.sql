CREATE TABLE IF NOT EXISTS "users"(
    "id" UUID PRIMARY KEY,
    "first_name" VARCHAR(25) NOT NULL,
    "last_name" VARCHAR(25) NOT NULL,
    "birthday" DATE NOT NULL,
    "gender" VARCHAR(7) CHECK (gender IN ('Мужчина', 'Женщина')),
    "phone_number" VARCHAR(20) UNIQUE NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIME,
    "updated_at" TIMESTAMP
);

/*
    request approve bolganda schedulega create boladi

    bo'sh vaqtlarni olish uchun schedule getlist qilinadi va bosh vaqtlarda request create api qoyiladi
*/


        SELECT
			id
		FROM requests
		WHERE TO_CHAR(date,'yyyy-mm-dd') = '2024-05-27' AND service_id = '0c6a8a1b-ce9a-4138-baa9-e82fc3c4ef17' AND TO_CHAR(start_time, 'HH24:MI') = '18:00' AND TO_CHAR(end_time, 'HH24:MI') = '19:00' AND deleted_at is null AND status = 'approved'



        SELECT
                        id
                FROM requests
                WHERE date = '2024-07-01' AND service_id = 'c913856f-9240-4186-9fdc-b49461da3355' AND TO_CHAR(start_time, 'HH24:MI') BETWEEN '13:00' AND '14:00'  AND TO_CHAR(end_time, 'HH24:MI') BETWEEN '13:00' AND '14:00' AND deleted_at is null AND status = 'approved';




SELECT
			id
		FROM requests
		WHERE date = '2024-07-01' AND service_id = 'c913856f-9240-4186-9fdc-b49461da3355' AND '13:00' BETWEEN TO_CHAR(start_time, 'HH24:MI') AND TO_CHAR(end_time, 'HH24:MI')  AND '14:00' BETWEEN TO_CHAR(start_time, 'HH24:MI') AND TO_CHAR(end_time, 'HH24:MI') AND deleted_at is null AND status = 'approved'



SELECT
			id
		FROM requests
		WHERE date = '2024-07-05' AND service_id = 'be985ae7-241d-493f-91e3-11a09e94ac02' AND '00:00' BETWEEN TO_CHAR(start_time, 'HH24:MI') AND TO_CHAR(end_time, 'HH24:MI')  AND '01:00' BETWEEN TO_CHAR(start_time, 'HH24:MI') AND TO_CHAR(end_time, 'HH24:MI') AND deleted_at is null AND status = 'approved'
	

       

        SELECT
			COUNT(*) OVER(),
			r.id,
			r.service_id, 
			r.user_id, 
			r.merchant_id,
			TO_CHAR(r.start_time, 'HH24:MI'), 
			TO_CHAR(r.end_time, 'HH24:MI'), 
			r.price, 
			r.status,
			TO_CHAR(r.date, 'yyyy-mm-dd'),
			TO_CHAR(r.created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(r.updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "requests" AS r
        WHERE r.user_id ='0c025b85-53a7-4a21-bd2d-2a05dddb0853'
		AND (r.status IN ('new', 'approved')
		OR (r.status = 'canceled' AND r.updated_at + INTERVAL '1 day' > NOW())) 