package models

type BusinessMerchant struct {
	Id           string  `json:"id"`
	MerchantID   string  `json:"merchant_id"`
	BusinessName string  `json:"business_name"`
	Description  string  `json:"description"`
	Address      string  `json:"address"`
	Latitude     float64 `json:"latitude"`
	Longitude    float64 `json:"longitude"`
	Phone1       string  `json:"phone_1"`
	Phone2       string  `json:"phone_2"`
	CreatedAt    string  `json:"created_at"`
	UpdatedAt    string  `json:"updated_at"`
}

type BusinessMerchantCreate struct {
	MerchantID   string  `json:"merchant_id"`
	BusinessName string  `json:"business_name"`
	Description  string  `json:"description"`
	Address      string  `json:"address"`
	Latitude     float64 `json:"latitude"`
	Longitude    float64 `json:"longitude"`
	Phone1       string  `json:"phone_1"`
	Phone2       string  `json:"phone_2"`
}

type BusinessMerchantUpdate struct {
	Id           string  `json:"id"`
	MerchantID   string  `json:"merchant_id"`
	BusinessName string  `json:"business_name"`
	Description  string  `json:"description"`
	Address      string  `json:"address"`
	Latitude     float64 `json:"latitude"`
	Longitude    float64 `json:"longitude"`
	Phone1       string  `json:"phone_1"`
	Phone2       string  `json:"phone_2"`
}

type BusinessMerchantPrimaryKey struct {
	Id string `json:"id"`
}

type BusinessMerchantGetListRequest struct {
	Offset     int    `json:"offset"`
	Limit      int    `json:"limit"`
	Filter     string `json:"filter"`
	Search     string `json:"search"`
	MerchantId string `json:"merchant_id"`
}

type BusinessMerchantGetListResponse struct {
	Count             int                 `json:"count"`
	BusinessMerchants []*BusinessMerchant `json:"business_merchants"`
}
