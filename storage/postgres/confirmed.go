package postgres

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"ibron/models"
	"ibron/pkg/logger"
	"time"
)

type confirmedRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewConfirmedRepo(db *pgxpool.Pool, log logger.LoggerI) *confirmedRepo {
	return &confirmedRepo{
		db:  db,
		log: log,
	}
}

func (u *confirmedRepo) Create(ctx context.Context, confirmed *models.Confirmed) error {
	location, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error while loading location")
	}

	currentTime := time.Now().In(location)
	if _, err := u.db.Exec(ctx, `insert into confirmed_requests (request_id, created_at) values($1, $2)`,
		confirmed.RequestId,
		currentTime); err != nil {
		u.log.Error("insert error" + err.Error())
		return err
	}
	return nil
}
