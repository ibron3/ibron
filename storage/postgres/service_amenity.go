package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"ibron/models"
	"ibron/pkg/helper"
	"ibron/pkg/logger"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type serviceAmenityRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewServiceAmenityRepo(db *pgxpool.Pool, log logger.LoggerI) *serviceAmenityRepo {
	return &serviceAmenityRepo{
		db:  db,
		log: log,
	}
}

func (u *serviceAmenityRepo) Create(ctx context.Context, req *models.ServiceAmenityCreate) (*models.ServiceAmenity, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "service_amenities"(id, service_id,amenity_id)
		VALUES ($1, $2, $3) 
	`

	if _, err := u.db.Exec(ctx, query,
		id,
		req.ServiceId,
		req.AmenityId,
	); err != nil {
		u.log.Error("insert error" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.ServiceAmenityPrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id service amenity" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *serviceAmenityRepo) GetByID(ctx context.Context, req *models.ServiceAmenityPrimaryKey) (*models.ServiceAmenity, error) {
	var (
		query string

		id         sql.NullString
		serivce_id sql.NullString
		amenity_id sql.NullString
		createdAt  sql.NullString
		updatedAt  sql.NullString
	)

	query = `
		SELECT 
			id,
			service_id, 
			amenity_id,
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "service_amenities" 
		WHERE id =$1

	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&serivce_id,
		&amenity_id,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		u.log.Error("error while scanning amenity data" + err.Error())
		return nil, err
	}

	return &models.ServiceAmenity{
		Id:        id.String,
		ServiceId: serivce_id.String,
		AmenityId: amenity_id.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}, nil
}

func (u *serviceAmenityRepo) GetList(ctx context.Context, req *models.ServiceAmenityGetListRequest) (*models.ServiceAmenityGetListResponse, error) {
	var (
		resp   = &models.ServiceAmenityGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			service_id, 
			amenity_id,
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "service_amenities"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting service amenity list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			serivce_id sql.NullString
			amenity_id sql.NullString
			createdAt  sql.NullString
			updatedAt  sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&serivce_id,
			&amenity_id,
			&createdAt,
			&updatedAt,
		)
		if err != nil {
			u.log.Error("error is while getting service amenity list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.ServiceAmenity = append(resp.ServiceAmenity, &models.ServiceAmenity{
			Id:        id.String,
			ServiceId: serivce_id.String,
			AmenityId: amenity_id.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}
	return resp, nil
}

func (u *serviceAmenityRepo) Update(ctx context.Context, req *models.ServiceAmenityUpdate) (int64, error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"service_amenities"
		SET
			service_id = :service_id,
			amenity_id = :amenity_id
			updated_at = NOW()
		WHERE id = :id
	`
	params = map[string]interface{}{
		"id":         req.Id,
		"service_id": req.ServiceId,
		"amenity_id": req.AmenityId,
	}

	query, args := helper.ReplaceQueryParams(query, params)
	result, err := u.db.Exec(ctx, query, args...)
	if err != nil {
		u.log.Error("error is while updating service amenity data", logger.Error(err))
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (u *serviceAmenityRepo) Delete(ctx context.Context, req *models.ServiceAmenityPrimaryKey) error {

	_, err := u.db.Exec(ctx, `DELETE FROM service_amenities  WHERE id = $1`, req.Id)
	if err != nil {
		u.log.Error("error is while deleting service amenity", logger.Error(err))
		return err
	}

	return nil
}


