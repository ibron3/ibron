package helper

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/nfnt/resize"
	"golang.org/x/image/bmp"
	"golang.org/x/image/tiff"
	"golang.org/x/image/webp"
	"google.golang.org/api/storage/v1"
	"ibron/models"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"os"
	"path/filepath"

	_ "golang.org/x/image/bmp"
	_ "golang.org/x/image/tiff"
	_ "golang.org/x/image/webp"

	firebase "firebase.google.com/go"
	"google.golang.org/api/option"
)

type Cred struct {
	Type                        string `json:"type"`
	Project_id                  string `json:"project_id"`
	Private_key_id              string `json:"private_key_id"`
	Private_key                 string `json:"private_key"`
	Client_email                string `json:"client_email"`
	Client_id                   string `json:"client_id"`
	Auth_uri                    string `json:"auth_uri"`
	Token_uri                   string `json:"token_uri"`
	Auth_provider_x509_cert_url string `json:"auth_provider_x509_cert_url"`
	Client_x509_cert_url        string `json:"client_x509_cert_url"`
	Universe_domain             string `json:"universe_domain"`
}

func ResizeImage(file multipart.File) (*bytes.Buffer, error) {
	img, err := jpeg.Decode(file)
	if err != nil {
		log.Fatal(err.Error())
	}

	desiredWidth := 535
	desiredHeight := 285

	aspectRatio := float64(img.Bounds().Dx()) / float64(img.Bounds().Dy())

	// Calculate the new dimensions while preserving the aspect ratio
	var newWidth, newHeight uint
	if float64(desiredWidth)/aspectRatio > float64(desiredHeight) {
		newWidth = uint(float64(desiredHeight) * aspectRatio)
		newHeight = uint(desiredHeight)
	} else {
		newWidth = uint(desiredWidth)
		newHeight = uint(float64(desiredWidth) / aspectRatio)
	}

	resizedImg := resize.Resize(newWidth, newHeight, img, resize.Lanczos3)

	x := (resizedImg.Bounds().Dx() - 500) / 2
	y := (resizedImg.Bounds().Dy() - 250) / 2

	croppedImg := resizedImg.(interface {
		SubImage(r image.Rectangle) image.Image
	}).SubImage(image.Rect(x, y, x+500, y+250))

	out := new(bytes.Buffer)
	err = jpeg.Encode(out, croppedImg, nil)
	if err != nil {
		return nil, err
	}

	return out, nil
}

func CropImage(file multipart.File, width, height int) (*bytes.Buffer, error) {
	var img image.Image

	config, format, err := image.DecodeConfig(file)
	if err != nil {
		log.Fatalf("Error decoding image format: %v", err)
	}
	fmt.Println("Image format:", format)
	fmt.Println("widht ", config.Width, " height ", config.Height)

	if config.Width < 1000 && config.Height < 1000 {
		log.Println("picture size is small")
		return nil, errors.New("picture size is small")
	}
	
	_, err = file.Seek(0, 0)
	if err != nil {
		log.Fatalf("Error seeking file: %v", err)
	}

	switch format {
	case "jpeg":
		img, err = jpeg.Decode(file)
		if err != nil {
			log.Fatalf("cannot decoding err: " + err.Error())
			return nil, err
		}
	case "png":
		img, err = png.Decode(file)
		if err != nil {
			log.Fatalf("cannot decoding err: " + err.Error())
			return nil, err
		}
	case "gif":
		img, err = gif.Decode(file)
		if err != nil {
			log.Fatalf("cannot decoding err: " + err.Error())
			return nil, err
		}
	case "bmp":
		img, err = bmp.Decode(file)
		if err != nil {
			log.Fatalf("cannot decoding err: " + err.Error())
			return nil, err
		}
	case "tiff":
		img, err = tiff.Decode(file)
		if err != nil {
			log.Fatalf("cannot decoding err: " + err.Error())
			return nil, err
		}
	case "webp":
		img, err = webp.Decode(file)
		if err != nil {
			log.Fatalf("cannot decoding err: " + err.Error())
			return nil, err
		}
	default:
		log.Fatalf("Unsupported image format: %s", format)
	}

	desiredWidth := width + 100
	desiredHeight := height + 100

	aspectRatio := float64(img.Bounds().Dx()) / float64(img.Bounds().Dy())

	// Calculate the new dimensions while preserving the aspect ratio
	var newWidth, newHeight uint
	if float64(desiredWidth)/aspectRatio > float64(desiredHeight) {
		newWidth = uint(float64(desiredHeight) * aspectRatio)
		newHeight = uint(desiredHeight)
	} else {
		newWidth = uint(desiredWidth)
		newHeight = uint(float64(desiredWidth) / aspectRatio)
	}

	resizedImg := resize.Resize(newWidth, newHeight, img, resize.Lanczos3)

	x := (resizedImg.Bounds().Dx() - width) / 2
	y := (resizedImg.Bounds().Dy() - height) / 2

	croppedImg := resizedImg.(interface {
		SubImage(r image.Rectangle) image.Image
	}).SubImage(image.Rect(x, y, x+width, y+height))

	out := new(bytes.Buffer)
	err = jpeg.Encode(out, croppedImg, nil)
	if err != nil {
		return nil, err
	}

	return out, nil
}

func UploadFiles(file *multipart.Form, name string) (*models.MultipleFileUploadResponse, error) {

	i := 0
	var resp models.MultipleFileUploadResponse

	for _, v := range file.File["file"] {
		id := uuid.New().String()
		var (
			url           models.Url
			width, height int
		)

		url.Url = fmt.Sprintf("https://firebasestorage.googleapis.com/v0/b/ibron-b5f1c.appspot.com/o/%s?alt=media&token=%s", id, id)

		resp.Url = append(resp.Url, &url)

		imageFile, err := v.Open()

		if name == "thumbnails" {
			width = 180
			height = 110
		} else if name == "service_photo" {
			width = 500
			height = 250
		} else if name == "banner" {
			width = 380
			height = 200
		}

		resizedImage, err := CropImage(imageFile, width, height)
		if err != nil {
			log.Println("error is while resize image" + err.Error())
			return nil, err
		}

		tempFile, err := os.Create(id)
		if err != nil {
			return nil, err
		}
		_, err = io.Copy(tempFile, resizedImage)
		if err != nil {
			return nil, err
		}

		defer tempFile.Close()

		var cred Cred
		cred.Type = "service_account"
		cred.Project_id = "ibron-b5f1c"
		cred.Private_key_id = "d68d475cb1dd5843b149d04e6828eceec6459224"
		cred.Private_key = "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC+x+oZMne+L3Mm\nHW5mhubySrIZeIKf1OU9D3Hp1Y5zRtgIHTEuRfoeFIPgyP/whqeVP8mOMaEhB/0A\nV2Y/5mLLXtbquE+2F3qDEMofW+8dnXs2o2GIRiIIO2NZkMDuH3zlSbVQGRk3K6nn\n5RTNw+VSDqR8lw8HAFKOuV6ZgfdQOWQ3pt+v5HVO+4LBi6MQO9TicTIJDwRhoHmV\n+Tzoi7deMToYES9jGNH/XUGggMTLpsYb143Y4ycczuKlOOjNxm8QEgplyvXnf2lb\nWxZ+iU4kzWw1Ug1Vs0zNoB631FIEtJWmTwpBbEXDYxW804IunHO5oMZihdhT2WN0\nf64asTC1AgMBAAECggEAFsQ7FK4v2W+GpYACHnSPR08wITd/GQ06HPcEoEO2l92W\npqM7LPs3TZMjqLW7yyt6gKZHynL0OEGgiMOlRU9cL0o+SezcQE3ddhoRELxzFLzo\nCP1mxHEgGwzhPn02dMQnrqIE483W8z0/7ZcqDCMKMOhj6yVIqwx4DaWj2xxGE9nM\nrjn1N+WgFIxl55KIdkjW6p8EG0m2TuanuZnKxUN3CLJA9K+WjUUpWOYVgLC6heKT\nbni6CK62KcIYtxign/Rw4MuA42cT5XQfpxkt73gK/VtAQXkIpNc1keO+7rUwbZu1\ntBu3chZ5Sd8eidbcfWm4pW34yNRyvSslKLW1i2RRsQKBgQDjYqcNl7RfnuvMcfdu\nkvx5qecdkYs7preMxy1muRXkqUu8+owjFrwwyzlh4lnK8BNW7GA+0sfIp7Y3DNWy\nfxGqBJO2Q9LcmHCzyUEsLirQmJq7sw8vJEi/mhEF78+hSHLb2Q+mrimeXNKWUrh1\nMIFQgKqTBn6bXOP6lh+Qn5G8ZQKBgQDWygdw8keJ+ctNAiRQFumz5aLPj6t4Ydfo\nX0oLbKwX8uzQ6B0aefw51FsuK9nMycpNMsiHQTL3dnAo0aKiVVDuFbwXM9WjG3w7\nWmkpbWVsTwDO6S6YDKUfb02zg4ce10YPmh7f7YJTEWlTsTCJpAZdxP3SDKqoVa9O\n9wL9B9cWEQKBgQDgd6rhyDjPjFrKYFcaWQ/UKt10WxFPuazI1HcFSQFL0bRqwgtF\nOFRe9r+W2ZQ6ufTdgksguYNvmeBPR+RNaRNO+2W2MA4gpnvbf3lO2+R4N63ITWNX\n7kHCo5zad3pIhPSeAxFoQdXj904MB20AgvmlNt8sdOTXCkQkQ761TlDX4QKBgDCe\nrM6JWV5HOBjnejbS6PX67j7Tw7nNVCVapJUvdUk6iRbE5AmhQVIJ2tX5wSSVLQEK\nlM/hF1ti6QKkaY8bhuVN8DTEXAC+8QEydwmUpNe+jm1H5W8vtZUHCeyvwj9d9ZME\njSadSdnp4BC81ELj6TkCpX4NtffC5ZsXWQrT4oExAoGAPB86M3woO2TkcIPIbULX\nkD5rbrzsxhWtap0AGzP4gF3Qf4irDlW7h3qSRwKwMVuvfO8yOHAIPhAaficihmPF\nI9wWhD5+P9Eb4+7PXbOSNQKo9fTlHkVpzO5iYXhGo8emqnzoYKTbYtAXP0YNmARJ\n9JM8suTztt8F49n0b+/D5+E=\n-----END PRIVATE KEY-----\n"
		cred.Client_email = "firebase-adminsdk-6ewj4@ibron-b5f1c.iam.gserviceaccount.com"
		cred.Client_id = "108239552011752485643"
		cred.Auth_uri = "https://accounts.google.com/o/oauth2/auth"
		cred.Token_uri = "https://oauth2.googleapis.com/token"
		cred.Auth_provider_x509_cert_url = "https://www.googleapis.com/oauth2/v1/certs"
		cred.Client_x509_cert_url = "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-6ewj4%40ibron-b5f1c.iam.gserviceaccount.com"
		cred.Universe_domain = "googleapis.com"

		jsonData, err := json.Marshal(cred)
		if err != nil {
			return nil, err
		}

		tempFileCred, err := os.Create("cred.json")
		if err != nil {
			return nil, err
		}

		filefile := bytes.NewReader(jsonData)
		_, err = io.Copy(tempFileCred, filefile)
		if err != nil {
			return nil, err
		}

		opt := option.WithCredentialsFile(tempFileCred.Name())
		defer os.Remove(tempFileCred.Name())
		app, err := firebase.NewApp(context.Background(), nil, opt)
		if err != nil {
			log.Println(err.Error())
			return nil, err
		}

		client, err := app.Storage(context.TODO())
		if err != nil {
			return nil, err
		}

		bucketHandle, err := client.Bucket("ibron-b5f1c.appspot.com")
		if err != nil {
			return nil, err
		}

		f, err := os.Open(tempFile.Name())
		if err != nil {
			return nil, err
		}
		defer os.Remove(tempFile.Name())
		objectHandle := bucketHandle.Object(tempFile.Name())

		writer := objectHandle.NewWriter(context.Background())

		writer.ObjectAttrs.Metadata = map[string]string{"firebaseStorageDownloadTokens": id}

		defer writer.Close()

		if _, err := io.Copy(writer, f); err != nil {
			return nil, err
		}

		if name == "thumbnails" {
			resp.Thumbnail = url.Url
		}

		defer os.Remove(tempFile.Name())
		i++
	}

	return &resp, nil
}

func UploadFile(file *multipart.FileHeader, id string) (string, error) {

	var (
		url  string
		resp string
	)

	if id == "" {
		id = uuid.New().String()
	}

	url = fmt.Sprintf("https://firebasestorage.googleapis.com/v0/b/ibron-b5f1c.appspot.com/o/%s?alt=media&token=%s", id, id)
	resp = url

	userPhoto, err := file.Open()
	tempFile, err := os.Create(id)
	if err != nil {
		return "", err
	}

	_, err = io.Copy(tempFile, userPhoto)
	if err != nil {
		return "", err
	}

	defer tempFile.Close()

	var cred Cred
	cred.Type = "service_account"
	cred.Project_id = "ibron-b5f1c"
	cred.Private_key_id = "d68d475cb1dd5843b149d04e6828eceec6459224"
	cred.Private_key = "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC+x+oZMne+L3Mm\nHW5mhubySrIZeIKf1OU9D3Hp1Y5zRtgIHTEuRfoeFIPgyP/whqeVP8mOMaEhB/0A\nV2Y/5mLLXtbquE+2F3qDEMofW+8dnXs2o2GIRiIIO2NZkMDuH3zlSbVQGRk3K6nn\n5RTNw+VSDqR8lw8HAFKOuV6ZgfdQOWQ3pt+v5HVO+4LBi6MQO9TicTIJDwRhoHmV\n+Tzoi7deMToYES9jGNH/XUGggMTLpsYb143Y4ycczuKlOOjNxm8QEgplyvXnf2lb\nWxZ+iU4kzWw1Ug1Vs0zNoB631FIEtJWmTwpBbEXDYxW804IunHO5oMZihdhT2WN0\nf64asTC1AgMBAAECggEAFsQ7FK4v2W+GpYACHnSPR08wITd/GQ06HPcEoEO2l92W\npqM7LPs3TZMjqLW7yyt6gKZHynL0OEGgiMOlRU9cL0o+SezcQE3ddhoRELxzFLzo\nCP1mxHEgGwzhPn02dMQnrqIE483W8z0/7ZcqDCMKMOhj6yVIqwx4DaWj2xxGE9nM\nrjn1N+WgFIxl55KIdkjW6p8EG0m2TuanuZnKxUN3CLJA9K+WjUUpWOYVgLC6heKT\nbni6CK62KcIYtxign/Rw4MuA42cT5XQfpxkt73gK/VtAQXkIpNc1keO+7rUwbZu1\ntBu3chZ5Sd8eidbcfWm4pW34yNRyvSslKLW1i2RRsQKBgQDjYqcNl7RfnuvMcfdu\nkvx5qecdkYs7preMxy1muRXkqUu8+owjFrwwyzlh4lnK8BNW7GA+0sfIp7Y3DNWy\nfxGqBJO2Q9LcmHCzyUEsLirQmJq7sw8vJEi/mhEF78+hSHLb2Q+mrimeXNKWUrh1\nMIFQgKqTBn6bXOP6lh+Qn5G8ZQKBgQDWygdw8keJ+ctNAiRQFumz5aLPj6t4Ydfo\nX0oLbKwX8uzQ6B0aefw51FsuK9nMycpNMsiHQTL3dnAo0aKiVVDuFbwXM9WjG3w7\nWmkpbWVsTwDO6S6YDKUfb02zg4ce10YPmh7f7YJTEWlTsTCJpAZdxP3SDKqoVa9O\n9wL9B9cWEQKBgQDgd6rhyDjPjFrKYFcaWQ/UKt10WxFPuazI1HcFSQFL0bRqwgtF\nOFRe9r+W2ZQ6ufTdgksguYNvmeBPR+RNaRNO+2W2MA4gpnvbf3lO2+R4N63ITWNX\n7kHCo5zad3pIhPSeAxFoQdXj904MB20AgvmlNt8sdOTXCkQkQ761TlDX4QKBgDCe\nrM6JWV5HOBjnejbS6PX67j7Tw7nNVCVapJUvdUk6iRbE5AmhQVIJ2tX5wSSVLQEK\nlM/hF1ti6QKkaY8bhuVN8DTEXAC+8QEydwmUpNe+jm1H5W8vtZUHCeyvwj9d9ZME\njSadSdnp4BC81ELj6TkCpX4NtffC5ZsXWQrT4oExAoGAPB86M3woO2TkcIPIbULX\nkD5rbrzsxhWtap0AGzP4gF3Qf4irDlW7h3qSRwKwMVuvfO8yOHAIPhAaficihmPF\nI9wWhD5+P9Eb4+7PXbOSNQKo9fTlHkVpzO5iYXhGo8emqnzoYKTbYtAXP0YNmARJ\n9JM8suTztt8F49n0b+/D5+E=\n-----END PRIVATE KEY-----\n"
	cred.Client_email = "firebase-adminsdk-6ewj4@ibron-b5f1c.iam.gserviceaccount.com"
	cred.Client_id = "108239552011752485643"
	cred.Auth_uri = "https://accounts.google.com/o/oauth2/auth"
	cred.Token_uri = "https://oauth2.googleapis.com/token"
	cred.Auth_provider_x509_cert_url = "https://www.googleapis.com/oauth2/v1/certs"
	cred.Client_x509_cert_url = "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-6ewj4%40ibron-b5f1c.iam.gserviceaccount.com"
	cred.Universe_domain = "googleapis.com"

	jsonData, err := json.Marshal(cred)
	if err != nil {
		return "", err
	}

	tempFileCred, err := os.Create("cred.json")
	if err != nil {
		return "", err
	}

	fileFile := bytes.NewReader(jsonData)
	_, err = io.Copy(tempFileCred, fileFile)
	if err != nil {
		return "", err
	}

	opt := option.WithCredentialsFile(tempFileCred.Name())
	defer os.Remove(tempFileCred.Name())
	app, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		log.Println(err.Error())
		return "", err
	}

	client, err := app.Storage(context.TODO())
	if err != nil {
		return "", err
	}

	bucketHandle, err := client.Bucket("ibron-b5f1c.appspot.com")
	if err != nil {
		return "", err
	}

	f, err := os.Open(tempFile.Name())
	if err != nil {
		return "", err
	}

	objectHandle := bucketHandle.Object(tempFile.Name())

	writer := objectHandle.NewWriter(context.Background())

	writer.ObjectAttrs.Metadata = map[string]string{"firebaseStorageDownloadTokens": id}

	defer writer.Close()

	if _, err := io.Copy(writer, f); err != nil {
		return "", err
	}

	return resp, nil
}

func DeleteFile(id string) error {
	var cred Cred
	cred.Type = "service_account"
	cred.Project_id = "ibron-b5f1c"
	cred.Private_key_id = "d68d475cb1dd5843b149d04e6828eceec6459224"
	cred.Private_key = "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC+x+oZMne+L3Mm\nHW5mhubySrIZeIKf1OU9D3Hp1Y5zRtgIHTEuRfoeFIPgyP/whqeVP8mOMaEhB/0A\nV2Y/5mLLXtbquE+2F3qDEMofW+8dnXs2o2GIRiIIO2NZkMDuH3zlSbVQGRk3K6nn\n5RTNw+VSDqR8lw8HAFKOuV6ZgfdQOWQ3pt+v5HVO+4LBi6MQO9TicTIJDwRhoHmV\n+Tzoi7deMToYES9jGNH/XUGggMTLpsYb143Y4ycczuKlOOjNxm8QEgplyvXnf2lb\nWxZ+iU4kzWw1Ug1Vs0zNoB631FIEtJWmTwpBbEXDYxW804IunHO5oMZihdhT2WN0\nf64asTC1AgMBAAECggEAFsQ7FK4v2W+GpYACHnSPR08wITd/GQ06HPcEoEO2l92W\npqM7LPs3TZMjqLW7yyt6gKZHynL0OEGgiMOlRU9cL0o+SezcQE3ddhoRELxzFLzo\nCP1mxHEgGwzhPn02dMQnrqIE483W8z0/7ZcqDCMKMOhj6yVIqwx4DaWj2xxGE9nM\nrjn1N+WgFIxl55KIdkjW6p8EG0m2TuanuZnKxUN3CLJA9K+WjUUpWOYVgLC6heKT\nbni6CK62KcIYtxign/Rw4MuA42cT5XQfpxkt73gK/VtAQXkIpNc1keO+7rUwbZu1\ntBu3chZ5Sd8eidbcfWm4pW34yNRyvSslKLW1i2RRsQKBgQDjYqcNl7RfnuvMcfdu\nkvx5qecdkYs7preMxy1muRXkqUu8+owjFrwwyzlh4lnK8BNW7GA+0sfIp7Y3DNWy\nfxGqBJO2Q9LcmHCzyUEsLirQmJq7sw8vJEi/mhEF78+hSHLb2Q+mrimeXNKWUrh1\nMIFQgKqTBn6bXOP6lh+Qn5G8ZQKBgQDWygdw8keJ+ctNAiRQFumz5aLPj6t4Ydfo\nX0oLbKwX8uzQ6B0aefw51FsuK9nMycpNMsiHQTL3dnAo0aKiVVDuFbwXM9WjG3w7\nWmkpbWVsTwDO6S6YDKUfb02zg4ce10YPmh7f7YJTEWlTsTCJpAZdxP3SDKqoVa9O\n9wL9B9cWEQKBgQDgd6rhyDjPjFrKYFcaWQ/UKt10WxFPuazI1HcFSQFL0bRqwgtF\nOFRe9r+W2ZQ6ufTdgksguYNvmeBPR+RNaRNO+2W2MA4gpnvbf3lO2+R4N63ITWNX\n7kHCo5zad3pIhPSeAxFoQdXj904MB20AgvmlNt8sdOTXCkQkQ761TlDX4QKBgDCe\nrM6JWV5HOBjnejbS6PX67j7Tw7nNVCVapJUvdUk6iRbE5AmhQVIJ2tX5wSSVLQEK\nlM/hF1ti6QKkaY8bhuVN8DTEXAC+8QEydwmUpNe+jm1H5W8vtZUHCeyvwj9d9ZME\njSadSdnp4BC81ELj6TkCpX4NtffC5ZsXWQrT4oExAoGAPB86M3woO2TkcIPIbULX\nkD5rbrzsxhWtap0AGzP4gF3Qf4irDlW7h3qSRwKwMVuvfO8yOHAIPhAaficihmPF\nI9wWhD5+P9Eb4+7PXbOSNQKo9fTlHkVpzO5iYXhGo8emqnzoYKTbYtAXP0YNmARJ\n9JM8suTztt8F49n0b+/D5+E=\n-----END PRIVATE KEY-----\n"
	cred.Client_email = "firebase-adminsdk-6ewj4@ibron-b5f1c.iam.gserviceaccount.com"
	cred.Client_id = "108239552011752485643"
	cred.Auth_uri = "https://accounts.google.com/o/oauth2/auth"
	cred.Token_uri = "https://oauth2.googleapis.com/token"
	cred.Auth_provider_x509_cert_url = "https://www.googleapis.com/oauth2/v1/certs"
	cred.Client_x509_cert_url = "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-6ewj4%40ibron-b5f1c.iam.gserviceaccount.com"
	cred.Universe_domain = "googleapis.com"

	jsonData, err := json.Marshal(cred)
	if err != nil {
		return err
	}

	tempFileCred, err := os.Create("cred.json")
	if err != nil {
		return err
	}
	filefile := bytes.NewReader(jsonData)
	_, err = io.Copy(tempFileCred, filefile)
	if err != nil {
		return err
	}

	// Initialize a context and Google Cloud Storage client
	ctx := context.Background()
	client, err := storage.NewService(ctx, option.WithCredentialsFile(tempFileCred.Name()))
	defer tempFileCred.Close()
	defer os.Remove("cred.json")
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	// Bucket name and object path to delete
	bucketName := "ibron-b5f1c.appspot.com"
	objectPath := id

	// Delete the object
	err = client.Objects.Delete(bucketName, objectPath).Do()
	if err != nil {
		fmt.Println(err.Error())
		log.Fatalf("Failed to delete object: %v", err)
		
	}

	fmt.Printf("Object %s deleted successfully from bucket %s\n", objectPath, bucketName)
	return nil
}

func FileToBytes(filename string) ([]byte, error) {
	// Read the entire file into memory
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func CreateMultipartFile(filePath string) (*multipart.FileHeader, *bytes.Buffer, error) {
	// Open the file to be uploaded
	file, err := os.Open(filePath)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to open file: %v", err)
	}
	defer file.Close()

	// Create a buffer to write our multipart form
	var buffer bytes.Buffer
	writer := multipart.NewWriter(&buffer)

	// Create a form field writer for the file field
	part, err := writer.CreateFormFile("file", filepath.Base(filePath))
	if err != nil {
		return nil, nil, fmt.Errorf("failed to create form file: %v", err)
	}

	// Copy the file content to the multipart form writer
	if _, err := io.Copy(part, file); err != nil {
		return nil, nil, fmt.Errorf("failed to copy file content: %v", err)
	}

	// Close the multipart writer
	if err := writer.Close(); err != nil {
		return nil, nil, fmt.Errorf("failed to close writer: %v", err)
	}

	// Create a new multipart reader from the buffer
	reader := multipart.NewReader(&buffer, writer.Boundary())

	// Read the form (as if it was received by a server)
	form, err := reader.ReadForm(int64(buffer.Len()))
	if err != nil {
		return nil, nil, fmt.Errorf("failed to read form: %v", err)
	}

	// Get the file header from the form
	fileHeaders := form.File["file"]
	if len(fileHeaders) == 0 {
		return nil, nil, fmt.Errorf("no file found in form")
	}

	return fileHeaders[0], &buffer, nil
}

//
//func ConvertFileToMultipartForm(file *os.File) (*multipart.Form, error) {
//	// Create a new multipart form
//	form := &multipart.Form{}
//
//	fileInfo, err := file.Stat()
//	if err != nil {
//		return nil, err
//	}
//
//	fileHeader := &multipart.FileHeader{
//		Filename: fileInfo.Name(),
//		Size:     fileInfo.Size(),
//		Header: ,
//	}
//
//	fileHeader.Open()
//
//	fmt.Println("file", fileHeader.Filename, "size", fileHeader.Size)
//	fmt.Println("file", fileHeader)
//	form.Value["file"] = append(form.Value["file"], fileHeader.Filename)
//	form.File["file"] = append(form.File["file"], fileHeader)
//	fmt.Println(form.File["file"])
//
//	return form, nil
//}
//
//func FileToMultipartForm(file *os.File) (*multipart.Form, error) {
//	// Create a new buffer to store the multipart form data
//	var buffer bytes.Buffer
//	writer := multipart.NewWriter(&buffer)
//
//	// Create a new part in the multipart form for the file
//	part, err := writer.CreateFormFile("file", file.Name())
//	if err != nil {
//		return nil, err
//	}
//
//	// Copy the file contents to the part
//	_, err = io.Copy(part, file)
//	if err != nil {
//		return nil, err
//	}
//
//	// Close the writer to finish writing the multipart form
//	writer.Close()
//
//	// Parse the multipart form from the buffer
//	form, err := http.NewRequest("POST", "http://example.com/upload", &buffer)
//	if err != nil {
//		return nil, err
//	}
//	form.Header.Set("Content-Type", writer.FormDataContentType())
//
//	return 	form.ParseMultipartForm(int64(buffer.Len())),nil
//}
