package jwt

import (
	"github.com/golang-jwt/jwt"
	"time"
)

var SignKey = []byte("asd@#lskd2!aw32k34242WSASdsk32")

const (
	AccessExpireTime  = time.Minute * 20
	RefreshExpireTime = time.Hour * 24
)

func GenerateJWT(m map[interface{}]interface{}) (string, string, error) {
	accessToken := jwt.New(jwt.SigningMethodHS256)
	refreshToken := jwt.New(jwt.SigningMethodHS256)

	aClaims := accessToken.Claims.(jwt.MapClaims)
	rClaims := refreshToken.Claims.(jwt.MapClaims)

	for key, value := range m {
		aClaims[key.(string)] = value
		rClaims[key.(string)] = value
	}

	aClaims["exp"] = time.Now().Add(AccessExpireTime).Unix()
	aClaims["iat"] = time.Now().Unix()

	rClaims["exp"] = time.Now().Add(RefreshExpireTime).Unix()
	rClaims["iat"] = time.Now().Unix()

	accessToken.Claims = aClaims
	refreshToken.Claims = rClaims

	accessTokenStr, err := accessToken.SignedString(SignKey)
	if err != nil {
		return "", "", err
	}

	refreshTokenStr, err := refreshToken.SignedString(SignKey)
	if err != nil {
		return "", "", err
	}

	return accessTokenStr, refreshTokenStr, nil
}
