package handler

import (
	"fmt"
	"ibron/models"
	"ibron/pkg/helper"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/skip2/go-qrcode"
	"github.com/spf13/cast"
)

// Create Requests godoc
// @ID create_requests
// @Router /ibron/api/v1/request [POST]
// @Summary Create Requests
// @Description Create Requests
// @Tags Request
// @Accept json
// @Procedure json
// @Param Request body models.RequestCreate true "CreateRequestRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateRequest(c *gin.Context) {
	var (
		requestCreate models.RequestCreate
	)

	err := c.ShouldBindJSON(&requestCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Request Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	if !helper.IsValidUUID(requestCreate.ServiceID) {
		h.logger.Error("Invalid Service ID!")
		c.JSON(http.StatusBadRequest, models.ErrorMessage{Phone: "Invalid Service ID"})
		return
	}

	if !helper.IsValidUUID(requestCreate.UserID) && requestCreate.UserID != "" {
		h.logger.Error("Invalid User ID!")
		c.JSON(http.StatusBadRequest, models.ErrorMessage{Phone: "Invalid User ID"})
		return
	}

	serviceData, err := h.storage.Service().GetByID(c.Request.Context(), &models.ServicePrimaryKey{Id: requestCreate.ServiceID})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Service.GetByID")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	businessMerchant, err := h.storage.BusinessMerchant().GetByID(c.Request.Context(), &models.BusinessMerchantPrimaryKey{Id: serviceData.BusinessMerchantID})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error BusinessMerchant.GetByID")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	requestCreate.MerchantID = businessMerchant.MerchantID

	resp, err := h.storage.Request().Create(c.Request.Context(), &requestCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Request.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	// send sms to businessMerchant
	service, err := h.storage.Service().GetByID(c.Request.Context(), &models.ServicePrimaryKey{Id: resp.ServiceID})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Service.GetByID")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	message := "Sizning " + service.Name + " ga yangi buyurtma tushdi!"

	err = helper.SendSms(businessMerchant.Phone1, message)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "Send SMS Error!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	// sms count
	err = h.storage.SmsUser().Create(c.Request.Context(), &models.SmsCreate{
		SenderID: "0deeec11-7433-41dc-a114-4c7ffa77c737",
		Name:     "ibron",
		Count:    1,
	})
	if err != nil {
		h.logger.Error(err.Error() + " error SmsUser.Create")
		c.JSON(http.StatusInternalServerError, "Server error")
		return
	}

	h.logger.Info("Create Request Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID Requests godoc
// @ID get_by_id_requests
// @Router /ibron/api/v1/request/{id} [GET]
// @Summary Get By ID Requests
// @Description Get By ID Requests
// @Tags Request
// @Accept json
// @Procedure json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdRequest(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	request, err := h.storage.Request().GetByID(c.Request.Context(), &models.RequestPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID Request Response!")
	c.JSON(http.StatusOK, request)
}

// Get Requests Report godoc
// @ID get_requests_report
// @Router /ibron/api/v1/report-request [GET]
// @Summary Get Requests Report
// @Description Get Requests Report
// @Tags Request
// @Accept json
// @Procedure json
// @Param merchant_id query string false "merchant_id"
// @Param date query string false "date"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetRequestsReport(c *gin.Context) {
	id := c.Query("merchant_id")
	date := c.Query("date")
	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	if date > time.Now().String() {
		h.logger.Error("is invalid date!")
		c.JSON(http.StatusBadRequest, "invalid date")
		return
	}

	request, err := h.storage.Request().Report(c.Request.Context(), &models.MerchantPrimaryKey{Id: id, Date: date})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.Report!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Get Request Report Response!")
	c.JSON(http.StatusOK, request)
}

// Get Requests Report For Month godoc
// @ID get_requests_report_for_month
// @Router /ibron/api/v1/report-for-month-request/{id} [GET]
// @Summary Get Requests Report
// @Description Get Requests Report
// @Tags Request
// @Accept json
// @Procedure json
// @Param merchant_id query string false "merchant_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetRequestsReportForMonth(c *gin.Context) {
	id := c.Query("merchant_id")
	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	request, err := h.storage.Request().ReportForMonth(c.Request.Context(), &models.MerchantPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.ReportForMonth!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Get Request Report For Month Response!")
	c.JSON(http.StatusOK, request)
}

// Get Report godoc
// @ID get_report
// @Router /ibron/api/v1/report [GET]
// @Summary Get Requests Report
// @Description Get Requests Report
// @Tags Request
// @Accept json
// @Procedure json
// @Param merchant_id query string false "merchant_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetReport(c *gin.Context) {
	merchantID := c.Query("merchant_id")
	if !helper.IsValidUUID(merchantID) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid merchantID")
		return
	}

	request, err := h.storage.Request().ReportForMonth(c.Request.Context(), &models.MerchantPrimaryKey{Id: merchantID})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.ReportForMonth!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	report, err := h.storage.Request().ReportDiagram(c.Request.Context(), &models.MerchantPrimaryKey{Id: merchantID})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.ReportDiagram!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	for _, list := range report.ReportList {
		request.Labels = append(request.Labels, &list.Month)
		request.Data = append(request.Data, &list.Data)
	}

	h.logger.Info("Get Request Report For Month Response!")
	c.JSON(http.StatusOK, request)
}

// Get Report Diagram godoc
// @ID get_requests_report_diagram
// @Router /ibron/api/v1/report-diagram-request/{id} [GET]
// @Summary Get Requests Report
// @Description Get Requests Report
// @Tags Request
// @Accept json
// @Procedure json
// @Param merchant_id query string false "merchant_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetReportDiagram(c *gin.Context) {
	id := c.Query("merchant_id")
	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	request, err := h.storage.Request().ReportDiagram(c.Request.Context(), &models.MerchantPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.ReportDiagram!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Get Report Diagram Response!")
	c.JSON(http.StatusOK, request)
}

// GetList Request By service id godoc
// @ID get_list_requests_by_service_id
// @Router /ibron/api/v1/requests/service [GET]
// @Summary Get List Requests by service id
// @Description Get List Requests by service id
// @Tags Request
// @Accept json
// @Procedure json
// @Param date query string false "date"
// @Param service_id query string false "service_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListApprovedRequestsByServiceId(c *gin.Context) {

	resp, err := h.storage.Request().GetListApprovedRequestsByServiceId(c.Request.Context(), &models.RequestGetListRequest{
		Offset:    0,
		Limit:     1000,
		ServiceId: c.Query("service_id"),
		Date:      c.Query("date"),
	})
	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}
	h.logger.Info("GetListRequest Response!")
	c.JSON(http.StatusOK, resp)
}

// GetList Requests godoc
// @ID get_list_requests
// @Router /ibron/api/v1/requests [GET]
// @Summary Get List Requests
// @Description Get List Requests
// @Tags Request
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param service_id query string false "service_id"
// @Param user_id query string false "user_id"
// @Param merchant_id query string false "merchant_id"
// @Param status query string false "status"
// @Param date query string false "date"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListRequest(c *gin.Context) {
	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListRequest INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListRequest INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	serviceId := c.Query("service_id")
	merchantId := c.Query("merchant_id")
	userId := c.Query("user_id")
	status := c.Query("status")
	date := c.Query("date")

	resp, err := h.storage.Request().GetList(c.Request.Context(), &models.RequestGetListRequest{
		Offset:     offset,
		Limit:      limit,
		ServiceId:  serviceId,
		MerchantID: merchantId,
		UserId:     userId,
		Type:       status,
		Date:       date,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListRequest Response!")
	c.JSON(http.StatusOK, resp)
}

// GetList Free Time godoc
// @ID get_list_free_time
// @Router /ibron/api/v1/requests/free-times [GET]
// @Summary Get List Free Time
// @Description Get List Free Time
// @Tags Request
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param service_id query string false "service_id"
// @Param date query string false "date"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListFreeTime(c *gin.Context) {

	var (
		resp models.FreeTimeResponse
		free bool
	)

	service_id := c.Query("service_id")
	date, err := time.Parse("2006-01-02", c.Query("date"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.GetList.Time.Parse!")
		c.JSON(400, "Wrong date format!")
		return
	}

	service, err := h.storage.Service().GetByID(c.Request.Context(), &models.ServicePrimaryKey{Id: service_id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Service.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	working_hours, err := h.storage.WorkingHours().GetByID(c.Request.Context(), &models.WorkingHoursPrimaryKey{ServiceId: service_id, Day: helper.EngToRuWeekday(date.Weekday().String())})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.WorkingHours.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	layout := "15:04"

	if working_hours.StartTime == "24:00" || working_hours.EndTime == "24:00" {
		working_hours.StartTime = "00:00"
		working_hours.EndTime = "00:00"
	}

	start, err := time.Parse(layout, working_hours.StartTime)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.GetList.Start.Time.Parse!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	end, err := time.Parse(layout, working_hours.EndTime)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.GetList.End.Time.Parse!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	s := start.Hour()
	e := end.Hour()

	if e < s {
		e += 24
	}

	if e == 0 {
		e = 24
	}

	openHour := cast.ToFloat64(s)
	closeHour := cast.ToFloat64(e)

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		h.logger.Error("error is while loading location" + err.Error())
		c.JSON(http.StatusInternalServerError, "server error")
		return
	}
	currentDate := time.Now().In(timeZone).Format("2006-01-02")

	for openHour < closeHour {
		if date.String() >= currentDate {
			free, err = h.storage.WorkingHours().GetFreeTimeByServiceId(c.Request.Context(), &models.FreeTime{
				Date:      date.Format("2006-01-02"),
				StartTime: start.Format("15:04"),
				EndTime:   start.Add(time.Duration(int64(service.Duration)) * time.Minute).Format("15:04"),
				ServiceId: service_id,
			})
			if err != nil {
				h.logger.Error(err.Error() + "  :  " + "storage.WorkingHours.GetFreeTimeByServiceId!")
				c.JSON(http.StatusInternalServerError, "Server Error!")
				return
			}

			timeZone, err := time.LoadLocation("Asia/Tashkent")
			if err != nil {
				h.logger.Error("error is while loading location" + err.Error())
				c.JSON(http.StatusInternalServerError, "server error")
				return
			}
			currentTime := time.Now().In(timeZone).Format("15:04")
			currentDate := time.Now().In(timeZone).Format("2006-01-02")

			timeNow, err := strconv.Atoi(currentTime[:2])
			if err != nil {
				h.logger.Error("error is while parsing" + err.Error())
				c.JSON(http.StatusInternalServerError, "server error")
				return
			}

			startTime := start.Format("15:04")
			timeStart, err := strconv.Atoi(startTime[:2])
			if err != nil {
				h.logger.Error("error is while parsing end time " + err.Error())
				c.JSON(http.StatusInternalServerError, "server error")
				return
			}

			if openHour >= 24 {
				if timeNow <= timeStart {
					free = false
				}
			} else if timeNow >= timeStart && currentDate == date.Format("2006-01-02") {
				free = false
			}
		}

		resp.Times = append(resp.Times, &models.Time{
			StartTime: start.Format("15:04"),
			EndTime:   start.Add(time.Duration(int64(service.Duration)) * time.Minute).Format("15:04"),
			Status:    free,
		})

		start = start.Add(time.Duration(int64(service.Duration)) * time.Minute)
		openHour += service.Duration / 60
	}

	h.logger.Info("GetListFreeTime Response!")
	c.JSON(http.StatusOK, resp)
}

// GetList Finished Requests godoc
// @ID get_list_finished_requests
// @Router /ibron/api/v1/finished-requests [GET]
// @Summary Get List Finished Requests
// @Description Get List Finished Requests
// @Tags Request
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param user_id query string false "user_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListFinishedRequest(c *gin.Context) {
	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListRequest INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListRequest INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	resp, err := h.storage.Request().GetListFinishedRequests(c.Request.Context(), &models.RequestGetListRequest{
		Offset: offset,
		Limit:  limit,
		UserId: c.Query("user_id"),
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.GetListFinishedRequest!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	for k, request := range resp.Requests {
		service, err := h.storage.Service().GetByID(c.Request.Context(), &models.ServicePrimaryKey{Id: request.ServiceID})
		if err != nil {
			h.logger.Error(err.Error() + "  :  " + "storage.Request.GetListFinishedRequest.Service.GetById!")
			c.JSON(http.StatusInternalServerError, "Server Error!")
			return
		}
		resp.Requests[k].Service = service
	}

	h.logger.Info("GetListFinishedRequest Response!")
	c.JSON(http.StatusOK, resp)
}

// GetList Approved Requests godoc
// @ID get_list_approved_requests
// @Router /ibron/api/v1/approved-requests [GET]
// @Summary Get List Approved Requests
// @Description Get List Approved Requests
// @Tags Request
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param user_id query string false "user_id"
// @Param service_id query string false "service_id"
// @Param date query string false "date"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListApprovedRequest(c *gin.Context) {
	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListRequest INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListRequest INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	resp, err := h.storage.Request().GetListApprovedRequests(c.Request.Context(), &models.RequestGetListRequest{
		Offset:    offset,
		Limit:     limit,
		UserId:    c.Query("user_id"),
		ServiceId: c.Query("service_id"),
		Date:      c.Query("date"),
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.GetListApprovedRequest!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListApprovedRequest Response!")
	c.JSON(http.StatusOK, resp)
}

// Update Request Status Canceled godoc
// @ID update_request_status_canceled
// @Router /ibron/api/v1/request-status-canceled-client/{id} [PUT]
// @Summary Update Request Status
// @Description Update Request Status
// @Tags Request
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UpdateRequestStatusCancel(c *gin.Context) {
	var (
		id            = c.Param("id")
		requestUpdate models.UpdateStatus
	)

	if !helper.IsValidUUID(id) {
		h.logger.Error("is invalid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	requestData, err := h.storage.Request().GetByID(c.Request.Context(), &models.RequestPrimaryKey{
		Id: id,
	})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	location, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		h.logger.Error("cannot load location" + err.Error())
		return
	}

	currentTime := time.Now().In(location)

	startTime, err := time.Parse("15:04", requestData.StartTime)
	if err != nil {
		h.logger.Error("cannot parse time" + err.Error())
		return
	}

	date := currentTime.Format("2006-01-02")
	timeN := startTime.Hour()
	timeN = timeN - currentTime.Hour()
	if timeN < 2.0 && requestData.Status == "approved" && requestData.Date == date {
		h.logger.Info("cannot cancel, it is approved!")
		c.JSON(http.StatusBadRequest, "cannot cancel, it is before 2 hours!")
		return
	}

	requestUpdate.ID = id
	requestUpdate.Status = "canceled"
	rowsAffected, err := h.storage.Request().UpdateStatus(c.Request.Context(), &requestUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.Update!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if rowsAffected <= 0 {
		h.logger.Error("storage.Request.Update!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
		return
	}

	resp, err := h.storage.Request().GetByID(c.Request.Context(), &models.RequestPrimaryKey{Id: requestUpdate.ID})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Update Request Status Successfully!")
	c.JSON(http.StatusAccepted, resp)
}

// Update Status By Merchant ID godoc
// @ID update_status_by_merchant_id
// @Router /ibron/api/v1/request-status-canceled-merchant/{id} [PUT]
// @Summary Update Request Status
// @Description Update Request Status
// @Tags Request
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param merchant_id query string true "merchant_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UpdateStatusByMerchantID(c *gin.Context) {
	var (
		id            = c.Param("id")
		merchantID    = c.Param("merchant_id")
		requestUpdate models.UpdateStatus
	)

	if !helper.IsValidUUID(id) {
		h.logger.Error("is invalid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	requestUpdate.ID = id
	requestUpdate.Status = "canceled"
	requestUpdate.MerchantID = merchantID
	rowsAffected, err := h.storage.Request().UpdateStatus(c.Request.Context(), &requestUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.Update!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if rowsAffected <= 0 {
		h.logger.Error("storage.Request.Update!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
		return
	}

	resp, err := h.storage.Request().GetByID(c.Request.Context(), &models.RequestPrimaryKey{Id: requestUpdate.ID})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Update Request Status Successfully!")
	c.JSON(http.StatusAccepted, resp)
}

// Update Request Status Approved godoc
// @ID update_request_status_approved
// @Router /ibron/api/v1/request-status-approved/{id} [PUT]
// @Summary Update Request Status
// @Description Update Request Status
// @Tags Request
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UpdateRequestStatusApproved(c *gin.Context) {
	var (
		id            = c.Param("id")
		requestUpdate models.UpdateStatus
	)

	if !helper.IsValidUUID(id) {
		h.logger.Error("is invalid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	requestData, err := h.storage.Request().GetByID(c.Request.Context(), &models.RequestPrimaryKey{
		Id: id,
	})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}
	fmt.Println("request data", requestData)

	if requestData.Status == "canceled" && requestUpdate.Status == "approved" {
		h.logger.Info("cannot approve, it is canceled!")
		c.JSON(http.StatusBadRequest, "Unable to update data. cannot approve, it is canceled!!")
		return
	}

	isTrue, err := h.storage.Request().CheckTime(c.Request.Context(), &models.Request{
		Id:        id,
		StartTime: requestData.StartTime,
		EndTime:   requestData.EndTime,
		Date:      requestData.Date,
	})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.Check time!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if !isTrue {
		h.logger.Info("Cannot update this time period already taken!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Cannot update this time period already taken!")
		return
	}

	requestUpdate.ID = id
	requestUpdate.Status = "approved"
	rowsAffected, err := h.storage.Request().UpdateStatus(c.Request.Context(), &requestUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.Update!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if rowsAffected <= 0 {
		h.logger.Error("storage.Request.Update!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
		return
	}

	resp, err := h.storage.Request().GetByID(c.Request.Context(), &models.RequestPrimaryKey{Id: requestUpdate.ID})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Update Request Status Successfully!")
	c.JSON(http.StatusAccepted, resp)
}

// Update Schedule godoc
// @ID update_requests
// @Router /ibron/api/v1/request/{id} [PUT]
// @Summary Update Requests
// @Description Update Requests
// @Tags Request
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Request body models.RequestUpdate true "UpdateRequestRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UpdateRequest(c *gin.Context) {
	var (
		id            = c.Param("id")
		requestUpdate models.RequestUpdate
	)

	if !helper.IsValidUUID(id) {
		h.logger.Error("is invalid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	err := c.ShouldBindJSON(&requestUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Request Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	requestUpdate.Id = id
	rowsAffected, err := h.storage.Request().Update(c.Request.Context(), &requestUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.Update!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if rowsAffected <= 0 {
		h.logger.Error("storage.Request.Update!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
		return
	}

	if requestUpdate.Status == "approved" {
		date, err := time.Parse("2006-01-02", requestUpdate.Date)
		if err != nil {
			h.logger.Error(err.Error() + "  :  " + "storage.Request.Update.Time.Parse!")
			c.JSON(http.StatusInternalServerError, "Server Error!")
			return
		}
		_, err = h.storage.Schedule().Create(c.Request.Context(), &models.ScheduleCreate{
			ServiceID: requestUpdate.ServiceID,
			Day:       date.Weekday().String(),
			StartTime: requestUpdate.StartTime,
			EndTime:   requestUpdate.EndTime,
		})
		if err != nil {
			h.logger.Error(err.Error() + "  :  " + "storage.Request.Update.Schedule.Create!")
			c.JSON(http.StatusInternalServerError, "Server Error!")
			return
		}
	}

	resp, err := h.storage.Request().GetByID(c.Request.Context(), &models.RequestPrimaryKey{Id: requestUpdate.Id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Update Request Successfully!")
	c.JSON(http.StatusAccepted, resp)
}

// Delete Requests godoc
// @ID delete_requests
// @Router /ibron/api/v1/request/{id} [DELETE]
// @Summary Delete Requests
// @Description Delete Requests
// @Tags Request
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteRequest(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.Request().Delete(c.Request.Context(), &models.RequestPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Request Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}

// Approve Request godoc
// @ID get_approve_request
// @Router /ibron/api/v1/approve-request/{id} [GET]
// @Summary Get By ID Requests
// @Description Get By ID Requests
// @Tags Request
// @Accept json
// @Procedure json
// @Param request_id query string true "request_id"
// @Param longitude query string true "longitude"
// @Param latitude query string true "latitude"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) ApproveRequest(c *gin.Context) {
	requestId := c.Query("request_id")
	latitudeStr := c.Query("latitude")
	longitudeStr := c.Query("longitude")

	latitude, err := strconv.ParseFloat(latitudeStr, 64)
	if err != nil {
		h.logger.Error("cannot parse latitude")
		c.JSON(http.StatusBadRequest, "cannot parse latitude")
		return
	}

	longitude, err := strconv.ParseFloat(longitudeStr, 64)
	if err != nil {
		h.logger.Error("cannot parse longitude")
		c.JSON(http.StatusBadRequest, "cannot parse longitude")
		return
	}

	requestData, err := h.storage.Request().GetByID(c.Request.Context(), &models.RequestPrimaryKey{Id: requestId})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server error!")
		return
	}

	service, err := h.storage.Service().GetByID(c.Request.Context(), &models.ServicePrimaryKey{Id: requestData.ServiceID})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Service.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server error!")
		return
	}

	distance := helper.Haversine(service.Latitude, service.Longitude, latitude, longitude)
	if distance > 0.5 {
		h.logger.Error("you is too far")
		c.JSON(http.StatusBadRequest, "you is too far")
		return
	}

	location, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		h.logger.Error("cannot load location" + err.Error())
		return
	}
	currentTime := time.Now().In(location)
	date := currentTime.Format("2006-01-02")

	s, err := time.Parse("15:04", requestData.StartTime)
	if err != nil {
		h.logger.Error("Parse Error in Approve.Request")
		c.JSON(http.StatusBadRequest, "Bad Time")
		return
	}

	if s.After(currentTime.Add(time.Minute*30)) && date == requestData.Date {
		h.logger.Error("you is before your start time")
		c.JSON(http.StatusBadRequest, "please wait your start time")
		return
	}

	fileName := uuid.New().String() + ".png"

	err = qrcode.WriteFile(requestId, qrcode.Medium, 256, fileName)
	if err != nil {
		log.Fatalf("Failed to generate QR code: %v", err)
	}

	file, _, err := helper.CreateMultipartFile(fileName)
	if err != nil {
		log.Println("error is while creating multipart file", err.Error())
		return
	}

	url, err := helper.UploadFile(file, requestId)
	if err != nil {
		log.Println("error while uploading file to firebase", err.Error())
		return
	}

	defer os.Remove(file.Filename)

	h.logger.Info("Qrcode uploaded!")
	c.JSON(http.StatusOK, url)
}

// Confirm Request godoc
// @ID get_confirm_request
// @Router /ibron/api/v1/confirm-request [GET]
// @Summary Get By ID Requests
// @Description Get By ID Requests
// @Tags Request
// @Accept json
// @Procedure json
// @Param request_id query string true "request_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) ConfirmRequest(c *gin.Context) {
	requestId := c.Query("request_id")

	request, err := h.storage.Request().GetByID(c.Request.Context(), &models.RequestPrimaryKey{Id: requestId})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.GetById!")
		c.JSON(http.StatusInternalServerError, "Server error!")
		return
	}

	start_time, err := time.Parse("2006-01-02", request.StartTime)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "Time Parse Error!")
		c.JSON(http.StatusInternalServerError, "Server error!")
		return
	}

	if start_time.After(time.Now().Add(time.Minute * 30)) {
		h.logger.Info("your time is not yet!")
		c.JSON(http.StatusBadRequest, "Your time is not yet!")
		return
	}

	err = h.storage.Confirmed().Create(c.Request.Context(), &models.Confirmed{RequestId: requestId})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Confirmed.Create!")
		c.JSON(http.StatusInternalServerError, "Server error!")
		return
	}

	_, err = h.storage.Request().UpdateStatus(c.Request.Context(), &models.UpdateStatus{ID: requestId, Status: "finished"})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.UpdateStatus!")
		c.JSON(http.StatusInternalServerError, "Server error!")
		return
	}

	err = helper.DeleteFile(requestId)
	if err != nil {
		h.logger.Error("error while deleting file" + err.Error())
		c.JSON(http.StatusInternalServerError, "Server error!")
		return
	}

	h.logger.Info("requests qrcode confirmed")
	c.JSON(http.StatusOK, "status confirmed")
}

// Update Request Status godoc
// @ID get_update_request
// @Router /ibron/api/v1/update-request-status [PUT]
// @Summary Update Request Status
// @Description Update Request Status
// @Tags Request
// @Accept json
// @Procedure json
// @Param request_id query string true "request_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UpdateStatus(c *gin.Context) {
	request_id := c.Query("request_id")

	rowsAffected, err := h.storage.Request().UpdateStatus(c.Request.Context(), &models.UpdateStatus{ID: request_id, Status: "finished"})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.UpdateStatus!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if rowsAffected <= 0 {
		h.logger.Error("storage.Request.Update!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
		return
	}

	resp, err := h.storage.Request().GetByID(c.Request.Context(), &models.RequestPrimaryKey{Id: request_id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Request.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	c.JSON(http.StatusAccepted, resp)
}
