package handler

import (
	"ibron/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// upload Banner Files godoc
// @ID upload_banner_files
// @Router /ibron/api/v1/upload-banner-files [POST]
// @Summary Upload Banner Files
// @Description Upload Banner Files
// @Tags Upload File
// @Accept multipart/form-data
// @Procedure json
// @Param file formData []file true "Video file to upload"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UploadBannerFiles(c *gin.Context) {

	form, err := c.MultipartForm()
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "File error")
		return
	}

	resp, _ := helper.UploadFiles(form, "banner")

	c.JSON(200, resp)

}

// upload Multiple Files godoc
// @ID upload_multiple_files
// @Router /ibron/api/v1/upload-files [POST]
// @Summary Upload Multiple Files
// @Description Upload Multiple Files
// @Tags Upload File
// @Accept multipart/form-data
// @Procedure json
// @Param file formData []file true "Video file to upload"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UploadFiles(c *gin.Context) {

	form, err := c.MultipartForm()
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "File error")
		return
	}

	files := form.File["file"]
	if len(files) == 0 {
		c.String(http.StatusBadRequest, "No images uploaded")
		return
	}

	resp, err := helper.UploadFiles(form, "service_photo")
	if err != nil {
		if err.Error() == "picture size is small" {
			h.logger.Error(err.Error() + "  :  " + "File error")
			c.JSON(http.StatusBadRequest, "picture size is small")
			return
		}

		h.logger.Error(err.Error() + "  :  " + "File error")
		return
	}

	form.File["file"] = files[:1]
	respThumbnail, err := helper.UploadFiles(form, "thumbnails")
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "File error")
		return
	}

	resp.Thumbnail = respThumbnail.Thumbnail

	c.JSON(200, resp)

}

// upload Photo  godoc
// @ID upload_multiple_photo
// @Router /ibron/api/v1/upload-photo [POST]
// @Summary Upload  Photo
// @Description Upload  Photo
// @Tags Upload Photo
// @Accept multipart/form-data
// @Procedure json
// @Param file formData []file true "Photo to upload"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UploadPhotos(c *gin.Context) {

	form, err := c.MultipartForm()
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "File error")
		return
	}

	resp, err := helper.UploadFiles(form,"thumbnails")
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "File error")
		return
	}

	c.JSON(200, resp)
}
