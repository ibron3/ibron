package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"ibron/models"
	"ibron/pkg/helper"
)

// Create Amenity godoc
// @ID create_Amenities
// @Router /ibron/api/v1/amenity [POST]
// @Summary Create Amenity
// @Description Create Amenity
// @Tags Amenity
// @Accept json
// @Procedure json
// @Param user body models.AmenityCreate true "CreateAmenityRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateAmenity(c *gin.Context) {
	var (
		createAmenity models.AmenityCreate
	)

	err := c.ShouldBindJSON(&createAmenity)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Amenity Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	resp, err := h.storage.Amenity().Create(c.Request.Context(), &createAmenity)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Amenity.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create User Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID Amenity godoc
// @ID get_by_id_Amenities
// @Router /ibron/api/v1/amenity/{id} [GET]
// @Summary Get By ID Amenity
// @Description Get By ID Amenity
// @Tags Amenity
// @Accept json
// @Procedure json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdAmenity(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.storage.Amenity().GetByID(c.Request.Context(), &models.AmenityPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Amenity.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID Amenity Response!")
	c.JSON(http.StatusOK, resp)
}

// GetList Amenity godoc
// @ID get_list_Amenities
// @Router /ibron/api/v1/amenity [GET]
// @Summary Get List Amenity
// @Description Get List Amenity
// @Tags Amenity
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param filter query string false "filter"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListAmenity(c *gin.Context) {
	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListAmenity INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListAmenity INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	filter := c.Query("filter")
	search := c.Query("search")

	resp, err := h.storage.Amenity().GetList(c.Request.Context(), &models.AmenityGetListRequest{
		Offset: offset,
		Limit:  limit,
		Filter: filter,
		Search: search,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Amenity.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListAmenity Response!")
	c.JSON(http.StatusOK, resp)
}

// Update Amenity godoc
// @ID update_Amenities
// @Router /ibron/api/v1/amenity/{id} [PUT]
// @Summary Update Amenity
// @Description Update Amenity
// @Tags Amenity
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Branch body models.AmenityUpdate true "UpdateAmenityRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UpdateAmenity(c *gin.Context) {
	var (
		id            = c.Param("id")
		updateAmenity models.AmenityUpdate
	)

	if !helper.IsValidUUID(id) {
		h.logger.Error("is invalid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	err := c.ShouldBindJSON(&updateAmenity)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Amenity Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	updateAmenity.Id = id
	rowsAffected, err := h.storage.Amenity().Update(c.Request.Context(), &updateAmenity)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Amenity.Update!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if rowsAffected <= 0 {
		h.logger.Error("storage.Amenity.Update!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
		return
	}

	resp, err := h.storage.Amenity().GetByID(c.Request.Context(), &models.AmenityPrimaryKey{Id: updateAmenity.Id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Amenity.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Update Amenity Successfully!")
	c.JSON(http.StatusAccepted, resp)
}

// Delete Amenity godoc
// @ID delete_Amenities
// @Router /ibron/api/v1/amenity/{id} [DELETE]
// @Summary Delete Amenity
// @Description Delete Amenity
// @Tags Amenity
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteAmenity(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.Amenity().Delete(c.Request.Context(), &models.AmenityPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Amenity.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Amenity Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}
