package handler

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"

	"ibron/models"
	"ibron/pkg/helper"
)

// Create Favorites godoc
// @ID create_Favorites
// @Router /ibron/api/v1/favorite [POST]
// @Summary Create Favorites
// @Description Create Favorites
// @Tags Favorite
// @Accept json
// @Procedure json
// @Param user body models.FavoriteCreate true "CreateFavoriteRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateFavorite(c *gin.Context) {
	var (
		createFavorite models.FavoriteCreate
	)

	err := c.ShouldBindJSON(&createFavorite)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Favorite Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	exist, err := h.storage.Favorite().CheckForExisting(c.Request.Context(), &models.Favorite{
		UserId:    createFavorite.UserId,
		ServiceId: createFavorite.ServiceId,
	})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Favorite.CheckForExistence")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	fmt.Println("exist", exist)
	if !exist{
		h.logger.Info("Cannot add to favourite, duplicated data!")
		c.JSON(http.StatusBadRequest, "Cannot add to favourite, duplicated data!")
		return
	}

	resp, err := h.storage.Favorite().Create(c.Request.Context(), &createFavorite)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Favorite.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create User Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetList Favorites godoc
// @ID get_list_Favorites
// @Router /ibron/api/v1/favorites [GET]
// @Summary Get List Favorites
// @Description Get List Favorites
// @Tags Favorite
// @Accept json
// @Procedure json
// @Param user_id query string false "user_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListFavorite(c *gin.Context) {
	userId := c.Query("user_id")

	resp, err := h.storage.Favorite().GetListUserFavorites(c.Request.Context(), &models.UserPrimaryKey{
		Id: userId,
	})
	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Favorite.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListFavorite Response!")
	c.JSON(http.StatusOK, resp)
}

// Delete Favorites godoc
// @ID delete_Favorites
// @Router /ibron/api/v1/favorite [DELETE]
// @Summary Delete Favorites
// @Description Delete Favorites
// @Tags Favorite
// @Accept json
// @Procedure json
// @Param user_id query string true "user_id"
// @Param service_id query string true "service_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteFavorite(c *gin.Context) {
	userId := c.Query("user_id")
	serviceId := c.Query("service_id")

	if !helper.IsValidUUID(userId) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	if !helper.IsValidUUID(serviceId) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.Favorite().Delete(c.Request.Context(), &models.DeleteFavorite{UserId: userId, ServiceId: serviceId})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Favorite.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Favorite Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}
