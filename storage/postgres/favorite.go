package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"ibron/models"
	"ibron/pkg/logger"

	"github.com/jackc/pgx/v4/pgxpool"
)

type favoriteRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewFavoriteRepo(db *pgxpool.Pool, log logger.LoggerI) *favoriteRepo {
	return &favoriteRepo{
		db:  db,
		log: log,
	}
}

func (u *favoriteRepo) Create(ctx context.Context, req *models.FavoriteCreate) (string, error) {

	query := `
		INSERT INTO "user_favorites"(user_id, service_id)
		VALUES ($1, $2) 
	`

	if _, err := u.db.Exec(ctx, query,
		req.UserId,
		req.ServiceId,
	); err != nil {
		u.log.Error("insert error" + err.Error())
		return "", err
	}

	return "success", nil
}

func (u *favoriteRepo) CheckForExisting(ctx context.Context, req *models.Favorite) (bool, error) {
	var id sql.NullString

	query := `SELECT user_id FROM user_favorites WHERE user_id = $1 and service_id = $2`

	if err := u.db.QueryRow(ctx, query, req.UserId, req.ServiceId).Scan(&id); err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error is while getting user favourites" + err.Error())
		return false, err
	}
	fmt.Println("id", id)
	if id.String != "" {
		return false, nil
	}

	return true, nil
}

func (u *favoriteRepo) GetListUserFavorites(ctx context.Context, req *models.UserPrimaryKey) (*models.ServiceGetListResponse, error) {
	var (
		resp  = &models.ServiceGetListResponse{}
		query string
		s     = NewServiceRepo(u.db, u.log)
	)

	query = `
		SELECT
			service_id
		FROM "user_favorites"
		WHERE user_id = $1
	`

	rows, err := u.db.Query(ctx, query, req.Id)
	if err != nil {
		u.log.Error("error is while getting banner list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			service_id sql.NullString
		)

		err = rows.Scan(
			&service_id,
		)
		if err != nil {
			u.log.Error("error is while getting favorite list (scanning data)", logger.Error(err))
			return nil, err
		}
		fmt.Println(service_id.String)
		service, err := s.GetByID(context.Background(), &models.ServicePrimaryKey{Id: service_id.String})
		if err != nil && err.Error()!= "no rows in result set" {
			u.log.Error("error is while getting service by id", logger.Error(err))
			return nil, err
		}

		resp.Services = append(resp.Services, service)
	}
	return resp, nil
}

func (u *favoriteRepo) Delete(ctx context.Context, req *models.DeleteFavorite) error {

	_, err := u.db.Exec(ctx, `DELETE FROM user_favorites  WHERE user_id =$1 AND service_id = $2`, req.UserId, req.ServiceId)
	if err != nil {
		u.log.Error("favorite.go 96 line : error is while deleting ", logger.Error(err))
		return err
	}

	return nil
}
