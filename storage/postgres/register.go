package postgres

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"ibron/models"
	"ibron/pkg/logger"
	"time"
)

type registerRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewRegisterRepo(db *pgxpool.Pool, log logger.LoggerI) *registerRepo {
	return &registerRepo{
		db:  db,
		log: log,
	}
}

func (u *registerRepo) Create(ctx context.Context, reg *models.Register) error {
	location, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		u.log.Error("error while loading location")
		return err
	}

	currentTime := time.Now().In(location)
	if _, err := u.db.Exec(ctx, `insert into register(phone_number, code, created_at) values($1, $2, $3)`,
		reg.PhoneNumber, reg.Code, currentTime); err != nil {
		u.log.Error("error is while insert err:" + err.Error())
		return err
	}

	return nil
}

func (u *registerRepo) VerifyCode(ctx context.Context, register *models.RegisterRequest) (string, error) {
	var code string
	query := `select code from register where phone_number = $1`
	if err := u.db.QueryRow(ctx, query, register.PhoneNumber).Scan(&code); err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error is select code by phone number" + err.Error())
		return "", err
	}

	return code, nil
}

func (u *registerRepo) DeleteVerifiedCode(ctx context.Context, reg *models.RegisterRequest) error {
	query := `delete from register where phone_number = $1`
	rowsAffected, err := u.db.Exec(ctx, query, reg.PhoneNumber)
	if err != nil {
		u.log.Error("error is while deleting verified code" + err.Error())
		return err
	}

	if n := rowsAffected.RowsAffected(); n == 0 {
		u.log.Error("error is rows affected")
		return err
	}

	return nil
}
