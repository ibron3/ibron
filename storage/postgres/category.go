package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"ibron/models"
	"ibron/pkg/helper"
	"ibron/pkg/logger"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type categoryRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewCategoryRepo(db *pgxpool.Pool, log logger.LoggerI) *categoryRepo {
	return &categoryRepo{
		db:  db,
		log: log,
	}
}

func (u *categoryRepo) Create(ctx context.Context, req *models.CategoryCreate) (*models.Category, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "categories"(id, name)
		VALUES ($1, $2) 
	`

	if _, err := u.db.Exec(ctx, query,
		id,
		req.Name,
	); err != nil {
		u.log.Error("insert error" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.CategoryPrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id category" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *categoryRepo) GetByID(ctx context.Context, req *models.CategoryPrimaryKey) (*models.Category, error) {
	var (
		query string

		id        sql.NullString
		name      sql.NullString
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	query = `
		SELECT 
			id,
			name, 
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "categories" 
		WHERE id =$1

	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		u.log.Error("error while scanning category data" + err.Error())
		return nil, err
	}

	return &models.Category{
		Id:        id.String,
		Name:      name.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}, nil
}

func (u *categoryRepo) GetList(ctx context.Context, req *models.CategoryGetListRequest) (*models.CategoryGetListResponse, error) {
	var (
		resp   = &models.CategoryGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "categories"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting category list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			name      sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&name,
			&createdAt,
			&updatedAt,
		)
		if err != nil {
			u.log.Error("error is while getting category list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Categories = append(resp.Categories, &models.Category{
			Id:        id.String,
			Name:      name.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}
	return resp, nil
}

func (u *categoryRepo) Update(ctx context.Context, req *models.CategoryUpdate) (int64, error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"categories"
		SET
			name = :name,
			updated_at = NOW()
		WHERE id = :id
	`
	params = map[string]interface{}{
		"id":   req.Id,
		"name": req.Name,
	}

	query, args := helper.ReplaceQueryParams(query, params)
	result, err := u.db.Exec(ctx, query, args...)
	if err != nil {
		u.log.Error("error is while updating category data", logger.Error(err))
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (u *categoryRepo) Delete(ctx context.Context, req *models.CategoryPrimaryKey) error {

	_, err := u.db.Exec(ctx, `DELETE FROM categories  WHERE id = $1`, req.Id)
	if err != nil {
		u.log.Error("error is while deleting category", logger.Error(err))
		return err
	}

	return nil
}
