package helper

import (
	"strconv"
	"strings"
)

func ReplaceQueryParams(namedQuery string, params map[string]interface{}) (string, []interface{}) {
	var (
		i    int = 1
		args []interface{}
	)

	for k, v := range params {
		if k != "" {
			namedQuery = strings.ReplaceAll(namedQuery, ":"+k, "$"+strconv.Itoa(i))

			args = append(args, v)
			i++
		}
	}

	return namedQuery, args
}

func EngToRuWeekday(day string)(string){
	switch day{
	case "Monday":
		return "Пн"
	case "Tuesday":
		return "Вт"
	case "Wednesday":
		return "Ср"
	case "Thursday":
		return "Чт"
	case "Friday":
		return "Пт"
	case "Saturday":
		return "Сб"
	case "Sunday":
		return "Вс"
	}
	return ""
}
