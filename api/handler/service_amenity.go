package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"ibron/models"
	"ibron/pkg/helper"
)

// Create ServiceAmenity godoc
// @ID create_ServiceAmenity
// @Router /ibron/api/v1/service-amenity [POST]
// @Summary Create ServiceAmenity
// @Description Create ServiceAmenity
// @Tags ServiceAmenity
// @Accept json
// @Procedure json
// @Param user body models.ServiceAmenityCreate true "CreateServiceAmenityRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateServiceAmenity(c *gin.Context) {
	var (
		createServiceAmenity models.ServiceAmenityCreate
	)

	err := c.ShouldBindJSON(&createServiceAmenity)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error ServiceAmenity Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	resp, err := h.storage.ServiceAmenity().Create(c.Request.Context(), &createServiceAmenity)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error ServiceAmenity.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create User Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID ServiceAmenity godoc
// @ID get_by_id_ServiceAmenity
// @Router /ibron/api/v1/service-amenity/{id} [GET]
// @Summary Get By ID ServiceAmenity
// @Description Get By ID ServiceAmenity
// @Tags ServiceAmenity
// @Accept json
// @Procedure json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdServiceAmenity(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.storage.ServiceAmenity().GetByID(c.Request.Context(), &models.ServiceAmenityPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.ServiceAmenity.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID ServiceAmenity Response!")
	c.JSON(http.StatusOK, resp)
}

// GetList ServiceAmenity godoc
// @ID get_list_ServiceAmenity
// @Router /ibron/api/v1/service-amenity [GET]
// @Summary Get List ServiceAmenity
// @Description Get List ServiceAmenity
// @Tags ServiceAmenity
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param filter query string false "filter"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListServiceAmenity(c *gin.Context) {
	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListServiceAmenity INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListServiceAmenity INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	filter := c.Query("filter")
	search := c.Query("search")

	resp, err := h.storage.ServiceAmenity().GetList(c.Request.Context(), &models.ServiceAmenityGetListRequest{
		Offset: offset,
		Limit:  limit,
		Filter: filter,
		Search: search,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.ServiceAmenity.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListServiceAmenity Response!")
	c.JSON(http.StatusOK, resp)
}

// Update ServiceAmenity godoc
// @ID update_ServiceAmenity
// @Router /ibron/api/v1/service-amenity/{id} [PUT]
// @Summary Update ServiceAmenity
// @Description Update ServiceAmenity
// @Tags ServiceAmenity
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Branch body models.ServiceAmenityUpdate true "UpdateServiceAmenityRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UpdateServiceAmenity(c *gin.Context) {
	var (
		id                   = c.Param("id")
		updateServiceAmenity models.ServiceAmenityUpdate
	)

	if !helper.IsValidUUID(id) {
		h.logger.Error("is invalid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	err := c.ShouldBindJSON(&updateServiceAmenity)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error ServiceAmenity Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	updateServiceAmenity.Id = id
	rowsAffected, err := h.storage.ServiceAmenity().Update(c.Request.Context(), &updateServiceAmenity)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.ServiceAmenity.Update!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if rowsAffected <= 0 {
		h.logger.Error("storage.ServiceAmenity.Update!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
		return
	}

	resp, err := h.storage.ServiceAmenity().GetByID(c.Request.Context(), &models.ServiceAmenityPrimaryKey{Id: updateServiceAmenity.Id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.ServiceAmenity.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Update ServiceAmenity Successfully!")
	c.JSON(http.StatusAccepted, resp)
}

// Delete ServiceAmenity godoc
// @ID delete_ServiceAmenity
// @Router /ibron/api/v1/service-amenity/{id} [DELETE]
// @Summary Delete ServiceAmenity
// @Description Delete ServiceAmenity
// @Tags ServiceAmenity
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteServiceAmenity(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.ServiceAmenity().Delete(c.Request.Context(), &models.ServiceAmenityPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.ServiceAmenity.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("ServiceAmenity Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}
