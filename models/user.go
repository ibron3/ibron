package models

type User struct {
	Id          string `json:"id"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	Birthday    string `json:"birthday"`
	Gender      string `json:"gender"`
	PhoneNumber string `json:"phone_number"`
	Photo       string `json:"photo"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
}

type UserCreate struct {
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	Birthday    string `json:"birthday"`
	Gender      string `json:"gender"`
	PhoneNumber string `json:"phone_number"`
	Photo       string `json:"photo"`
}

type UserUpdate struct {
	Id          string `json:"id"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	Birthday    string `json:"birthday"`
	Gender      string `json:"gender"`
	PhoneNumber string `json:"phone_number"`
	Photo       string `json:"photo"`
}

type UserPrimaryKey struct {
	Id          string `json:"id"`
	PhoneNumber string `json:"phone_number"`
}

type UserGetListRequest struct {
	Offset int    `json:"offset"`
	Limit  int    `json:"limit"`
	Filter string `json:"filter"`
	Search string `json:"search"`
}

type UserGetListResponse struct {
	Count int     `json:"count"`
	Users []*User `json:"users"`
}

type UserPhoneNumber struct {
	PhoneNumber string `json:"phone_number"`
}
