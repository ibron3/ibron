CREATE TABLE IF NOT EXISTS "users"(
    "id" UUID PRIMARY KEY,
    "first_name" VARCHAR(25) NOT NULL,
    "last_name" VARCHAR(25) NOT NULL,
    "birthday" DATE NOT NULL,
    "gender" VARCHAR(7) CHECK (gender IN ('Мужчина', 'Женщина')),
    "phone_number" VARCHAR(13) UNIQUE NOT NULL,
    "photo" VARCHAR,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP,
    "deleted_at" TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "user_favorites"(
    "user_id" UUID REFERENCES "users"("id"),
    "service_id" UUID REFERENCES "services"("id"),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "merchants"(
    "id" UUID PRIMARY KEY,
    "name" VARCHAR(25) NOT NULL,    
    "login" VARCHAR(20) NOT NULL,
    "password" VARCHAR NOT NULL,
    "phone_number" VARCHAR(13) NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP,
    "deleted_at" TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "business_merchants"(
        "id" UUID PRIMARY KEY,
        "merchant_id" UUID REFERENCES "merchants"("id"),
        "business_name" VARCHAR(30) NOT NULL,
        "description" TEXT NOT NULL,
        "address" VARCHAR(100), 
        "latitude" NUMERIC NOT NULL,
        "longitude" NUMERIC NOT NULL,
        "phone1" VARCHAR(13) NOT NULL,
        "phone2" VARCHAR(13),
        "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        "updated_at" TIMESTAMP,
        "deleted_at" TIMESTAMP
    );

CREATE TABLE IF NOT EXISTS "services"(
        "id" UUID PRIMARY KEY,
        "business_merchant_id" UUID REFERENCES "business_merchants"("id"),
        "category_id"  UUID REFERENCES "categories"(id),
        "merchant_id" UUID REFERENCES "merchants"("id"),
        "name" VARCHAR NOT NULL,
        "description" TEXT,
        "duration" NUMERIC,
        "price" NUMERIC,
        "address" VARCHAR,
        "latitude" NUMERIC,
        "longitude"  NUMERIC,
        "view" int ,
        "book" BOOLEAN,
        "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        "updated_at" TIMESTAMP,
        "deleted_at" TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "service_photos"(
    "id" UUID PRIMARY KEY,
    "business_id" UUID REFERENCES "business_merchants"("id"),
    "service_id" UUID REFERENCES "services"("id"),
    "url" TEXT NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "categories"(
    "id" UUID PRIMARY KEY,
    "name" VARCHAR NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "schedule"(
    "id" UUID PRIMARY KEY,
    "service_id" UUID REFERENCES "services"("id"),
    "day" VARCHAR NOT NULL,
    "start_time" TIME,
    "end_time" TIME,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP,
    "deleted_at" TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "requests"(
    "id" UUID PRIMARY KEY,
    "service_id" UUID REFERENCES "services"("id"),
    "user_id" UUID REFERENCES "users"("id"),
    "client_id" UUID REFERENCES "clients"("id"),
    "merchant_id" UUID REFERENCES "merchants"("id"),
    "start_time" TIME,
    "end_time" TIME,
    "price" NUMERIC NOT NULL,
    "date" DATE,
    "status" VARCHAR DEFAULT 'new' CHECK (status IN ('new','approved', 'canceled','finished')),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP,
    "deleted_at" TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "clients"(
    "id" UUID PRIMARY KEY,
    "merchant_id" UUID REFERENCES "merchants"("id"),
    "first_name" VARCHAR(25) NOT NULL,
    "last_name" VARCHAR(25) NOT NULL,
    "phone_number" VARCHAR(13),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP,
    "deleted_at" TIMESTAMP
);



CREATE TABLE IF NOT EXISTS "banner" (
    "id" UUID PRIMARY KEY,
    "service_id" UUID REFERENCES "services"("id"),
    "url" VARCHAR,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
)

CREATE TABLE IF NOT EXISTS "amenities" (
    "id" UUID PRIMARY KEY,
    "name" VARCHAR NOT NULL,
    "url" VARCHAR NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "service_amenities" (
     "id" UUID PRIMARY KEY,
     "service_id" UUID REFERENCES "services"("id"),
     "amenity_id" UUID REFERENCES "amenities"("id"),
     "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
     "updated_at" TIMESTAMP
);



--CRUD qlish kk
CREATE TABLE IF NOT EXISTS "working_hours" (
    "id" UUID PRIMARY KEY,
    "business_merchant_id" UUID REFERENCES "business_merchants"("id"),
    "service_id" UUID REFERENCES "services"("id"),
    "day" VARCHAR,
    "start_time" TIME,
    "end_time" TIME,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "confirmed_requests" (
    "request_id" UUID ,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "register"(
    "phone_number" VARCHAR(13) NOT NULL,
    "code" VARCHAR(4) NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "thumbnails"(
    "id" UUID PRIMARY KEY,
    "service_id" UUID REFERENCES "services"("id"),
    "url" VARCHAR,
);


CREATE TABLE IF NOT EXISTS "thumnails"(
     "id" UUID PRIMARY KEY,
     "service_id" UUID REFERENCES "services"("id"),
     "url" VARCHAR
);
