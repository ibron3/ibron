package handler

import (
	"ibron/models"
	"ibron/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
)

// Create Services godoc
// @ID create_service
// @Router /ibron/api/v1/service [POST]
// @Summary Create Services
// @Description Create Services
// @Tags Service
// @Accept json
// @Procedure json
// @Param service body models.ServiceCreate true "CreateServiceRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateServices(c *gin.Context) {
	var (
		serviceCreate models.ServiceCreate
	)

	err := c.ShouldBindJSON(&serviceCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Service Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	if serviceCreate.Latitude != 0 && serviceCreate.Longitude != 0 && serviceCreate.BusinessMerchantID == "6a58e9cd-6bbf-4157-bac4-af4709150426" {
		serviceCreate.Book = false
	} else {
		businessMerchantData, err := h.storage.BusinessMerchant().GetByID(c.Request.Context(), &models.BusinessMerchantPrimaryKey{Id: serviceCreate.BusinessMerchantID})
		if err != nil {
			h.logger.Error(err.Error() + "  :  " + "error BusinessMerchant.GetByID")
			c.JSON(http.StatusInternalServerError, "Server Error!")
			return
		}

		serviceCreate.Address = businessMerchantData.Address
		serviceCreate.Longitude = businessMerchantData.Longitude
		serviceCreate.Latitude = businessMerchantData.Latitude
		serviceCreate.Book = true

	}

	service, err := h.storage.Service().Create(c.Request.Context(), &serviceCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Service.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	for _, url := range serviceCreate.Url {
		_, err = h.storage.ServicePhotos().Create(c.Request.Context(), &models.ServicePhotoCreate{
			BusinessID: service.BusinessMerchantID,
			ServiceID:  service.Id,
			Url:        url.Url,
		})
		if err != nil {
			h.logger.Error(err.Error() + "  :  " + "error ServicePhotos.Create")
			c.JSON(http.StatusInternalServerError, "Server Error!")
			return
		}
	}

	for _, amenity := range serviceCreate.Amenity {
		_, err := h.storage.ServiceAmenity().Create(c.Request.Context(), &models.ServiceAmenityCreate{
			ServiceId: service.Id,
			AmenityId: amenity.AmenityId,
		})

		if err != nil {
			h.logger.Error(err.Error() + "  :  " + "error ServiceAmenity.Create")
			c.JSON(http.StatusInternalServerError, "Server Error!")
			return
		}

	}

	for _, workingHours := range serviceCreate.Days {
		h.storage.WorkingHours().Create(c.Request.Context(), &models.WorkingHoursCreate{
			BusinessMerchantId: service.BusinessMerchantID,
			ServiceId:          service.Id,
			Day:                workingHours.Day,
			StartTime:          workingHours.StartTime,
			EndTime:            workingHours.EndTime,
		})
	}

	h.logger.Info("Create Service Successfully!!")
	c.JSON(http.StatusCreated, service)
}

// GetByID Services godoc
// @ID get_by_id_service
// @Router /ibron/api/v1/service/{id} [GET]
// @Summary Get By ID Services
// @Description Get By ID Services
// @Tags Service
// @Accept json
// @Procedure json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdServices(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	service, err := h.storage.Service().GetByID(c.Request.Context(), &models.ServicePrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Service.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	_, err = h.storage.Service().Update(c.Request.Context(), &models.ServiceUpdate{
		Id:          service.Id,
		Name:        service.Name,
		Description: service.Description,
		Duration:    service.Duration,
		Price:       service.Price,
		Address:     service.Address,
		Latitude:    service.Latitude,
		Longitude:   service.Longitude,
		View:        service.View + 1,
	})
	if err != nil {
		h.logger.Error("error is while updating view" + err.Error())
		c.JSON(500, "server error")
		return
	}

	service.View += 1

	h.logger.Info("GetByID Service Response!")
	c.JSON(http.StatusOK, service)
}

// GetList Services godoc
// @ID get_list_service
// @Router /ibron/api/v1/services [GET]
// @Summary Get List Services
// @Description Get List Services
// @Tags Service
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param filter query string false "filter"
// @Param search query string false "search"
// @Param merchant_id query string false "merchant_id"
// @Param longitude query string false "longitude"
// @Param latitude query string false "latitude"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListServices(c *gin.Context) {
	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListService INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListService INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	filter := c.Query("filter")
	search := c.Query("search")
	merchant_id := c.Query("merchant_id")
	longitude := cast.ToFloat64(c.Query("longitude"))
	latitude := cast.ToFloat64(c.Query("latitude"))

	resp, err := h.storage.Service().GetList(c.Request.Context(), &models.ServiceGetListRequest{
		Offset:     offset,
		Limit:      limit,
		Filter:     filter,
		Search:     search,
		MerchantId: merchant_id,
		Longitude:  longitude,
		Latitude:   latitude,
	})

	for k, v := range resp.Services {
		working_hours, err := h.storage.WorkingHours().GetListByServiceId(c.Request.Context(), &models.WorkingHoursGetListRequest{ServiceId: v.Id})
		if err != nil {
			h.logger.Error(err.Error() + "  :  " + "storage.WorkingHours.GetList!")
			c.JSON(http.StatusInternalServerError, "Server Error!")
			return
		}
		resp.Services[k].WorkingHours = working_hours.WorkingHours
	}

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Service.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	target := models.Location{
		Latitude:  latitude,
		Longitude: longitude,
	}

	services := helper.Distance(target, resp.Services)

	h.logger.Info("GetListService Response!")
	c.JSON(http.StatusOK, services)
}

// GetList False Services godoc
// @ID get_list_false_service
// @Router /ibron/api/v1/false-services [GET]
// @Summary Get List False Services
// @Description Get List False Services
// @Tags Service
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param longitude query string false "longitude"
// @Param latitude query string false "latitude"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListFalseServices(c *gin.Context) {
	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListService INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListService INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	longitude := cast.ToFloat64(c.Query("longitude"))
	latitude := cast.ToFloat64(c.Query("latitude"))

	resp, err := h.storage.Service().GetListFalseServices(c.Request.Context(), &models.ServiceGetListRequest{
		Offset: offset,
		Limit:  limit,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Service.GetListFalseServices!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	target := models.Location{
		Latitude:  latitude,
		Longitude: longitude,
	}

	services := helper.Distance(target, resp.Services)

	h.logger.Info("GetListService Response!")
	c.JSON(http.StatusOK, services)
}

// GetList False Services godoc
// @ID get_list_true_service
// @Router /ibron/api/v1/true-services [GET]
// @Summary Get List True Services
// @Description Get List True Services
// @Tags Service
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param longitude query string false "longitude"
// @Param latitude query string false "latitude"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListTrueServices(c *gin.Context) {
	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListService INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListService INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	longitude := cast.ToFloat64(c.Query("longitude"))
	latitude := cast.ToFloat64(c.Query("latitude"))

	resp, err := h.storage.Service().GetListTrueServices(c.Request.Context(), &models.ServiceGetListRequest{
		Offset: offset,
		Limit:  limit,
	})

	for k, v := range resp.Services {
		working_hours, err := h.storage.WorkingHours().GetListByServiceId(c.Request.Context(), &models.WorkingHoursGetListRequest{ServiceId: v.Id})
		if err != nil {
			h.logger.Error(err.Error() + "  :  " + "storage.WorkingHours.GetList!")
			c.JSON(http.StatusInternalServerError, "Server Error!")
			return
		}
		resp.Services[k].WorkingHours = working_hours.WorkingHours
	}

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Service.GetListFalseServices!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	target := models.Location{
		Latitude:  latitude,
		Longitude: longitude,
	}

	services := helper.Distance(target, resp.Services)

	h.logger.Info("GetListService Response!")
	c.JSON(http.StatusOK, services)
}

// Recommendation Services godoc
// @ID recommendation_service
// @Router /ibron/api/v1/recommendation-services [GET]
// @Summary Recommendation Services
// @Description Recommendation Services
// @Tags Service
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param latitude query string false "latitude"
// @Param longitude query string false "longitude"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) RecommendationServices(c *gin.Context) {
	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListService INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListService INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	latitude := c.Query("latitude")
	longitude := c.Query("longitude")

	var target = models.Location{
		Latitude:  cast.ToFloat64(latitude),
		Longitude: cast.ToFloat64(longitude),
	}

	resp, err := h.storage.Service().Recommendation(c.Request.Context(), &models.ServiceGetListRequest{
		Offset: offset,
		Limit:  limit,
	})
	var response = &models.ServiceGetListResponse{}
	response = helper.Distance(target, resp.Services)

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Service.Recommendation!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	for _, service := range response.Services {
		amenities, err := h.storage.Amenity().GetListServiceAmenity(c.Request.Context(), &models.ServicePrimaryKey{service.Id})
		if err != nil {
			h.logger.Error(err.Error() + "  :  " + "storage.Service.Recommendation.Amenity.GetListServiceAmenity!")
			c.JSON(http.StatusInternalServerError, "Server Error!")
			return
		}
		service.Amenity = append(service.Amenity, amenities.Amenity...)

		photos, err := h.storage.ServicePhotos().GetServicePhotos(c.Request.Context(), &models.ServicePrimaryKey{Id: service.Id})
		if err != nil {
			h.logger.Error(err.Error() + "  :  " + "storage.Service.Recommendation.ServicePhotos.GetServicePhotos!")
			c.JSON(http.StatusInternalServerError, "Server Error!")
			return
		}
		service.Url = append(service.Url, photos.Url...)
	}

	h.logger.Info("Recommendation service Response!")
	c.JSON(http.StatusOK, response)
}

// GetList Closest Service godoc
// @ID get_list_closest_services
// @Router /ibron/api/v1/closest-service [POST]
// @Summary Get List Closest Service
// @Description Get List Closest Service
// @Tags Service
// @Accept json
// @Procedure json
// @Param location body models.Location true "Location request"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListClosestService(c *gin.Context) {

	var location models.Location
	err := c.ShouldBindJSON(&location)
	if err != nil {
		c.JSON(400, "Bad Request")
		return
	}
	service, err := h.storage.Service().GetList(c.Request.Context(), &models.ServiceGetListRequest{
		Offset: 0,
		Limit:  10000,
	})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Service.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	resp := helper.ClosestLocation(location, service.Services)

	c.JSON(200, resp.Services)

}

// Update Services godoc
// @ID update_service
// @Router /ibron/api/v1/service/{id} [PUT]
// @Summary Update Services
// @Description Update Services
// @Tags Service
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param service body models.ServiceUpdate true "UpdateServiceRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UpdateServices(c *gin.Context) {
	var (
		id            = c.Param("id")
		serviceUpdate models.ServiceUpdate
	)

	if !helper.IsValidUUID(id) {
		h.logger.Error("is invalid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	err := c.ShouldBindJSON(&serviceUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Service Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	service, err := h.storage.Service().GetByID(c.Request.Context(), &models.ServicePrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Service.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	err = h.storage.WorkingHours().Delete(c.Request.Context(), &models.WorkingHoursPrimaryKey{
		ServiceId: service.Id,
	})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.WorkingHours.Delete!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	business, err := h.storage.BusinessMerchant().GetByID(c.Request.Context(), &models.BusinessMerchantPrimaryKey{
		Id: service.BusinessMerchantID,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.BusinessMerchant.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	// if serviceUpdate.Thumbnail != "" {
	// // thumbnail delete
	// parsedURL, err := url.Parse(service.Thumbnail)
	// if err != nil {
	// 	fmt.Println("Error parsing URL:", err)
	// 	return
	// }
	// thumbnailID := path.Base(parsedURL.Path)
	// fmt.Println("ID:", thumbnailID)

	// err = helper.DeleteFile(thumbnailID)
	// if err != nil {
	// 	h.logger.Error("delete thumbnail file from firebase!")
	// 	c.JSON(http.StatusInternalServerError, "Server error!")
	// 	return
	// }
	// firebase delete
	// servicePhotos, err := h.storage.ServicePhotos().GetServicePhotos(c.Request.Context(), &models.ServicePrimaryKey{Id: service.Id})
	// if err != nil {
	// 	h.logger.Error(err.Error() + "  :  " + "storage.ServicePhotos.GetServicePhotos!")
	// 	c.JSON(http.StatusInternalServerError, "Server Error!")
	// 	return
	// }

	// for _, url := range servicePhotos.Url {
	// 	var fileID string
	// 	re := regexp.MustCompile(`/o/([a-z0-9\-]+)\?`)

	// 	match := re.FindStringSubmatch(url.Url)
	// 	if len(match) > 1 {
	// 		fileID = match[1]
	// 	} else {
	// 		fmt.Println("ID not found")
	// 	}

	// 	err = helper.DeleteFile(fileID)
	// 	if err != nil {
	// 		h.logger.Error(err.Error() + "  :  " + " delete file !")
	// 		c.JSON(http.StatusInternalServerError, "delete file!")
	// 		return
	// 	}

	// }

	// service photo by service id delete
	// err = h.storage.ServicePhotos().Delete(c.Request.Context(), &models.ServicePhotoPrimaryKey{Id: service.Id})
	// if err != nil {
	// 	h.logger.Error(err.Error() + "  :  " + "storage.ServicePhotos.Delete!")
	// 	c.JSON(http.StatusInternalServerError, "Server Error!")
	// 	return
	// }

	// url service id create
	// for _, url := range serviceUpdate.Urls {
	// 	_, err = h.storage.ServicePhotos().Create(c.Request.Context(), &models.ServicePhotoCreate{
	// 		BusinessID: service.BusinessMerchantID,
	// 		ServiceID:  service.Id,
	// 		Url:        url.Url,
	// 	})
	// 	if err != nil {
	// 		h.logger.Error("storage.ServicePhoto.Create!")
	// 		c.JSON(http.StatusInternalServerError, "Server error!")
	// 		return
	// 	}
	// }

	serviceUpdate.View = service.View + 1
	serviceUpdate.Id = id
	serviceUpdate.Address = business.Address

	rowsAffected, err := h.storage.Service().Update(c.Request.Context(), &serviceUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Service.Update!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if rowsAffected <= 0 {
		h.logger.Error("storage.Service.Update!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
		return
	}

	// creating working hours
	for _, workingHour := range serviceUpdate.Days {
		_, err := h.storage.WorkingHours().Create(c.Request.Context(), &models.WorkingHoursCreate{
			ServiceId: service.Id,
			Day:       workingHour.Day,
			StartTime: workingHour.StartTime,
			EndTime:   workingHour.EndTime,
		})
		if err != nil {
			h.logger.Error(err.Error() + "  :  " + "storage.WorkingHours.Create!")
			c.JSON(http.StatusInternalServerError, "Server Error!")
			return
		}
	}

	resp, err := h.storage.Service().GetByID(c.Request.Context(), &models.ServicePrimaryKey{Id: serviceUpdate.Id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Service.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Update Service Successfully!")
	c.JSON(http.StatusAccepted, resp)
}

// Delete Services godoc
// @ID delete_service
// @Router /ibron/api/v1/service/{id} [DELETE]
// @Summary Delete Services
// @Description Delete Services
// @Tags Service
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteServices(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.WorkingHours().Delete(c.Request.Context(), &models.WorkingHoursPrimaryKey{ServiceId: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.WorkingHours.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	err = h.storage.Service().Delete(c.Request.Context(), &models.ServicePrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Service.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Service Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}

// upload service photos godoc
// @ID upload_service_photo
// @Router /ibron/api/v1/upload-service-photos [POST]
// @Summary Upload Service Photos
// @Description Upload Service Photos
// @Tags Service
// @Accept multipart/form-data
// @Procedure json
// @Param file formData []file true "Photos to upload"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UploadServicePhotos(c *gin.Context) {

	// form, err := c.MultipartForm()
	// if err != nil {
	// h.logger.Error(err.Error() + "  :  " + "File error")
	// return
	// }

	// resp, _ := helper.UploadFile(form)

	// c.JSON(200, resp)

}
