package models

type Amenity struct {
	Id        string   `json:"id"`
	Name      string   `json:"name"`
	Url       string   `json:"url"`
	CreatedAt string   `json:"created_at,omitempty"`
	UpdatedAt string   `json:"updated_at,omitempty"`
}

type AmenityCreate struct {
	Name      string   `json:"name"`
	Url       string   `json:"url"`
}

type AmenityUpdate struct {
	Id        string `json:"id"`
	Name      string   `json:"name"`
	Url       string   `json:"url"`
}

type AmenityPrimaryKey struct {
	Id string `json:"id"`
}

type AmenityGetListRequest struct {
	Offset int    `json:"offset"`
	Limit  int    `json:"limit"`
	Filter string `json:"filter"`
	Search string `json:"search"`
}

type AmenityGetListResponse struct {
	Count  int       `json:"count"`
	Amenity []*Amenity `json:"amenities"`
}
