package handler

import (
	"ibron/models"
	"ibron/pkg/helper"
	"math/rand"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// Send OTP godoc
// @ID send_otp
// @Router /ibron/api/v1/send-otp [POST]
// @Summary Send OTP
// @Description Send OTP
// @Tags Register
// @Accept json
// @Procedure json
// @Param register body models.RegisterRequest true "RegisterRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) SendOTP(c *gin.Context) {
	var reg models.RegisterRequest
	if err := c.ShouldBindJSON(&reg); err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Request Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	if !helper.IsValidPhone(reg.PhoneNumber) {
		h.logger.Error("invalid phone number!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Phone!")
		return
	}

	// check for existence before send otp to this number
	code, err := h.storage.Register().VerifyCode(c.Request.Context(), &models.RegisterRequest{
		PhoneNumber: reg.PhoneNumber,
	})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Register.VerifyCode")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if code != "" {
		err = h.storage.Register().DeleteVerifiedCode(c.Request.Context(), &models.RegisterRequest{PhoneNumber: reg.PhoneNumber})
		if err != nil {
			h.logger.Error(err.Error() + "  :  " + "error Register.DeleteVerifiedCode")
			c.JSON(http.StatusInternalServerError, "Server Error!")
			return
		}
	}

	// send otp
	randomCode := rand.Intn(9000) + 1000
	message := "iBron ilovasi ro‘yxatdan o‘tish uchun tasdiqlash kodi: " + strconv.Itoa(randomCode)
	err = helper.SendSms(reg.PhoneNumber[1:], message)
	if err != nil {
		h.logger.Error("error is while sending message " + err.Error())
		c.JSON(http.StatusInternalServerError, "Server error")
		return
	}

	// count sms
	err = h.storage.SmsUser().Create(c.Request.Context(), &models.SmsCreate{
		SenderID: "0deeec11-7433-41dc-a114-4c7ffa77c737",
		Name:     "ibron",
		Count:    1,
	})
	if err != nil {
		h.logger.Error(err.Error() + " error SmsUser.Create")
		c.JSON(http.StatusInternalServerError, "Server error")
		return
	}

	err = h.storage.Register().Create(c.Request.Context(), &models.Register{PhoneNumber: reg.PhoneNumber, Code: strconv.Itoa(randomCode)})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Register.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	c.JSON(http.StatusOK, randomCode)
}

// Verify Code godoc
// @ID verify_code
// @Router /ibron/api/v1/verify-code [POST]
// @Summary Verify Code
// @Description Verify Code
// @Tags Register
// @Accept json
// @Procedure json
// @Param register body models.Register true "Register"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) VerifyCode(c *gin.Context) {
	var reg models.Register
	if err := c.ShouldBindJSON(&reg); err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Request Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	if !helper.IsValidPhone(reg.PhoneNumber) {
		h.logger.Error("invalid phone number!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Phone!")
		return
	}

	code, err := h.storage.Register().VerifyCode(c.Request.Context(), &models.RegisterRequest{
		PhoneNumber: reg.PhoneNumber,
	})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Register.VerifyCode")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if code != reg.Code {
		h.logger.Error("error Request Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Code!")
		return
	}

	err = h.storage.Register().DeleteVerifiedCode(c.Request.Context(), &models.RegisterRequest{PhoneNumber: reg.PhoneNumber})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Register.DeleteVerifiedCode")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}
	reg.Code = ""

	user, err := h.storage.User().GetByPhoneNumber(c.Request.Context(), &models.UserPhoneNumber{PhoneNumber: reg.PhoneNumber})
	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.User.GetByPhoneNumber!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.User.GetByPhoneNumber!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	reg.Exist = false
	if user != nil {
		reg.Exist = true
	}

	c.JSON(http.StatusOK, reg)
}
