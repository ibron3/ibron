package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"ibron/models"
	"ibron/pkg/helper"
)

// Create Banners godoc
// @ID create_Banners
// @Router /ibron/api/v1/banner [POST]
// @Summary Create Banners
// @Description Create Banners
// @Tags Banner
// @Accept json
// @Procedure json
// @Param user body models.BannerCreate true "CreateBannerRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateBanner(c *gin.Context) {
	var (
		createBanner models.BannerCreate
	)

	err := c.ShouldBindJSON(&createBanner)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Banner Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	resp, err := h.storage.Banner().Create(c.Request.Context(), &createBanner)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Banner.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create User Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID Banners godoc
// @ID get_by_id_Banners
// @Router /ibron/api/v1/banner/{id} [GET]
// @Summary Get By ID Banners
// @Description Get By ID Banners
// @Tags Banner
// @Accept json
// @Procedure json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdBanner(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.storage.Banner().GetByID(c.Request.Context(), &models.BannerPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Banner.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}
	resp.Service, err = h.storage.Service().GetByID(c.Request.Context(), &models.ServicePrimaryKey{Id: resp.ServiceId})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Service.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID Banner Response!")
	c.JSON(http.StatusOK, resp)
}

// GetList Banners godoc
// @ID get_list_Banners
// @Router /ibron/api/v1/banner [GET]
// @Summary Get List Banners
// @Description Get List Banners
// @Tags Banner
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param filter query string false "filter"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListBanner(c *gin.Context) {
	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListBanner INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListBanner INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	filter := c.Query("filter")
	search := c.Query("search")

	resp, err := h.storage.Banner().GetList(c.Request.Context(), &models.BannerGetListRequest{
		Offset: offset,
		Limit:  limit,
		Filter: filter,
		Search: search,
	})

	for _, val := range resp.Banner {
		if val != nil {
			val.Service, err = h.storage.Service().GetByID(c.Request.Context(), &models.ServicePrimaryKey{Id: val.ServiceId})
			if err != nil && err.Error() != "no rows in result set" {
				h.logger.Error(err.Error() + "  :  " + "storage.Service.GetByID!")
				c.JSON(http.StatusInternalServerError, "Server Error!")
				return
			}
		}
	}

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Banner.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListBanner Response!")
	c.JSON(http.StatusOK, resp)
}

// Update Banners godoc
// @ID update_Banners
// @Router /ibron/api/v1/banner/{id} [PUT]
// @Summary Update Banners
// @Description Update Banners
// @Tags Banner
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Branch body models.BannerUpdate true "UpdateBannerRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UpdateBanner(c *gin.Context) {
	var (
		id           = c.Param("id")
		updateBanner models.BannerUpdate
	)

	if !helper.IsValidUUID(id) {
		h.logger.Error("is invalid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	err := c.ShouldBindJSON(&updateBanner)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Banner Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	updateBanner.Id = id
	rowsAffected, err := h.storage.Banner().Update(c.Request.Context(), &updateBanner)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Banner.Update!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if rowsAffected <= 0 {
		h.logger.Error("storage.Banner.Update!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
		return
	}

	resp, err := h.storage.Banner().GetByID(c.Request.Context(), &models.BannerPrimaryKey{Id: updateBanner.Id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Banner.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Update Banner Successfully!")
	c.JSON(http.StatusAccepted, resp)
}

// Delete Banners godoc
// @ID delete_Banners
// @Router /ibron/api/v1/banner/{id} [DELETE]
// @Summary Delete Banners
// @Description Delete Banners
// @Tags Banner
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteBanner(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.Banner().Delete(c.Request.Context(), &models.BannerPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Banner.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Banner Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}
