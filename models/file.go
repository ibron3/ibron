package models

type MultipleFileUploadResponse struct {
	Url       []*Url `json:"url"`
	Thumbnail string `json:"thumbnail"`
}
