package models

type ServicePhoto struct {
	Id         string `json:"id"`
	BusinessID string `json:"business_id"`
	ServiceID  string `json:"service_id"`
	Url        string `json:"url"`
	CreatedAt  string `json:"created_at"`
	UpdatedAt  string `json:"updated_at"`
}

type ServicePhotoCreate struct {
	BusinessID string `json:"business_id"`
	ServiceID  string `json:"service_id"`
	Url        string `json:"url"`
}

type ServicePhotoUpdate struct {
	Id         string `json:"id"`
	BusinessID string `json:"business_id"`
	ServiceID  string `json:"service_id"`
	Url        string `json:"url"`
}

type ServicePhotoPrimaryKey struct {
	Id string `json:"id"`
}

type ServicePhotoGetListRequest struct {
	Offset int    `json:"offset"`
	Limit  int    `json:"limit"`
	Search string `json:"search"`
}

type ServicePhotoGetListResponse struct {
	Count        int             `json:"count"`
	ServicePhoto []*ServicePhoto `json:"service_photo"`
}

type ServicePhotoResponse struct{
	Url []*Url `json:"url"`
}
