package handler

import (
	"ibron/models"
	"ibron/pkg/helper"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

// Create WorkingHours godoc
// @ID create_working_hours
// @Router /ibron/api/v1/working-hours [POST]
// @Summary Create WorkingHours
// @Description Create WorkingHours
// @Tags Working Hours
// @Accept json
// @Procedure json
// @Param service body models.WorkingHoursCreate true "CreateWorkingHoursRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateWorkingHours(c *gin.Context) {
	var (
		workingHours models.WorkingHoursCreate
	)

	err := c.ShouldBindJSON(&workingHours)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error wokring hours Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	resp, err := h.storage.WorkingHours().Create(c.Request.Context(), &workingHours)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error WorkingHours.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create Working Hours Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID Working Hours godoc
// @ID get_by_id_working_hours
// @Router /ibron/api/v1/working-hours/{id} [GET]
// @Summary Get By ID Working Hours
// @Description Get By ID Working Hours
// @Tags Working Hours
// @Accept json
// @Procedure json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdWorkingHours(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	user, err := h.storage.WorkingHours().GetByID(c.Request.Context(), &models.WorkingHoursPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.WorkingHours.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID Service Response!")
	c.JSON(http.StatusOK, user)
}

// GetBy ServiceID Working Hours godoc
// @ID get_by_service_id_working_hours
// @Router /ibron/api/v1/working-hours-service-id [GET]
// @Summary Get By Service ID Working Hours
// @Description Get By Service ID Working Hours
// @Tags Working Hours
// @Accept json
// @Procedure json
// @Param service_id query string false "service_id"
// @Param date query string false "date"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByServiceIdWorkingHours(c *gin.Context) {
	serviceId := c.Query("service_id")
	day, err := time.Parse("2006-01-02", c.Query("date"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "time parsing!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if !helper.IsValidUUID(serviceId) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid service id")
		return
	}

	getByDate, err := h.storage.WorkingHours().GetByDate(c.Request.Context(), &models.WorkingHoursPrimaryKey{
		ServiceId: serviceId,
		Day:       helper.EngToRuWeekday(day.Weekday().String()),
	})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.WorkingHours.GetByDate!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByDate Service Response!")
	c.JSON(http.StatusOK, getByDate)
}

// GetList WorkingHours godoc
// @ID get_list_working_hours
// @Router /ibron/api/v1/working-hours [GET]
// @Summary Get List WorkingHours
// @Description Get List WorkingHours
// @Tags Working Hours
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param filter query string false "filter"
// @Param service_id query string false "service_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListWorkingHours(c *gin.Context) {
	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	filter := c.Query("filter")
	search := c.Query("search")
	serviceID := c.Query("service_id")

	resp, err := h.storage.WorkingHours().GetList(c.Request.Context(), &models.WorkingHoursGetListRequest{
		Offset:    offset,
		Limit:     limit,
		Filter:    filter,
		Search:    search,
		ServiceId: serviceID,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.WorkingHours.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	c.JSON(http.StatusOK, resp)
}

// Update WorkingHours godoc
// @ID update_working_hours
// @Router /ibron/api/v1/working-hours/{id} [PUT]
// @Summary Update WorkingHours
// @Description Update WorkingHours
// @Tags Working Hours
// @Accept json
// @Procedure json
// @Param id query string false "id"
// @Param service body models.WorkingHoursUpdate true "UpdateServiceRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UpdateWorkingHours(c *gin.Context) {
	var (
		id                 = c.Query("id")
		workingHoursUpdate models.WorkingHoursUpdate
	)

	if !helper.IsValidUUID(id) {
		h.logger.Error("is invalid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	err := c.ShouldBindJSON(&workingHoursUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Working Hours Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	workingHoursUpdate.Id = id
	rowsAffected, err := h.storage.WorkingHours().UpdateServiceTime(c.Request.Context(), &workingHoursUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.WorkingHours.Update!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if rowsAffected <= 0 {
		h.logger.Error("storage.WorkingHours.Update!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
		return
	}

	resp, err := h.storage.WorkingHours().GetByID(c.Request.Context(), &models.WorkingHoursPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.WorkingHours.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Update Service Successfully!")
	c.JSON(http.StatusAccepted, resp)
}

// Delete WorkingHours godoc
// @ID delete_working_hours
// @Router /ibron/api/v1/working-hours/{id} [DELETE]
// @Summary Delete WorkingHours
// @Description Delete WorkingHours
// @Tags Working Hours
// @Accept json
// @Procedure json
// @Param service_id path string true "service_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteWorkingHours(c *gin.Context) {
	var service_id = c.Param("service_id")

	if !helper.IsValidUUID(service_id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.WorkingHours().Delete(c.Request.Context(), &models.WorkingHoursPrimaryKey{ServiceId: service_id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.WorkingHours.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Service Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}
