package helper

import (
	"ibron/models"
	"math"
)

func degreesToRadians(degrees float64) float64 {
	return degrees * math.Pi / 180
}

func round(num float64) float64 {
	return (float64(int((num+0.005)*100)) / 100)
}

func Haversine(lat1, lon1, lat2, lon2 float64) float64 {
	const earthRadius = 6371 // Earth radius in kilometers

	dLat := degreesToRadians(lat2 - lat1)
	dLon := degreesToRadians(lon2 - lon1)

	lat1 = degreesToRadians(lat1)
	lat2 = degreesToRadians(lat2)

	a := math.Pow(math.Sin(dLat/2), 2) + math.Pow(math.Sin(dLon/2), 2)*math.Cos(lat1)*math.Cos(lat2)
	c := 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))
	distance := earthRadius * c

	return distance
}

func ClosestLocation(target models.Location, locations []*models.Service) *models.ServiceGetListResponse {
	var resp = models.ServiceGetListResponse{}

	for _, loc := range locations {
		distance := Haversine(target.Latitude, target.Longitude, loc.Latitude, loc.Longitude)
		if distance <= 3 {
			loc.Distance = math.Round(distance*10) / 10
			resp.Services = append(resp.Services, loc)
		}

	}

	return &resp
}

func Distance(target models.Location, locations []*models.Service) *models.ServiceGetListResponse {
	var resp = models.ServiceGetListResponse{}
	for _, loc := range locations {
		distance := Haversine(target.Latitude, target.Longitude, loc.Latitude, loc.Longitude)
		loc.Distance = math.Round(distance*10) / 10
		resp.Services = append(resp.Services, loc)
	}

	return &resp
}

