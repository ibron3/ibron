package handler

import (
	"ibron/models"
	"ibron/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Create Schedule godoc
// @ID create_schedule
// @Router /ibron/api/v1/schedule [POST]
// @Summary Create Schedule
// @Description Create Schedule
// @Tags Schedule
// @Accept json
// @Procedure json
// @Param Schedule body models.ScheduleCreate true "CreateScheduleRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateSchedule(c *gin.Context) {
	var (
		scheduleCreate models.ScheduleCreate
	)

	err := c.ShouldBindJSON(&scheduleCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Schedule Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	if !helper.IsValidUUID(scheduleCreate.ServiceID) {
		h.logger.Error("Invalid Service ID!")
		c.JSON(http.StatusBadRequest, models.ErrorMessage{Phone: "Invalid Service ID"})
		return
	}

	//if scheduleCreate.Day > time.Now().String() {
	//	h.logger.Error("Invalid Day!")
	//	c.JSON(http.StatusBadRequest, models.ErrorMessage{Phone: "Invalid Day"})
	//	return
	//}

	resp, err := h.storage.Schedule().Create(c.Request.Context(), &scheduleCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Schedule.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create Schedule Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID Schedule godoc
// @ID get_by_id_schedule
// @Router /ibron/api/v1/schedule/{id} [GET]
// @Summary Get By ID Schedule
// @Description Get By ID Schedule
// @Tags Schedule
// @Accept json
// @Procedure json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdSchedule(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	user, err := h.storage.Schedule().GetByID(c.Request.Context(), &models.SchedulePrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Schedule.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID Schedule Response!")
	c.JSON(http.StatusOK, user)
}

// GetList Schedule godoc
// @ID get_list_schedule
// @Router /ibron/api/v1/schedules [GET]
// @Summary Get List Schedule
// @Description Get List Schedule
// @Tags Schedule
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param service_id query string false "service_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListSchedule(c *gin.Context) {
	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListSchedule INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListSchedule INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}
	service_id := c.Query("service_id")

	resp, err := h.storage.Schedule().GetList(c.Request.Context(), &models.ScheduleGetListRequest{
		Offset:    offset,
		Limit:     limit,
		ServiceId: service_id,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Schedule.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListSchedule Response!")
	c.JSON(http.StatusOK, resp)
}

// Update Schedule godoc
// @ID update_schedule
// @Router /ibron/api/v1/schedule/{id} [PUT]
// @Summary Update Schedule
// @Description Update Schedule
// @Tags Schedule
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Schedule body models.ScheduleUpdate true "UpdateScheduleRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UpdateSchedule(c *gin.Context) {
	var (
		id             = c.Param("id")
		scheduleUpdate models.ScheduleUpdate
	)

	if !helper.IsValidUUID(id) {
		h.logger.Error("is invalid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	err := c.ShouldBindJSON(&scheduleUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Schedule Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	scheduleUpdate.Id = id
	rowsAffected, err := h.storage.Schedule().Update(c.Request.Context(), &scheduleUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Schedule.Update!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if rowsAffected <= 0 {
		h.logger.Error("storage.Schedule.Update!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
		return
	}

	resp, err := h.storage.Schedule().GetByID(c.Request.Context(), &models.SchedulePrimaryKey{Id: scheduleUpdate.Id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Schedule.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Update Schedule Successfully!")
	c.JSON(http.StatusAccepted, resp)
}

// Delete Schedule godoc
// @ID delete_schedule
// @Router /ibron/api/v1/schedule/{id} [DELETE]
// @Summary Delete Schedule
// @Description Delete Schedule
// @Tags Schedule
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteSchedule(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.Schedule().Delete(c.Request.Context(), &models.SchedulePrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Schedule.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Schedule Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}
