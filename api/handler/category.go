package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"ibron/models"
	"ibron/pkg/helper"
)

// Create Categories godoc
// @ID create_Categories
// @Router /ibron/api/v1/category [POST]
// @Summary Create Categories
// @Description Create Categories
// @Tags Category
// @Accept json
// @Procedure json
// @Param user body models.CategoryCreate true "CreateCategoryRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateCategory(c *gin.Context) {
	var (
		createCategory models.CategoryCreate
	)

	err := c.ShouldBindJSON(&createCategory)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Category Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	resp, err := h.storage.Category().Create(c.Request.Context(), &createCategory)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Category.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create User Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID Categories godoc
// @ID get_by_id_Categories
// @Router /ibron/api/v1/category/{id} [GET]
// @Summary Get By ID Categories
// @Description Get By ID Categories
// @Tags Category
// @Accept json
// @Procedure json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdCategory(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.storage.Category().GetByID(c.Request.Context(), &models.CategoryPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Category.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID Category Response!")
	c.JSON(http.StatusOK, resp)
}

// GetList Categories godoc
// @ID get_list_Categories
// @Router /ibron/api/v1/category [GET]
// @Summary Get List Categories
// @Description Get List Categories
// @Tags Category
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param filter query string false "filter"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListCategory(c *gin.Context) {
	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListCategory INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListCategory INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	filter := c.Query("filter")
	search := c.Query("search")

	resp, err := h.storage.Category().GetList(c.Request.Context(), &models.CategoryGetListRequest{
		Offset: offset,
		Limit:  limit,
		Filter: filter,
		Search: search,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Category.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListCategory Response!")
	c.JSON(http.StatusOK, resp)
}

// Update Categories godoc
// @ID update_Categories
// @Router /ibron/api/v1/category/{id} [PUT]
// @Summary Update Categories
// @Description Update Categories
// @Tags Category
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Branch body models.CategoryUpdate true "UpdateCategoryRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UpdateCategory(c *gin.Context) {
	var (
		id             = c.Param("id")
		updateCategory models.CategoryUpdate
	)

	if !helper.IsValidUUID(id) {
		h.logger.Error("is invalid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	err := c.ShouldBindJSON(&updateCategory)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Category Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	updateCategory.Id = id
	rowsAffected, err := h.storage.Category().Update(c.Request.Context(), &updateCategory)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Category.Update!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if rowsAffected <= 0 {
		h.logger.Error("storage.Category.Update!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
		return
	}

	resp, err := h.storage.Category().GetByID(c.Request.Context(), &models.CategoryPrimaryKey{Id: updateCategory.Id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Category.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Update Category Successfully!")
	c.JSON(http.StatusAccepted, resp)
}

// Delete Categories godoc
// @ID delete_Categories
// @Router /ibron/api/v1/category/{id} [DELETE]
// @Summary Delete Categories
// @Description Delete Categories
// @Tags Category
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteCategory(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.Category().Delete(c.Request.Context(), &models.CategoryPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Category.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Category Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}
