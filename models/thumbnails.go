package models

type Thumbnail struct {
	ID        string `json:"id"`
	ServiceID string `json:"service_id"`
	Url       string `json:"url"`
}

type ThumbnailCreate struct {
	ServiceID string `json:"service_id"`
	Url       string `json:"url"`
}

type ThumbnailPrimaryKey struct {
	ID string `json:"id"`
}

type ThumbnailGetListRequest struct {
	Offset int `json:"offset"`
	Limit  int `json:"limit"`
}

type ThumbnailGetListResponse struct {
	Thumbnails []*Thumbnail `json:"thumbnails"`
	Count      int          `json:"count"`
}
