package models

type Merchant struct {
	Id          string `json:"id"`
	Name        string `json:"name"`
	Login       string `json:"login"`
	Password    string `json:"password"`
	PhoneNumber string `json:"phone_number"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
}

type MerchantCreate struct {
	Name        string `json:"name"`
	Login       string `json:"login"`
	Password    string `json:"password"`
	PhoneNumber string `json:"phone_number"`
}

type MerchantUpdate struct {
	Id          string `json:"id"`
	Name        string `json:"name"`
	Login       string `json:"login"`
	Password    string `json:"password"`
	PhoneNumber string `json:"phone_number"`
}

type MerchantPrimaryKey struct {
	Id   string `json:"id"`
	Date string `json:"date"`
}

type MerchantGetListRequest struct {
	Offset int    `json:"offset"`
	Limit  int    `json:"limit"`
	Filter string `json:"filter"`
	Search string `json:"search"`
}

type MerchantGetListResponse struct {
	Count     int         `json:"count"`
	Merchants []*Merchant `json:"merchants"`
}

type UpdatePassword struct {
	PhoneNumber string `json:"phone_number"`
	Password    string `json:"password"`
}

type MerchantLoginRequest struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type LoginResponse struct {
	Merchant     *Merchant `json:"merchant"`
	AccessToken  string    `json:"access_token"`
	RefreshToken string    `json:"refresh_token"`
}
