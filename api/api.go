package api

import (
	_ "ibron/api/docs"
	"ibron/api/handler"
	"ibron/config"
	"ibron/pkg/logger"
	"ibron/storage"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func NewApi(r *gin.Engine, cfg *config.Config, storage storage.StorageI, logger logger.LoggerI) {

	h := handler.NewHandler(cfg, storage, logger)
	r.Use(customCORSMiddleware())
	v1 := r.Group("/ibron/api/v1")

	v1.POST("/user", h.CreateUser)
	v1.GET("/user/:id", h.GetByIdUser)
	v1.GET("/users", h.GetListUser)
	v1.PUT("/user/:id", h.UpdateUser)
	v1.DELETE("/user/:id", h.DeleteUser)
	v1.GET("/user/by-phone-number", h.GetByPhoneNumberUser)

	v1.POST("/merchant", h.CreateMerchant)
	v1.GET("/merchant/:id", h.GetByIdMerchant)
	v1.GET("/merchants", h.GetListMerchant)
	v1.PUT("/merchant/:id", h.UpdateMerchant)
	v1.DELETE("/merchant/:id", h.DeleteMerchant)
	v1.POST("/merchant/login", h.MerchantLogin)

	v1.POST("/banner", h.CreateBanner)
	v1.GET("/banner/:id", h.GetByIdBanner)
	v1.GET("/banner", h.GetListBanner)
	v1.PUT("/banner/:id", h.UpdateBanner)
	v1.DELETE("/banner/:id", h.DeleteBanner)

	v1.POST("/business_merchant", h.CreateBusinessMerchant)
	v1.GET("/business_merchant/:id", h.GetByIdBusinessMerchant)
	v1.GET("/business_merchants", h.GetListBusinessMerchant)
	v1.PUT("/business_merchant/:id", h.UpdateBusinessMerchant)
	v1.DELETE("/business_merchant/:id", h.DeleteBusinessMerchant)
	// v1.POST("/business_merchant/upload", h.UploadBusinessPhotos)
	//v1.POST("/closest-business", h.GetListClosestBusinessMerchant)

	v1.POST("/service_photo", h.CreateServicePhoto)
	v1.GET("/service_photo/:id", h.GetByIdServicePhoto)
	v1.GET("/service_photos", h.GetListServicePhoto)
	v1.PUT("/service_photo/:id", h.UpdateServicePhoto)
	v1.DELETE("/service_photo/:id", h.DeleteServicePhoto)

	v1.POST("/service-amenity", h.CreateServiceAmenity)
	v1.GET("/service-amenity/:id", h.GetByIdServiceAmenity)
	v1.GET("/service-amenity", h.GetListServiceAmenity)
	v1.PUT("/service-amenity/:id", h.UpdateServiceAmenity)
	v1.DELETE("/service-amenity/:id", h.DeleteServiceAmenity)

	v1.POST("/service", h.CreateServices)
	v1.GET("/service/:id", h.GetByIdServices)
	v1.GET("/services", h.GetListServices)
	v1.GET("/false-services", h.GetListFalseServices)
	v1.GET("/true-services", h.GetListTrueServices)
	v1.GET("/recommendation-services", h.RecommendationServices)
	v1.PUT("/service/:id", h.UpdateServices)
	v1.DELETE("/service/:id", h.DeleteServices)
	v1.POST("/closest-service", h.GetListClosestService)
	v1.POST("/upload-service-photos", h.UploadServicePhotos)

	v1.POST("/schedule", h.CreateSchedule)
	v1.GET("/schedule/:id", h.GetByIdSchedule)
	v1.GET("/schedules", h.GetListSchedule)
	v1.PUT("/schedule/:id", h.UpdateSchedule)
	v1.DELETE("/schedule/:id", h.DeleteSchedule)

	v1.POST("/request", h.CreateRequest)
	v1.GET("/requests/service", h.GetListApprovedRequestsByServiceId)
	v1.GET("/request/:id", h.GetByIdRequest)
	v1.GET("/approve-request/:id", h.ApproveRequest)
	v1.GET("/confirm-request", h.ConfirmRequest)
	v1.GET("/requests", h.GetListRequest)
	v1.GET("/report-request", h.GetRequestsReport)
	v1.GET("/report-diagram-request/:id", h.GetReportDiagram)
	v1.GET("/report-for-month-request/:id", h.GetRequestsReportForMonth)
	v1.GET("/report", h.GetReport)
	v1.GET("/requests/free-times", h.GetListFreeTime)
	v1.GET("/finished-requests", h.GetListFinishedRequest)
	v1.GET("/approved-requests", h.GetListApprovedRequest)
	v1.PUT("/request/:id", h.UpdateRequest)
	v1.PUT("/request-status-approved/:id", h.UpdateRequestStatusApproved)
	v1.PUT("/request-status-canceled-client/:id", h.UpdateRequestStatusCancel)
	v1.PUT("/request-status-canceled-merchant/:id", h.UpdateStatusByMerchantID)
	v1.PUT("/update-request-status", h.UpdateStatus)
	v1.DELETE("/request/:id", h.DeleteRequest)

	v1.POST("/client", h.CreateClient)
	v1.GET("/client/:id", h.GetByIdClient)
	v1.GET("/clients", h.GetListClient)
	v1.GET("/by-merchant-clients", h.GetListByMerchantIDClient)
	v1.PUT("/client/:id", h.UpdateClient)
	v1.DELETE("/client/:id", h.DeleteClient)

	v1.POST("/category", h.CreateCategory)
	v1.GET("/category/:id", h.GetByIdCategory)
	v1.GET("/category", h.GetListCategory)
	v1.PUT("/category/:id", h.UpdateCategory)
	v1.DELETE("/category/:id", h.DeleteCategory)

	v1.POST("/amenity", h.CreateAmenity)
	v1.GET("/amenity/:id", h.GetByIdAmenity)
	v1.GET("/amenity", h.GetListAmenity)
	v1.PUT("/amenity/:id", h.UpdateAmenity)
	v1.DELETE("/amenity/:id", h.DeleteAmenity)

	v1.POST("/working-hours", h.CreateWorkingHours)
	v1.GET("/working-hours/:id", h.GetByIdWorkingHours)
	v1.GET("/working-hours-service-id", h.GetByServiceIdWorkingHours)
	v1.GET("/working-hours", h.GetListWorkingHours)
	v1.PUT("/working-hours/:id", h.UpdateWorkingHours)
	v1.DELETE("/working-hours/:id", h.DeleteWorkingHours)

	v1.POST("/favorite", h.CreateFavorite)
	v1.GET("/favorites", h.GetListFavorite)
	v1.DELETE("/favorite", h.DeleteFavorite)

	v1.POST("/upload-files", h.UploadFiles)
	v1.POST("/upload-photo", h.UploadPhotos)
	v1.POST("/upload-banner-files", h.UploadBannerFiles)

	v1.POST("send-otp", h.SendOTP)
	v1.POST("verify-code", h.VerifyCode)

	v1.POST("sms-user", h.CreateSmsUser)
	//v1.GET("sms-user/:id", h.GetByIdSmsUser)

	url := ginSwagger.URL("swagger/doc.json")
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
}

func customCORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE, HEAD")
		c.Header("Access-Control-Allow-Headers", "Platform-Id, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
