package handler

import (
	"ibron/models"
	"ibron/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Create Service Photos godoc
// @ID create_service_photos
// @Router /ibron/api/v1/service_photo [POST]
// @Summary Create Service Photos
// @Description Create Service Photos
// @Tags ServicePhoto
// @Accept json
// @Procedure json
// @Param Service_photo body models.ServicePhotoCreate true "CreateServicePhotoRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateServicePhoto(c *gin.Context) {
	var (
		servicePhotoCreate models.ServicePhotoCreate
	)

	err := c.ShouldBindJSON(&servicePhotoCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Service Photo Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	if !helper.IsValidUUID(servicePhotoCreate.BusinessID) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	if !helper.IsValidUUID(servicePhotoCreate.ServiceID) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.storage.ServicePhotos().Create(c.Request.Context(), &servicePhotoCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error ServicePhoto.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create Business Photos Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID Service Photos godoc
// @ID get_by_id_service_photos
// @Router /ibron/api/v1/service_photo/{id} [GET]
// @Summary Get By ID Service Photos
// @Description Get By ID Service Photos
// @Tags ServicePhoto
// @Accept json
// @Procedure json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdServicePhoto(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	businessMerchant, err := h.storage.ServicePhotos().GetByID(c.Request.Context(), &models.ServicePhotoPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.BusinessPhotos.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID Business Photos Response!")
	c.JSON(http.StatusOK, businessMerchant)
}

// GetList Service Photos godoc
// @ID get_list_service_photos
// @Router /ibron/api/v1/service_photos [GET]
// @Summary Get List Service Photos
// @Description Get List Service Photos
// @Tags ServicePhoto
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListServicePhoto(c *gin.Context) {
	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListBusinessPhotos INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListBusinessPhotos INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	search := c.Query("search")

	resp, err := h.storage.ServicePhotos().GetList(c.Request.Context(), &models.ServicePhotoGetListRequest{
		Offset: offset,
		Limit:  limit,
		Search: search,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.ServicePhotos.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListServicePhotos Response!")
	c.JSON(http.StatusOK, resp)
}

// Update Service Photos godoc
// @ID update_service_photos
// @Router /ibron/api/v1/service_photo/{id} [PUT]
// @Summary Update Service Photos
// @Description Update Service Photos
// @Tags ServicePhoto
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param service_photo_update body models.ServicePhotoUpdate true "UpdateServicePhotosRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UpdateServicePhoto(c *gin.Context) {
	var (
		id                  = c.Param("id")
		businessPhotoUpdate models.ServicePhotoUpdate
	)

	if !helper.IsValidUUID(id) {
		h.logger.Error("is invalid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	err := c.ShouldBindJSON(&businessPhotoUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Service Photos Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	businessPhotoUpdate.Id = id
	rowsAffected, err := h.storage.ServicePhotos().Update(c.Request.Context(), &businessPhotoUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.ServicePhoto.Update!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if rowsAffected <= 0 {
		h.logger.Error("storage.ServicePhoto.Update!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
		return
	}

	resp, err := h.storage.ServicePhotos().GetByID(c.Request.Context(), &models.ServicePhotoPrimaryKey{Id: businessPhotoUpdate.Id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.ServicePhoto.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Update Service Photos Successfully!")
	c.JSON(http.StatusAccepted, resp)
}

// Delete Service Photos godoc
// @ID delete_service_merchants
// @Router /ibron/api/v1/service_photo/{id} [DELETE]
// @Summary Delete Service Photos
// @Description Delete Service Photos
// @Tags ServicePhoto
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteServicePhoto(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.ServicePhotos().Delete(c.Request.Context(), &models.ServicePhotoPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.ServicePhotos.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Service Photos Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}
