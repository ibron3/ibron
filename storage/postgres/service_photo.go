package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"ibron/models"
	"ibron/pkg/helper"
	"ibron/pkg/logger"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type servicePhotosRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewServicePhotosRepo(db *pgxpool.Pool, log logger.LoggerI) *servicePhotosRepo {
	return &servicePhotosRepo{
		db:  db,
		log: log,
	}
}

func (u *servicePhotosRepo) Create(ctx context.Context, req *models.ServicePhotoCreate) (*models.ServicePhoto, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "service_photos"(id, business_id, service_id, url)
		VALUES ($1, $2, $3, $4) 
		
	`
	if _, err := u.db.Exec(ctx, query,
		id,
		req.BusinessID,
		req.ServiceID,
		req.Url,
	); err != nil {
		u.log.Error("error is while creating business photos", logger.Error(err))
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.ServicePhotoPrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id business photos", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *servicePhotosRepo) GetByID(ctx context.Context, req *models.ServicePhotoPrimaryKey) (*models.ServicePhoto, error) {
	var (
		query string

		id         sql.NullString
		businessID sql.NullString
		serviceID  sql.NullString
		url        sql.NullString
		createdAt  sql.NullString
		updatedAt  sql.NullString
	)

	query = `
		SELECT 
			id, 
			business_id, 
			service_id, 
			url,
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "service_photos" 
		WHERE id =$1
	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&businessID,
		&serviceID,
		&url,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		u.log.Error("error is while getting business photo by id", logger.Error(err))
		return nil, err
	}

	return &models.ServicePhoto{
		Id:         id.String,
		BusinessID: businessID.String,
		ServiceID:  serviceID.String,
		Url:        url.String,
		CreatedAt:  createdAt.String,
		UpdatedAt:  updatedAt.String,
	}, nil
}

func (u *servicePhotosRepo) GetList(ctx context.Context, req *models.ServicePhotoGetListRequest) (*models.ServicePhotoGetListResponse, error) {
	var (
		resp    = &models.ServicePhotoGetListResponse{}
		query   string
		where   = " WHERE TRUE"
		offset  = " OFFSET 0"
		limit   = " LIMIT 10"
		filter  = " ORDER BY created_at DESC"
		groupBy = " GROUP BY id  "
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id, 
			business_id, 
			service_id, 
			url,
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "service_photos" 
		
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += " AND (business_id ILIKE " + "'" + req.Search + " OR service_id ILIKE " + ")"
	}

	query += where + groupBy + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting business photos list", logger.Error(err))
		return nil, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			businessID sql.NullString
			serviceID  sql.NullString
			url        sql.NullString
			createdAt  sql.NullString
			updatedAt  sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&businessID,
			&serviceID,
			&url,
			&createdAt,
			&updatedAt,
		)
		if err != nil {
			u.log.Error("error is while getting business merchant list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.ServicePhoto = append(resp.ServicePhoto, &models.ServicePhoto{
			Id:         id.String,
			BusinessID: businessID.String,
			ServiceID:  serviceID.String,
			Url:        url.String,
			CreatedAt:  createdAt.String,
			UpdatedAt:  updatedAt.String,
		})
	}
	return resp, nil
}

func (u *servicePhotosRepo) Update(ctx context.Context, req *models.ServicePhotoUpdate) (int64, error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"service_photos"
		SET
			business_id = :business_id, 
			service_id = :service_id, 
			url = :url,
			updated_at = NOW()
		WHERE id = :id
	`
	params = map[string]interface{}{
		"id":          req.Id,
		"business_id": req.BusinessID,
		"service_id":  req.ServiceID,
		"url":         req.Url,
	}

	query, args := helper.ReplaceQueryParams(query, params)
	result, err := u.db.Exec(ctx, query, args...)
	if err != nil {
		u.log.Error("error is while updating business photos data", logger.Error(err))
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (u *servicePhotosRepo) Delete(ctx context.Context, req *models.ServicePhotoPrimaryKey) error {
	_, err := u.db.Exec(ctx, `DELETE FROM service_photos WHERE id=$1 OR service_id =$1`, req.Id)
	if err != nil {
		u.log.Error("error is while deleting business photos", logger.Error(err))
		return err
	}

	return nil
}

func (u *servicePhotosRepo) GetServicePhotos(ctx context.Context, req *models.ServicePrimaryKey) (*models.ServicePhotoResponse, error) {

	var urls []*models.Url
	var resp = &models.ServicePhotoResponse{}
	query := `
		SELECT
			url
		FROM "service_photos" 
		WHERE service_id = $1
	`

	rows, err := u.db.Query(ctx, query, req.Id)
	if err != nil {
		u.log.Error("error is while getting service photos list", logger.Error(err))
		return nil, err
	}

	for rows.Next() {
		var (
			url sql.NullString
		)

		err = rows.Scan(
			&url,
		)
		if err != nil {
			u.log.Error("error is while getting service photo(scanning data)", logger.Error(err))
			return nil, err
		}

		urls = append(urls, &models.Url{
			Url: url.String,
		})

	}

	resp.Url = urls

	return resp, nil
}

// func (u *businessPhotosRepo) GetListPhotoByBusinessID(ctx context.Context, req *models.BusinessMerchantPrimaryKey) (models.BusinessPhotoResponse, error) {

// 	query := `
// 		SELECT
// 			url,
// 		FROM "business_photos"
// 		WHERE business_id =$1

// 	`
// 	var resp = &models.BusinessPhotoResponse{}

// 	rows, err := u.db.Query(ctx, query,req.Id)
// 	if err != nil {
// 		u.log.Error("error is while getting business photos list", logger.Error(err))
// 		return nil, err
// 	}

// 	for rows.Next() {
// 		var (
// 			url        sql.NullString
// 		)

// 		err = rows.Scan(
// 			&url,
// 		)
// 		if err != nil {
// 			u.log.Error("error is while getting business merchant list (scanning data)", logger.Error(err))
// 			return nil, err
// 		}
// 		resp.Url = append(resp.Url, &models.Url{Url: url.String})

// 	}
// 	return resp, nil
// }
