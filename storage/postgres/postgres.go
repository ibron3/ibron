package postgres

import (
	"context"
	"fmt"
	"ibron/config"
	"ibron/pkg/logger"
	"ibron/storage"

	"github.com/jackc/pgx/v4/pgxpool"
)

type store struct {
	db               *pgxpool.Pool
	log              logger.LoggerI
	user             *userRepo
	merchant         *merchantRepo
	businessMerchant *businessMerchantRepo
	servicePhotos    *servicePhotosRepo
	service          *serviceRepo
	schedule         *scheduleRepo
	request          *requestRepo
	client           *clientRepo
	category         *categoryRepo
	banner           *bannerRepo
	amenity          *amenityRepo
	serviceAmenity   *serviceAmenityRepo
	workingHours     *workingHoursRepo
	favorite         *favoriteRepo
	confirmed        *confirmedRepo
	register         *registerRepo
	thumbnail        *thumbnailsRepo
	smsUser          *smsRepo
}

func NewConnectionPostgres(cfg *config.Config) (storage.StorageI, error) {

	connect, err := pgxpool.ParseConfig(fmt.Sprintf(
		"host=%s user=%s dbname=%s password=%s port=%d sslmode=require",
		cfg.PostgresHost,
		cfg.PostgresUser,
		cfg.PostgresDatabase,
		cfg.PostgresPassword,
		cfg.PostgresPort,
	))

	if err != nil {
		return nil, err
	}
	connect.MaxConns = cfg.PostgresMaxConnection

	pgxpool, err := pgxpool.ConnectConfig(context.Background(), connect)
	if err != nil {
		return nil, err
	}
	var loggerLevel = new(string)
	log := logger.NewLogger("app", *loggerLevel)
	defer func() {
		err := logger.Cleanup(log)
		if err != nil {
			return
		}
	}()
	return &store{
		db:  pgxpool,
		log: logger.NewLogger("app", *loggerLevel),
	}, nil
}

func (s *store) Close() {
	s.db.Close()
}

func (s *store) User() storage.UserI {
	if s.user == nil {
		s.user = NewUserRepo(s.db, s.log)
	}

	return s.user
}

func (s *store) Merchant() storage.MerchantI {
	if s.merchant == nil {
		s.merchant = NewMerchantRepo(s.db, s.log)
	}

	return s.merchant
}

func (s *store) BusinessMerchant() storage.BusinessMerchantI {
	if s.businessMerchant == nil {
		s.businessMerchant = NewBusinessMerchantRepo(s.db, s.log)
	}

	return s.businessMerchant
}

func (s *store) ServicePhotos() storage.ServicePhotosI {
	if s.servicePhotos == nil {
		s.servicePhotos = NewServicePhotosRepo(s.db, s.log)
	}

	return s.servicePhotos
}

func (s *store) Service() storage.ServiceI {
	if s.service == nil {
		s.service = NewServiceRepo(s.db, s.log)
	}

	return s.service
}

func (s *store) Schedule() storage.ScheduleI {
	if s.schedule == nil {
		s.schedule = NewScheduleRepo(s.db, s.log)
	}

	return s.schedule
}

func (s *store) Request() storage.RequestI {
	if s.request == nil {
		s.request = NewRequestRepo(s.db, s.log)
	}

	return s.request
}

func (s *store) Client() storage.ClientI {
	if s.client == nil {
		s.client = NewClientRepo(s.db, s.log)
	}

	return s.client
}

func (s *store) Category() storage.CategoryI {
	if s.category == nil {
		s.category = NewCategoryRepo(s.db, s.log)
	}
	return s.category
}

func (s *store) Banner() storage.BannerI {
	if s.banner == nil {
		s.banner = NewBannerRepo(s.db, s.log)
	}
	return s.banner
}

func (s *store) Amenity() storage.AmenityI {
	if s.amenity == nil {
		s.amenity = NewAmenityRepo(s.db, s.log)
	}
	return s.amenity
}

func (s *store) ServiceAmenity() storage.ServiceAmenityI {
	if s.serviceAmenity == nil {
		s.serviceAmenity = NewServiceAmenityRepo(s.db, s.log)
	}
	return s.serviceAmenity
}

func (s *store) WorkingHours() storage.WorkingHoursI {
	if s.workingHours == nil {
		s.workingHours = NewWorkingHoursRepo(s.db, s.log)
	}
	return s.workingHours
}

func (s *store) Favorite() storage.FavoriteI {
	if s.favorite == nil {
		s.favorite = NewFavoriteRepo(s.db, s.log)
	}
	return s.favorite
}

func (s *store) Confirmed() storage.ConfirmedI {
	if s.confirmed == nil {
		s.confirmed = NewConfirmedRepo(s.db, s.log)
	}
	return s.confirmed
}

func (s *store) Register() storage.RegisterI {
	if s.register == nil {
		s.register = NewRegisterRepo(s.db, s.log)
	}
	return s.register
}

func (s *store) Thumbnail() storage.ThumbnailI {
	if s.thumbnail == nil {
		s.thumbnail = NewThumbnailsRepo(s.db, s.log)
	}
	return s.thumbnail
}

func (s *store) SmsUser() storage.SmsUserI {
	if s.smsUser == nil {
		s.smsUser = NewSmsRepo(s.db, s.log)
	}
	return s.smsUser
}
