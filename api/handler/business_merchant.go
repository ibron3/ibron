package handler

import (
	"ibron/models"
	"ibron/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Create Business Merchants godoc
// @ID create_business_merchants
// @Router /ibron/api/v1/business_merchant [POST]
// @Summary Create Business Merchants
// @Description Create Business Merchants
// @Tags BusinessMerchant
// @Accept json
// @Procedure json
// @Param business_merchant body models.BusinessMerchantCreate true "CreateBusinessMerchantRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateBusinessMerchant(c *gin.Context) {
	var (
		businessMerchantCreate models.BusinessMerchantCreate
	)

	err := c.ShouldBindJSON(&businessMerchantCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Business Merchant Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	if !helper.IsValidUUID(businessMerchantCreate.MerchantID) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	if !helper.IsValidPhone(businessMerchantCreate.Phone1) {
		h.logger.Error("Invalid Phone1 Number!")
		c.JSON(http.StatusBadRequest, models.ErrorMessage{Phone: "Invalid Phone1 Number"})
		return
	}

	resp, err := h.storage.BusinessMerchant().Create(c.Request.Context(), &businessMerchantCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error BusinessMerchant.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create Business Merchant Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID Business Merchants godoc
// @ID get_by_id_business_merchants
// @Router /ibron/api/v1/business_merchant/{id} [GET]
// @Summary Get By ID Business Merchants
// @Description Get By ID Business Merchants
// @Tags BusinessMerchant
// @Accept json
// @Procedure json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdBusinessMerchant(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	businessMerchant, err := h.storage.BusinessMerchant().GetByID(c.Request.Context(), &models.BusinessMerchantPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.BusinessMerchant.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID Business Merchant Response!")
	c.JSON(http.StatusOK, businessMerchant)
}

//// GetList Closest Business Merchants godoc
//// @ID get_list_closest_business_merchants
//// @Router /ibron/api/v1/closest-business [POST]
//// @Summary Get List Closest Business Merchants
//// @Description Get List Closest Business Merchants
//// @Tags BusinessMerchant
//// @Accept json
//// @Procedure json
//// @Param location body models.Location true "Location request"
//// @Success 200 {object} Response{data=string} "Success Request"
//// @Response 400 {object} Response{data=string} "Bad Request"
//// @Failure 500 {object} Response{data=string} "Server error"
//func (h *handler) GetListClosestBusinessMerchant(c *gin.Context) {
//
//	var location models.Location
//	err := c.ShouldBindJSON(&location)
//	if err!=nil{
//		c.JSON(400,"Bad Request")
//		return
//	}
//
//
//	business, err := h.storage.BusinessMerchant().GetList(c.Request.Context(),&models.BusinessMerchantGetListRequest{
//		Offset: 0,
//		Limit : 10000,
//	})
//
//
//
//	resp := helper.ClosestLocation(location ,business.BusinessMerchants)
//
//	c.JSON(200,resp.BusinessMerchants)
//
//}

// GetList Business Merchants godoc
// @ID get_list_business_merchants
// @Router /ibron/api/v1/business_merchants [GET]
// @Summary Get List Business Merchants
// @Description Get List Business Merchants
// @Tags BusinessMerchant
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param filter query string false "filter"
// @Param search query string false "search"
// @Param merchant_id query string false "merchant_id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListBusinessMerchant(c *gin.Context) {
	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListUser INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListUser INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	filter := c.Query("filter")
	search := c.Query("search")
	merchant_id := c.Query("merchant_id")

	resp, err := h.storage.BusinessMerchant().GetList(c.Request.Context(), &models.BusinessMerchantGetListRequest{
		Offset:     offset,
		Limit:      limit,
		Filter:     filter,
		Search:     search,
		MerchantId: merchant_id,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.BusinessMerchant.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListBusinessMerchant Response!")
	c.JSON(http.StatusOK, resp)
}

// Update Business Merchants godoc
// @ID update_business_merchants
// @Router /ibron/api/v1/business_merchant/{id} [PUT]
// @Summary Update Business Merchants
// @Description Update Business Merchants
// @Tags BusinessMerchant
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param business_merchant body models.BusinessMerchantUpdate true "UpdateBusinessMerchantRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UpdateBusinessMerchant(c *gin.Context) {
	var (
		id                     = c.Param("id")
		businessMerchantUpdate models.BusinessMerchantUpdate
	)

	if !helper.IsValidUUID(id) {
		h.logger.Error("is invalid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	err := c.ShouldBindJSON(&businessMerchantUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Business Merchant Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	businessMerchantUpdate.Id = id
	rowsAffected, err := h.storage.BusinessMerchant().Update(c.Request.Context(), &businessMerchantUpdate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.BusinessMerchant.Update!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if rowsAffected <= 0 {
		h.logger.Error("186" + "  :  " + "storage.BusinessMerchant.Update!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
		return
	}

	resp, err := h.storage.BusinessMerchant().GetByID(c.Request.Context(), &models.BusinessMerchantPrimaryKey{Id: businessMerchantUpdate.Id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.BusinessMerchant.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Update Business Merchant Successfully!")
	c.JSON(http.StatusAccepted, resp)
}

// Delete Business Merchants godoc
// @ID delete_business_merchants
// @Router /ibron/api/v1/business_merchant/{id} [DELETE]
// @Summary Delete Business Merchants
// @Description Delete Business Merchants
// @Tags BusinessMerchant
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteBusinessMerchant(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.BusinessMerchant().Delete(c.Request.Context(), &models.BusinessMerchantPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.BusinessMerchant.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Business Merchant Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}

// Uploas Business photos godoc
// @ID Upload Business photos
// @Router /ibron/api/v1/business_merchant/upload [POST]
// @Summary Upload Business Photos
// @Description Upload Business Photos
// @Tags BusinessMerchant
// @Accept multipart/form-data
// @Procedure json
// @Param file formData []file true "Photos"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"

// func (h *handler) UploadBusinessPhotos(c *gin.Context) {
// 	photos, err := c.MultipartForm()
// 	if err != nil {
// 		h.logger.Error(err.Error() + "  :  " + "File error")
// 		return
// 	}

// 	 resp, err := helper.(photos)
// 	if err != nil {
// 		h.logger.Error(err.Error() + "  :  " + "File error")
// 		c.JSON(500, "Server error")
// 		return
// 	}

// 	c.JSON(200, resp)
// }
