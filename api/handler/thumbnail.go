package handler

import (
	"ibron/models"
	"ibron/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Create Thumbnail godoc
// @ID create_thumbnail
// @Router /ibron/api/v1/thumbnail [POST]
// @Summary Create Thumbnail
// @Description Create Thumbnail
// @Tags Thumbnail
// @Accept json
// @Procedure json
// @Param thumbnail body models.ThumbnailCreate true "CreateThumbnailRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateThumbnail(c *gin.Context) {
	var (
		thumbnailCreate models.ThumbnailCreate
	)

	err := c.ShouldBindJSON(&thumbnailCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Thumbnail Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	if !helper.IsValidUUID(thumbnailCreate.ServiceID) {
		h.logger.Error("invalid service ID")
		c.JSON(http.StatusBadRequest, "invalid service ID")
		return
	}

	service, err := h.storage.Thumbnail().Create(c.Request.Context(), &thumbnailCreate)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Thumbnail.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create Thumbnail Successfully!!")
	c.JSON(http.StatusCreated, service)
}

// GetByID Thumbnail godoc
// @ID get_by_id_thumbnail
// @Router /ibron/api/v1/thumbnail/{id} [GET]
// @Summary Get By ID Thumbnail
// @Description Get By ID Thumbnail
// @Tags Thumbnail
// @Accept json
// @Procedure json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdThumbnail(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	user, err := h.storage.Thumbnail().GetByID(c.Request.Context(), &models.ThumbnailPrimaryKey{ID: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Thumbnail.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID Thumbnail Response!")
	c.JSON(http.StatusOK, user)
}

// GetList Thumbnail godoc
// @ID get_list_thumbnail
// @Router /ibron/api/v1/thumbnails [GET]
// @Summary Get List Thumbnail
// @Description Get List Thumbnail
// @Tags Thumbnail
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListThumbnail(c *gin.Context) {
	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListService INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListService INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	resp, err := h.storage.Thumbnail().GetList(c.Request.Context(), &models.ThumbnailGetListRequest{
		Offset: offset,
		Limit:  limit,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Service.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListService Response!")
	c.JSON(http.StatusOK, resp)
}
