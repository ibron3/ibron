package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"ibron/models"
	"ibron/pkg/helper"
	"ibron/pkg/logger"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type scheduleRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewScheduleRepo(db *pgxpool.Pool, log logger.LoggerI) *scheduleRepo {
	return &scheduleRepo{
		db:  db,
		log: log,
	}
}

func (u *scheduleRepo) Create(ctx context.Context, req *models.ScheduleCreate) (*models.Schedule, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "schedule"(id, service_id, day, start_time, end_time)
		VALUES ($1, $2, $3, $4, $5) 
	`

	if _, err := u.db.Exec(ctx, query,
		id,
		req.ServiceID,
		req.Day,
		req.StartTime,
		req.EndTime,
	); err != nil {
		u.log.Error("insert error" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.SchedulePrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id schedule" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *scheduleRepo) GetByID(ctx context.Context, req *models.SchedulePrimaryKey) (*models.Schedule, error) {
	var (
		query string

		id        sql.NullString
		serviceID sql.NullString
		day       sql.NullString
		startTime sql.NullString
		endTime   sql.NullString
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	query = `
		SELECT 
			id,
			service_id, 
		    day, 
		    start_time, 
		    end_time,
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "schedule" 
		WHERE deleted_at is null AND id =$1

	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&serviceID,
		&day,
		&startTime,
		&endTime,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}

	return &models.Schedule{
		Id:        id.String,
		ServiceID: serviceID.String,
		Day:       day.String,
		StartTime: startTime.String,
		EndTime:   endTime.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}, nil
}

func (u *scheduleRepo) GetList(ctx context.Context, req *models.ScheduleGetListRequest) (*models.ScheduleGetListResponse, error) {
	var (
		resp    = &models.ScheduleGetListResponse{}
		query   string
		where   = " WHERE TRUE AND deleted_at is null"
		offset  = " OFFSET 0"
		limit   = " LIMIT 10"
		filter  = " ORDER BY s.created_at DESC"
		groupBy = " GROUP BY s.id  "
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			service_id, 
		    day, 
		    TO_CHAR(start_time, 'HH24:MI'), 
		    TO_CHAR(end_time, 'HH24:MI'),
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "schedule" AS s
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if len(req.ServiceId) > 0 {
		where += " AND service_id = " + "'" + req.ServiceId + "'"
	}

	query += where + groupBy + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting schedule list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			serviceID sql.NullString
			day       sql.NullString
			startTime sql.NullString
			endTime   sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&serviceID,
			&day,
			&startTime,
			&endTime,
			&createdAt,
			&updatedAt,
		)
		if err != nil {
			u.log.Error("error is while getting schedule list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Schedule = append(resp.Schedule, &models.Schedule{
			Id:        id.String,
			ServiceID: serviceID.String,
			Day:       day.String,
			StartTime: startTime.String,
			EndTime:   endTime.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}
	return resp, nil
}

func (u *scheduleRepo) Update(ctx context.Context, req *models.ScheduleUpdate) (int64, error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"schedule"
		SET
			service_id = :service_id, 
		    day = :day, 
		    start_time = :start_time, 
		    end_time = :end_time,
			updated_at = NOW()
		WHERE id = :id
	`
	params = map[string]interface{}{
		"id":         req.Id,
		"service_id": req.ServiceID,
		"day":        req.Day,
		"start_time": req.StartTime,
		"end_time":   req.EndTime,
	}

	query, args := helper.ReplaceQueryParams(query, params)
	result, err := u.db.Exec(ctx, query, args...)
	if err != nil {
		u.log.Error("error is while updating schedule data", logger.Error(err))
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (u *scheduleRepo) Delete(ctx context.Context, req *models.SchedulePrimaryKey) error {
	_, err := u.db.Exec(ctx, `UPDATE schedule set deleted_at = now()  WHERE id = $1`, req.Id)
	if err != nil {
		u.log.Error("error is while deleting schedule", logger.Error(err))
		return err
	}

	return nil
}
