package models

type Client struct {
	Id          string `json:"id,omitempty"`
	MerchantID  string `json:"merchant_id,omitempty"`
	FirstName   string `json:"first_name,omitempty"`
	LastName    string `json:"last_name,omitempty"`
	PhoneNumber string `json:"phone_number,omitempty"`
	CreatedAt   string `json:"created_at,omitempty"`
	UpdatedAt   string `json:"updated_at,omitempty"`
}

type ClientCreate struct {
	MerchantID  string `json:"merchant_id"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	PhoneNumber string `json:"phone_number"`
}

type ClientUpdate struct {
	Id          string `json:"id"`
	MerchantID  string `json:"merchant_id"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	PhoneNumber string `json:"phone_number"`
}

type ClientPrimaryKey struct {
	Id          string `json:"id"`
	PhoneNumber string `json:"phone_number"`
	MerchantId  string `json:"merchant_id"`
}

type ClientGetListRequest struct {
	Offset     int    `json:"offset"`
	Limit      int    `json:"limit"`
	MerchantID string `json:"merchant_id"`
	Search     string `json:"search"`
}

type ClientGetListResponse struct {
	Count   int       `json:"count"`
	Clients []*Client `json:"users"`
}
