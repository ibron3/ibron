package postgres

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"ibron/models"
	"ibron/pkg/logger"
	"log"
	"time"
)

type smsRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewSmsRepo(db *pgxpool.Pool, log logger.LoggerI) *smsRepo {
	return &smsRepo{
		db:  db,
		log: log,
	}
}

func connectionLmsDB() *pgxpool.Pool {
	// lms db
	connStr := "postgresql://lms_prod:SuperSecretPassword@95.217.21.150:5432/lms_prod?sslmode=disable"

	// Create a context with a timeout
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// Create a connection pool
	pool, err := pgxpool.Connect(ctx, connStr)
	if err != nil {
		log.Fatalf("Unable to connect to database: %v\n", err)
	}

	// Verify connection
	err = pool.Ping(ctx)
	if err != nil {
		log.Fatalf("Unable to ping the database: %v\n", err)
	}

	fmt.Println("Successfully connected to the database!")

	return pool
}

func (u *smsRepo) Create(ctx context.Context, req *models.SmsCreate) error {
	pool := connectionLmsDB()

	timeZone, err := time.LoadLocation("Asia/Tashkent")
	if err != nil {
		return err
	}
	currentTime := time.Now().In(timeZone)

	query := `insert into sms_users (id, name, count, created_at) values ($1, $2, $3, $4)`

	if _, err = pool.Exec(ctx, query, req.SenderID, req.Name, req.Count, currentTime); err != nil {
		u.log.Error("error is while insert " + err.Error())
		return err
	}

	defer pool.Close()

	return nil
}

//
//func (u *smsRepo) GetByID(ctx context.Context, req *models.SmsPrimaryKey) (*models.Sms, error) {
//	var (
//		id, name, createdAt sql.NullString
//		count               sql.NullInt64
//		pool                = connectionLmsDB()
//	)
//
//	query := `select id, name, count, created_at from sms_users where id = $1 order by id desc limit 1`
//	if err := pool.QueryRow(ctx, query, req.SenderID).Scan(
//		&id,
//		&name,
//		&count,
//		&createdAt,
//	); err != nil {
//		u.log.Error("error is while scanning sms_users data" + err.Error())
//		return nil, err
//	}
//
//	defer pool.Close()
//
//	return &models.Sms{
//		SenderID: id.String,
//		Name:     name.String,
//		Count:    count.Int64,
//	}, nil
//}
