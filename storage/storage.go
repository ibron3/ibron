package storage

import (
	"context"
	"ibron/models"
)

type StorageI interface {
	Close()
	User() UserI
	Merchant() MerchantI
	BusinessMerchant() BusinessMerchantI
	ServicePhotos() ServicePhotosI
	Service() ServiceI
	Schedule() ScheduleI
	Request() RequestI
	Client() ClientI
	Category() CategoryI
	Banner() BannerI
	Amenity() AmenityI
	ServiceAmenity() ServiceAmenityI
	WorkingHours() WorkingHoursI
	Favorite() FavoriteI
	Confirmed() ConfirmedI
	Register() RegisterI
	Thumbnail() ThumbnailI
	SmsUser() SmsUserI
}

type UserI interface {
	Create(context.Context, *models.UserCreate) (*models.User, error)
	GetByID(context.Context, *models.UserPrimaryKey) (*models.User, error)
	GetList(context.Context, *models.UserGetListRequest) (*models.UserGetListResponse, error)
	Update(context.Context, *models.UserUpdate) (int64, error)
	Delete(context.Context, *models.UserPrimaryKey) error
	GetByPhoneNumber(context.Context, *models.UserPhoneNumber) (*models.User, error)
	UpdateDelete(context.Context, *models.UserPrimaryKey) error
}

type MerchantI interface {
	Create(context.Context, *models.MerchantCreate) (*models.Merchant, error)
	GetByID(context.Context, *models.MerchantPrimaryKey) (*models.Merchant, error)
	GetList(context.Context, *models.MerchantGetListRequest) (*models.MerchantGetListResponse, error)
	Update(context.Context, *models.MerchantUpdate) (int64, error)
	Delete(context.Context, *models.MerchantPrimaryKey) error
	GetMerchantCredentials(context.Context, *models.MerchantLoginRequest) (*models.Merchant, error)
}

type BusinessMerchantI interface {
	Create(context.Context, *models.BusinessMerchantCreate) (*models.BusinessMerchant, error)
	GetByID(context.Context, *models.BusinessMerchantPrimaryKey) (*models.BusinessMerchant, error)
	GetList(context.Context, *models.BusinessMerchantGetListRequest) (*models.BusinessMerchantGetListResponse, error)
	Update(context.Context, *models.BusinessMerchantUpdate) (int64, error)
	Delete(context.Context, *models.BusinessMerchantPrimaryKey) error
}

type ServicePhotosI interface {
	Create(context.Context, *models.ServicePhotoCreate) (*models.ServicePhoto, error)
	GetByID(context.Context, *models.ServicePhotoPrimaryKey) (*models.ServicePhoto, error)
	GetList(context.Context, *models.ServicePhotoGetListRequest) (*models.ServicePhotoGetListResponse, error)
	Update(context.Context, *models.ServicePhotoUpdate) (int64, error)
	Delete(context.Context, *models.ServicePhotoPrimaryKey) error
	GetServicePhotos(ctx context.Context, req *models.ServicePrimaryKey) (*models.ServicePhotoResponse, error)
}

type ServiceI interface {
	Create(context.Context, *models.ServiceCreate) (*models.Service, error)
	GetByID(context.Context, *models.ServicePrimaryKey) (*models.Service, error)
	GetList(context.Context, *models.ServiceGetListRequest) (*models.ServiceGetListResponse, error)
	GetListFalseServices(context.Context, *models.ServiceGetListRequest) (*models.ServiceGetListResponse, error)
	GetListTrueServices(context.Context, *models.ServiceGetListRequest) (*models.ServiceGetListResponse, error)
	Recommendation(context.Context, *models.ServiceGetListRequest) (*models.ServiceGetListResponse, error)
	Update(context.Context, *models.ServiceUpdate) (int64, error)
	Delete(context.Context, *models.ServicePrimaryKey) error
}

type ScheduleI interface {
	Create(context.Context, *models.ScheduleCreate) (*models.Schedule, error)
	GetByID(context.Context, *models.SchedulePrimaryKey) (*models.Schedule, error)
	GetList(context.Context, *models.ScheduleGetListRequest) (*models.ScheduleGetListResponse, error)
	Update(context.Context, *models.ScheduleUpdate) (int64, error)
	Delete(context.Context, *models.SchedulePrimaryKey) error
}

type RequestI interface {
	Create(context.Context, *models.RequestCreate) (*models.Request, error)
	GetByID(context.Context, *models.RequestPrimaryKey) (*models.Request, error)
	Report(context.Context, *models.MerchantPrimaryKey) (*models.ReportResponse, error)
	ReportForMonth(context.Context, *models.MerchantPrimaryKey) (*models.ReportResponse, error)
	ReportDiagram(context.Context, *models.MerchantPrimaryKey) (*models.ReportDiagram, error)
	GetList(context.Context, *models.RequestGetListRequest) (*models.RequestGetListResponse, error)
	GetListFinishedRequests(context.Context, *models.RequestGetListRequest) (*models.RequestGetListResponse, error)
	GetListApprovedRequests(context.Context, *models.RequestGetListRequest) (*models.RequestGetListResponse, error)
	Update(context.Context, *models.RequestUpdate) (int64, error)
	UpdateStatus(context.Context, *models.UpdateStatus) (int64, error)
	CheckTime(context.Context, *models.Request) (bool, error)
	Delete(context.Context, *models.RequestPrimaryKey) error
	GetListApprovedRequestsByServiceId(ctx context.Context, req *models.RequestGetListRequest) (*models.RequestGetListResponse, error)
}

type ClientI interface {
	Create(context.Context, *models.ClientCreate) (*models.Client, error)
	GetByID(context.Context, *models.ClientPrimaryKey) (*models.Client, error)
	GetList(context.Context, *models.ClientGetListRequest) (*models.ClientGetListResponse, error)
	GetListByMerchantID(context.Context, *models.ClientGetListRequest) (*models.ClientGetListResponse, error)
	Update(context.Context, *models.ClientUpdate) (int64, error)
	Delete(context.Context, *models.ClientPrimaryKey) error
}

type CategoryI interface {
	Create(context.Context, *models.CategoryCreate) (*models.Category, error)
	GetByID(context.Context, *models.CategoryPrimaryKey) (*models.Category, error)
	GetList(context.Context, *models.CategoryGetListRequest) (*models.CategoryGetListResponse, error)
	Update(context.Context, *models.CategoryUpdate) (int64, error)
	Delete(context.Context, *models.CategoryPrimaryKey) error
}

type BannerI interface {
	Create(context.Context, *models.BannerCreate) (*models.Banner, error)
	GetByID(context.Context, *models.BannerPrimaryKey) (*models.Banner, error)
	GetList(context.Context, *models.BannerGetListRequest) (*models.BannerGetListResponse, error)
	Update(context.Context, *models.BannerUpdate) (int64, error)
	Delete(context.Context, *models.BannerPrimaryKey) error
}

type AmenityI interface {
	Create(context.Context, *models.AmenityCreate) (*models.Amenity, error)
	GetByID(context.Context, *models.AmenityPrimaryKey) (*models.Amenity, error)
	GetList(context.Context, *models.AmenityGetListRequest) (*models.AmenityGetListResponse, error)
	Update(context.Context, *models.AmenityUpdate) (int64, error)
	Delete(context.Context, *models.AmenityPrimaryKey) error
	GetListServiceAmenity(ctx context.Context, req *models.ServicePrimaryKey) (*models.AmenityGetListResponse, error)
}

type ServiceAmenityI interface {
	Create(context.Context, *models.ServiceAmenityCreate) (*models.ServiceAmenity, error)
	GetByID(context.Context, *models.ServiceAmenityPrimaryKey) (*models.ServiceAmenity, error)
	GetList(context.Context, *models.ServiceAmenityGetListRequest) (*models.ServiceAmenityGetListResponse, error)
	Update(context.Context, *models.ServiceAmenityUpdate) (int64, error)
	Delete(context.Context, *models.ServiceAmenityPrimaryKey) error
}

type WorkingHoursI interface {
	Create(context.Context, *models.WorkingHoursCreate) (*models.WorkingHours, error)
	GetByID(context.Context, *models.WorkingHoursPrimaryKey) (*models.WorkingHours, error)
	GetByDate(context.Context, *models.WorkingHoursPrimaryKey) (*models.WorkingHours, error)
	GetList(context.Context, *models.WorkingHoursGetListRequest) (*models.WorkingHoursGetListResponse, error)
	UpdateServiceTime(context.Context, *models.WorkingHoursUpdate) (int64, error)
	Delete(context.Context, *models.WorkingHoursPrimaryKey) error
	GetFreeTimeByServiceId(context.Context, *models.FreeTime) (bool, error)
	GetListByServiceId(context.Context, *models.WorkingHoursGetListRequest) (*models.WorkingHoursGetListResponse, error)
}

type FavoriteI interface {
	Create(ctx context.Context, req *models.FavoriteCreate) (string, error)
	GetListUserFavorites(ctx context.Context, req *models.UserPrimaryKey) (*models.ServiceGetListResponse, error)
	CheckForExisting(ctx context.Context, req *models.Favorite) (bool, error)
	Delete(ctx context.Context, req *models.DeleteFavorite) error
}

type ConfirmedI interface {
	Create(context.Context, *models.Confirmed) error
}

type RegisterI interface {
	Create(context.Context, *models.Register) error
	VerifyCode(context.Context, *models.RegisterRequest) (string, error)
	DeleteVerifiedCode(context.Context, *models.RegisterRequest) error
}

type ThumbnailI interface {
	Create(context.Context, *models.ThumbnailCreate) (*models.Thumbnail, error)
	GetByID(context.Context, *models.ThumbnailPrimaryKey) (*models.Thumbnail, error)
	GetList(context.Context, *models.ThumbnailGetListRequest) (*models.ThumbnailGetListResponse, error)
}

type SmsUserI interface {
	Create(context.Context, *models.SmsCreate) error
	//GetByID(context.Context, *models.SmsPrimaryKey) (*models.Sms, error)
}
