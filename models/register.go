package models

type RegisterRequest struct {
	PhoneNumber string `json:"phone_number"`
}

type Register struct {
	PhoneNumber string `json:"phone_number"`
	Exist       bool   `json:"exist"`
	Code        string `json:"code"`
}
