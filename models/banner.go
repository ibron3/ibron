package models

type Banner struct {
	Id        string   `json:"id"`
	ServiceId string   `json:"service_id"`
	Url       string   `json:"url"`
	Service   *Service `json:"service"`
	CreatedAt string   `json:"created_at"`
	UpdatedAt string   `json:"updated_at"`
}

type BannerCreate struct {
	ServiceId string `json:"service_id"`
	Url       string `json:"url"`
}

type BannerUpdate struct {
	Id        string `json:"id"`
	ServiceId string `json:"service_id"`
	Url       string `json:"url"`
}

type BannerPrimaryKey struct {
	Id string `json:"id"`
}

type BannerGetListRequest struct {
	Offset int    `json:"offset"`
	Limit  int    `json:"limit"`
	Filter string `json:"filter"`
	Search string `json:"search"`
}

type BannerGetListResponse struct {
	Count  int       `json:"count"`
	Banner []*Banner `json:"banners"`
}
