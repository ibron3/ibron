package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"ibron/models"
	"ibron/pkg/helper"
	"ibron/pkg/logger"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type businessMerchantRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewBusinessMerchantRepo(db *pgxpool.Pool, log logger.LoggerI) *businessMerchantRepo {
	return &businessMerchantRepo{
		db:  db,
		log: log,
	}
}

func (u *businessMerchantRepo) Create(ctx context.Context, req *models.BusinessMerchantCreate) (*models.BusinessMerchant, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "business_merchants" (id, merchant_id, business_name, description, address, latitude, longitude, phone1, phone2)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) 
	`
	if _, err := u.db.Exec(ctx, query,
		id,
		req.MerchantID,
		req.BusinessName,
		req.Description,
		req.Address,
		req.Latitude,
		req.Longitude,
		req.Phone1,
		req.Phone2,
	); err != nil {
		u.log.Error("error is while creating business merchant ", logger.Error(err))
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.BusinessMerchantPrimaryKey{Id: id})
	if err != nil {
		u.log.Error("get by id merchant business error", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *businessMerchantRepo) GetByID(ctx context.Context, req *models.BusinessMerchantPrimaryKey) (*models.BusinessMerchant, error) {
	var (
		query string

		id           sql.NullString
		merchantID   sql.NullString
		businessName sql.NullString
		description  sql.NullString
		address      sql.NullString
		latitude     sql.NullFloat64
		longitude    sql.NullFloat64
		phone1       sql.NullString
		phone2       sql.NullString
		createdAt    sql.NullString
		updatedAt    sql.NullString
	)

	query = `
		SELECT 
			id,
			merchant_id, 
			business_name, 
			description,
			address, 
			latitude, 
			longitude, 
			phone1, 
			phone2,
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "business_merchants" 
		WHERE deleted_at is null AND id = $1
	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&merchantID,
		&businessName,
		&description,
		&address,
		&latitude,
		&longitude,
		&phone1,
		&phone2,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		u.log.Error("error is while getting user by id", logger.Error(err))
		return nil, err
	}

	return &models.BusinessMerchant{
		Id:           id.String,
		MerchantID:   merchantID.String,
		BusinessName: businessName.String,
		Description:  description.String,
		Address:      address.String,
		Latitude:     latitude.Float64,
		Longitude:    longitude.Float64,
		Phone1:       phone1.String,
		Phone2:       phone2.String,
		CreatedAt:    createdAt.String,
		UpdatedAt:    updatedAt.String,
	}, nil
}

func (u *businessMerchantRepo) GetList(ctx context.Context, req *models.BusinessMerchantGetListRequest) (*models.BusinessMerchantGetListResponse, error) {
	var (
		resp    = &models.BusinessMerchantGetListResponse{}
		query   string
		where   = " WHERE TRUE AND deleted_at is null"
		offset  = " OFFSET 0"
		limit   = " LIMIT 10"
		filter  = " ORDER BY created_at DESC"
		groupBy = " GROUP BY id  "
		// bp      businessPhotosRepo
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			merchant_id, 
			business_name, 
			description,
			address, 
			latitude, 
			longitude, 
			phone1, 
			phone2,
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "business_merchants" 
		
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += " AND (business_name ILIKE " + "'" + req.Search + " OR phone1 ILIKE " + "'" + req.Search + "%'" + " OR phone2 ILIKE " + "'" + req.Search + "%'" + ")"
	}

	if req.Filter == "name" {
		filter = " ORDER BY business_name "
	}

	if len(req.MerchantId) > 0 {
		where += " AND merchant_id = " + "'" + req.MerchantId + "'"
	}

	query += where + groupBy + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting business merchant list", logger.Error(err))
		return nil, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			merchantID   sql.NullString
			businessName sql.NullString
			description  sql.NullString
			address      sql.NullString
			latitude     sql.NullFloat64
			longitude    sql.NullFloat64
			phone1       sql.NullString
			phone2       sql.NullString
			createdAt    sql.NullString
			updatedAt    sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&merchantID,
			&businessName,
			&description,
			&address,
			&latitude,
			&longitude,
			&phone1,
			&phone2,
			&createdAt,
			&updatedAt,
		)
		if err != nil {
			u.log.Error("error is while getting business merchant list (scanning data)", logger.Error(err))
			return nil, err
		}
		// url, err := bp.GetListPhotoByBusinessID(context.Background(), &models.BusinessMerchantPrimaryKey{Id: id.String})
		// if err!=nil{
		// 	u.log.Error("GetListPhotoByBusinessID ", logger.Error(err))
		// 	return nil, err
		// }

		resp.BusinessMerchants = append(resp.BusinessMerchants, &models.BusinessMerchant{
			Id:           id.String,
			MerchantID:   merchantID.String,
			BusinessName: businessName.String,
			Description:  description.String,
			Address:      address.String,
			Latitude:     latitude.Float64,
			Longitude:    longitude.Float64,
			Phone1:       phone1.String,
			Phone2:       phone2.String,
			CreatedAt:    createdAt.String,
			UpdatedAt:    updatedAt.String,
		})
	}
	return resp, nil
}

func (u *businessMerchantRepo) Update(ctx context.Context, req *models.BusinessMerchantUpdate) (int64, error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"business_merchants"
		SET
			merchant_id = :merchant_id, 
			business_name = :business_name,  
			description = :description,
			address = :address, 
			latitude = :latitude, 
			longitude = :longitude, 
			phone1 = :phone1, 
			phone2 = :phone2,
			updated_at = NOW()
		WHERE id = :id
	`
	params = map[string]interface{}{
		"id":            req.Id,
		"merchant_id":   req.MerchantID,
		"business_name": req.BusinessName,
		"description":   req.Description,
		"address":       req.Address,
		"latitude":      req.Latitude,
		"longitude":     req.Longitude,
		"phone1":        req.Phone1,
		"phone2":        req.Phone2,
	}

	query, args := helper.ReplaceQueryParams(query, params)
	result, err := u.db.Exec(ctx, query, args...)
	if err != nil {
		u.log.Error("error is while updating business merchant data", logger.Error(err))
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (u *businessMerchantRepo) Delete(ctx context.Context, req *models.BusinessMerchantPrimaryKey) error {
	_, err := u.db.Exec(ctx, `UPDATE "business_merchants" SET deleted_at = now() WHERE id = $1`, req.Id)
	if err != nil {
		u.log.Error("error is while deleting business merchant", logger.Error(err))
		return err
	}

	return nil

}
