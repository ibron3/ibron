package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"ibron/models"
	"ibron/pkg/helper"
	"ibron/pkg/logger"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type serviceRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewServiceRepo(db *pgxpool.Pool, log logger.LoggerI) *serviceRepo {
	return &serviceRepo{
		db:  db,
		log: log,
	}
}

func (u *serviceRepo) Create(ctx context.Context, req *models.ServiceCreate) (*models.Service, error) {

	var (
		id    = uuid.New().String()
		view  sql.NullInt64
		query string
	)

	query = `
		INSERT INTO "services"(id,category_id,merchant_id, name, description, duration,thumbnail, price, business_merchant_id, address, longitude, latitude, view, book)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8,$9,$10, $11,$12, $13, $14) 
	`

	if _, err := u.db.Exec(ctx, query,
		id,
		req.CategoryId,
		req.MerchantId,
		req.Name,
		req.Description,
		req.Duration,
		req.Thumbnail,
		req.Price,
		req.BusinessMerchantID,
		req.Address,
		req.Longitude,
		req.Latitude,
		view,
		req.Book,
	); err != nil {
		u.log.Error("error is while creating service ", logger.Error(err))
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.ServicePrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error service get by id", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *serviceRepo) GetByID(ctx context.Context, req *models.ServicePrimaryKey) (*models.Service, error) {
	var (
		query              string
		sp                 = NewServicePhotosRepo(u.db, u.log)
		a                  = NewAmenityRepo(u.db, u.log)
		id                 sql.NullString
		category_id        sql.NullString
		name               sql.NullString
		description        sql.NullString
		duration           sql.NullFloat64
		thumbnail          sql.NullString
		price              sql.NullFloat64
		businessMerchantID sql.NullString
		address            sql.NullString
		longitude          sql.NullFloat64
		latitude           sql.NullFloat64
		view               sql.NullInt64
		book               sql.NullBool
		createdAt          sql.NullString
		updatedAt          sql.NullString
	)

	query = `
		SELECT 
			id,
			category_id,
			name, 
			description,
			duration, 
			thumbnail,
			price,
			business_merchant_id, 
			address, 
			longitude, 
			latitude,
			view,
		    book,
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "services" 
		WHERE deleted_at is null AND id = $1
	`

	err := u.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&category_id,
		&name,
		&description,
		&duration,
		&thumbnail,
		&price,
		&businessMerchantID,
		&address,
		&longitude,
		&latitude,
		&view,
		&book,
		&createdAt,
		&updatedAt,
	)

	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error is while getting service by id" + err.Error())
		return nil, err
	}

	url, err := sp.GetServicePhotos(context.Background(), &models.ServicePrimaryKey{Id: id.String})
	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error is while getting service photos", logger.Error(err))
		return nil, err
	}

	amenity, err := a.GetListServiceAmenity(context.Background(), &models.ServicePrimaryKey{Id: id.String})
	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error is while getting service amenities", logger.Error(err))
		return nil, err
	}

	return &models.Service{
		Id:                 id.String,
		Url:                url.Url,
		Amenity:            amenity.Amenity,
		CategoryId:         category_id.String,
		Name:               name.String,
		Description:        description.String,
		Duration:           duration.Float64,
		Thumbnail:          thumbnail.String,
		Price:              price.Float64,
		BusinessMerchantID: businessMerchantID.String,
		Address:            address.String,
		Latitude:           latitude.Float64,
		Longitude:          longitude.Float64,
		View:               view.Int64,
		Book:               book.Bool,
		CreatedAt:          createdAt.String,
		UpdatedAt:          updatedAt.String,
	}, nil
}

func (u *serviceRepo) GetList(ctx context.Context, req *models.ServiceGetListRequest) (*models.ServiceGetListResponse, error) {
	var (
		resp    = &models.ServiceGetListResponse{}
		query   string
		where   = " WHERE TRUE AND s.deleted_at is null"
		offset  = " OFFSET 0"
		limit   = " LIMIT 10"
		filter  = " ORDER BY s.created_at DESC"
		groupBy = " GROUP BY s.id  "
		sp      = NewServicePhotosRepo(u.db, u.log)
		a       = NewAmenityRepo(u.db, u.log)
	)

	query = `
		SELECT
			COUNT(*) OVER(),
		    s.id,
			s.category_id,
			s.name, 
			s.description,
			s.duration,
			s.thumbnail, 
			s.price,
			s.business_merchant_id, 
			s.address, 
			s.longitude, 
			s.latitude,
			s.view,
			s.book,
			TO_CHAR(s.created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(s.updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "services" AS s
		JOIN "business_merchants" AS bm ON s.business_merchant_id = bm.id
		
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += " AND (s.name ILIKE " + ")"
	}

	if req.Filter == "name" {
		filter = " ORDER BY s.name "
	}

	if len(req.MerchantId) > 0 {
		where += " AND bm.merchant_id = " + "'" + req.MerchantId + "'"
	}

	query += where + groupBy + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting service list", logger.Error(err))
		return nil, err
	}

	for rows.Next() {
		var (
			id                 sql.NullString
			category_id        sql.NullString
			name               sql.NullString
			description        sql.NullString
			duration           sql.NullFloat64
			thumbnail          sql.NullString
			price              sql.NullFloat64
			businessMerchantID sql.NullString
			address            sql.NullString
			longitude          sql.NullFloat64
			latitude           sql.NullFloat64
			view               sql.NullInt64
			book               sql.NullBool
			createdAt          sql.NullString
			updatedAt          sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&category_id,
			&name,
			&description,
			&duration,
			&thumbnail,
			&price,
			&businessMerchantID,
			&address,
			&longitude,
			&latitude,
			&view,
			&book,
			&createdAt,
			&updatedAt,
		)
		if err != nil {
			u.log.Error("error is while getting service list (scanning data)", logger.Error(err))
			return nil, err
		}

		url, err := sp.GetServicePhotos(context.Background(), &models.ServicePrimaryKey{Id: id.String})
		if err != nil && err.Error() != "no rows in result set" {
			u.log.Error("error is while getting service photos", logger.Error(err))
			return nil, err
		}

		amenity, err := a.GetListServiceAmenity(context.Background(), &models.ServicePrimaryKey{Id: id.String})
		if err != nil && err.Error() != "no rows in result set" {
			u.log.Error("error is while getting service amenities", logger.Error(err))
			return nil, err
		}

		resp.Services = append(resp.Services, &models.Service{
			Id:                 id.String,
			CategoryId:         category_id.String,
			Url:                url.Url,
			Amenity:            amenity.Amenity,
			Name:               name.String,
			Description:        description.String,
			Duration:           duration.Float64,
			Thumbnail:          thumbnail.String,
			Price:              price.Float64,
			BusinessMerchantID: businessMerchantID.String,
			Address:            address.String,
			Latitude:           latitude.Float64,
			Longitude:          longitude.Float64,
			View:               view.Int64,
			Book:               book.Bool,
			CreatedAt:          createdAt.String,
			UpdatedAt:          updatedAt.String,
		})
	}
	return resp, nil
}

func (u *serviceRepo) Recommendation(ctx context.Context, req *models.ServiceGetListRequest) (*models.ServiceGetListResponse, error) {
	var (
		resp   = &models.ServiceGetListResponse{}
		query  string
		where  = " WHERE TRUE AND s.deleted_at is null "
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY s.view DESC "
	)

	query = `
		SELECT
			COUNT(*) OVER(),
		    s.id,
			s.name, 
			s.thumbnail, 
			s.address, 
			s.latitude,
			s.longitude,
			s.view,
			s.book,
			TO_CHAR(s.created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(s.updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "services" AS s
		
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting service list", logger.Error(err))
		return nil, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			name      sql.NullString
			thumbnail sql.NullString
			address   sql.NullString
			latitude  sql.NullFloat64
			longitude sql.NullFloat64
			view      sql.NullInt64
			book      sql.NullBool
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&name,
			&thumbnail,
			&address,
			&latitude,
			&longitude,
			&view,
			&book,
			&createdAt,
			&updatedAt,
		)
		if err != nil {
			u.log.Error("error is while getting service list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Services = append(resp.Services, &models.Service{
			Id:        id.String,
			Name:      name.String,
			Thumbnail: thumbnail.String,
			Address:   address.String,
			View:      view.Int64,
			Book:      book.Bool,
			Latitude:  latitude.Float64,
			Longitude: longitude.Float64,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}
	return resp, nil
}

func (u *serviceRepo) GetListFalseServices(ctx context.Context, req *models.ServiceGetListRequest) (*models.ServiceGetListResponse, error) {
	var (
		resp    = &models.ServiceGetListResponse{}
		query   string
		where   = " WHERE TRUE AND s.deleted_at is null and book = false"
		offset  = " OFFSET 0"
		limit   = " LIMIT 10"
		filter  = " ORDER BY s.created_at DESC"
		groupBy = " GROUP BY s.id  "
		sp      = NewServicePhotosRepo(u.db, u.log)
		a       = NewAmenityRepo(u.db, u.log)
	)

	query = `
		SELECT
			COUNT(*) OVER(),
		    s.id,
			s.category_id,
			s.name, 
			s.description,
			s.duration,
			s.thumbnail, 
			s.price,
			s.business_merchant_id, 
			s.address, 
			s.longitude, 
			s.latitude,
			s.view,
			s.book,
			TO_CHAR(s.created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(s.updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "services" AS s
		JOIN "business_merchants" AS bm ON s.business_merchant_id = bm.id
		
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + groupBy + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting service list", logger.Error(err))
		return nil, err
	}

	for rows.Next() {
		var (
			id                 sql.NullString
			category_id        sql.NullString
			name               sql.NullString
			description        sql.NullString
			duration           sql.NullFloat64
			thumbnail          sql.NullString
			price              sql.NullFloat64
			businessMerchantID sql.NullString
			address            sql.NullString
			longitude          sql.NullFloat64
			latitude           sql.NullFloat64
			view               sql.NullInt64
			book               sql.NullBool
			createdAt          sql.NullString
			updatedAt          sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&category_id,
			&name,
			&description,
			&duration,
			&thumbnail,
			&price,
			&businessMerchantID,
			&address,
			&longitude,
			&latitude,
			&view,
			&book,
			&createdAt,
			&updatedAt,
		)
		if err != nil {
			u.log.Error("error is while getting service list (scanning data)", logger.Error(err))
			return nil, err
		}

		url, err := sp.GetServicePhotos(context.Background(), &models.ServicePrimaryKey{Id: id.String})
		if err != nil && err.Error() != "no rows in result set" {
			u.log.Error("error is while getting service photos", logger.Error(err))
			return nil, err
		}

		amenity, err := a.GetListServiceAmenity(context.Background(), &models.ServicePrimaryKey{Id: id.String})
		if err != nil && err.Error() != "no rows in result set" {
			u.log.Error("error is while getting service amenities", logger.Error(err))
			return nil, err
		}

		resp.Services = append(resp.Services, &models.Service{
			Id:                 id.String,
			CategoryId:         category_id.String,
			Url:                url.Url,
			Amenity:            amenity.Amenity,
			Name:               name.String,
			Description:        description.String,
			Duration:           duration.Float64,
			Thumbnail:          thumbnail.String,
			Price:              price.Float64,
			BusinessMerchantID: businessMerchantID.String,
			Address:            address.String,
			Latitude:           latitude.Float64,
			Longitude:          longitude.Float64,
			View:               view.Int64,
			Book:               book.Bool,
			CreatedAt:          createdAt.String,
			UpdatedAt:          updatedAt.String,
		})
	}
	return resp, nil
}

func (u *serviceRepo) GetListTrueServices(ctx context.Context, req *models.ServiceGetListRequest) (*models.ServiceGetListResponse, error) {
	var (
		resp    = &models.ServiceGetListResponse{}
		query   string
		where   = " WHERE TRUE AND s.deleted_at is null and book = true"
		offset  = " OFFSET 0"
		limit   = " LIMIT 10"
		filter  = " ORDER BY s.created_at DESC"
		groupBy = " GROUP BY s.id  "
		sp      = NewServicePhotosRepo(u.db, u.log)
		a       = NewAmenityRepo(u.db, u.log)
	)

	query = `
		SELECT
			COUNT(*) OVER(),
		    s.id,
			s.category_id,
			s.name, 
			s.description,
			s.duration,
			s.thumbnail, 
			s.price,
			s.business_merchant_id, 
			s.address, 
			s.longitude, 
			s.latitude,
			s.view,
			s.book,
			TO_CHAR(s.created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(s.updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "services" AS s
		JOIN "business_merchants" AS bm ON s.business_merchant_id = bm.id
		
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	query += where + groupBy + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting service list", logger.Error(err))
		return nil, err
	}

	for rows.Next() {
		var (
			id                 sql.NullString
			category_id        sql.NullString
			name               sql.NullString
			description        sql.NullString
			duration           sql.NullFloat64
			thumbnail          sql.NullString
			price              sql.NullFloat64
			businessMerchantID sql.NullString
			address            sql.NullString
			longitude          sql.NullFloat64
			latitude           sql.NullFloat64
			view               sql.NullInt64
			book               sql.NullBool
			createdAt          sql.NullString
			updatedAt          sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&category_id,
			&name,
			&description,
			&duration,
			&thumbnail,
			&price,
			&businessMerchantID,
			&address,
			&longitude,
			&latitude,
			&view,
			&book,
			&createdAt,
			&updatedAt,
		)
		if err != nil {
			u.log.Error("error is while getting service list (scanning data)", logger.Error(err))
			return nil, err
		}

		url, err := sp.GetServicePhotos(context.Background(), &models.ServicePrimaryKey{Id: id.String})
		if err != nil && err.Error() != "no rows in result set" {
			u.log.Error("error is while getting service photos", logger.Error(err))
			return nil, err
		}

		amenity, err := a.GetListServiceAmenity(context.Background(), &models.ServicePrimaryKey{Id: id.String})
		if err != nil && err.Error() != "no rows in result set" {
			u.log.Error("error is while getting service amenities", logger.Error(err))
			return nil, err
		}

		resp.Services = append(resp.Services, &models.Service{
			Id:                 id.String,
			CategoryId:         category_id.String,
			Url:                url.Url,
			Amenity:            amenity.Amenity,
			Name:               name.String,
			Description:        description.String,
			Duration:           duration.Float64,
			Thumbnail:          thumbnail.String,
			Price:              price.Float64,
			BusinessMerchantID: businessMerchantID.String,
			Address:            address.String,
			Latitude:           latitude.Float64,
			Longitude:          longitude.Float64,
			View:               view.Int64,
			Book:               book.Bool,
			CreatedAt:          createdAt.String,
			UpdatedAt:          updatedAt.String,
		})
	}
	return resp, nil
}

func (u *serviceRepo) Update(ctx context.Context, req *models.ServiceUpdate) (int64, error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"services"
		SET
			name = :name, 
			description = :description,
			duration = :duration, 
			price = :price,
			address = :address, 
			longitude = :longitude, 
			latitude = :latitude,
			view = :view,
			book = :book,
			thumbnail = :thumbnail,
			updated_at = NOW()
		WHERE id = :id
	`
	params = map[string]interface{}{
		"id":          req.Id,
		"name":        req.Name,
		"description": req.Description,
		"duration":    req.Duration,
		"price":       req.Price,
		"address":     req.Address,
		"longitude":   req.Longitude,
		"latitude":    req.Latitude,
		"view":        req.View,
		"book":        req.Book,
		"thumbnail":   req.Thumbnail,
	}

	query, args := helper.ReplaceQueryParams(query, params)
	result, err := u.db.Exec(ctx, query, args...)
	if err != nil {
		u.log.Error("error is while updating service data", logger.Error(err))
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (u *serviceRepo) Delete(ctx context.Context, req *models.ServicePrimaryKey) error {
	_, err := u.db.Exec(ctx, `UPDATE "services" set deleted_at = now() WHERE id = $1`, req.Id)
	if err != nil {
		u.log.Error("error is while deleting service", logger.Error(err))
		return err
	}

	return nil
}
