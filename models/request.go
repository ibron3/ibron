package models

type Request struct {
	Id         string   `json:"id"`
	ServiceID  string   `json:"service_id"`
	UserID     string   `json:"user_id"`
	MerchantID string   `json:"merchant_id"`
	ClientID   string   `json:"client_id"`
	UserName   string   `json:"user_name"`
	StartTime  string   `json:"start_time"`
	EndTime    string   `json:"end_time"`
	Price      float64  `json:"price"`
	Status     string   `json:"status"`
	Date       string   `json:"date"`
	Day        string   `json:"day"`
	Service    *Service `json:"service"`
	Duration   int      `json:"duration,omitempty"`
	CreatedAt  string   `json:"created_at"`
	UpdatedAt  string   `json:"updated_at,omitempty"`
}

type RequestCreate struct {
	ServiceID  string  `json:"service_id"`
	UserID     string  `json:"user_id"`
	ClientID   string  `json:"client_id"`
	MerchantID string  `json:"-"`
	StartTime  string  `json:"start_time"`
	EndTime    string  `json:"end_time"`
	Price      float64 `json:"price"`
	Status     string  `json:"status"`
	Date       string  `json:"date"`
}

type RequestUpdate struct {
	Id        string  `json:"id"`
	ServiceID string  `json:"service_id"`
	UserID    string  `json:"user_id"`
	ClientID  string  `json:"client_id"`
	StartTime string  `json:"start_time"`
	EndTime   string  `json:"end_time"`
	Price     float64 `json:"price"`
	Date      string  `json:"date"`
	Status    string  `json:"status"`
}

type CheckRequest struct {
	Time      string `json:"time"`
	ServiceId string `json:"service_id"`
	Date      string `json:"date"`
}

type RequestPrimaryKey struct {
	Id string `json:"id"`
}

type RequestGetListRequest struct {
	Offset     int    `json:"offset"`
	Limit      int    `json:"limit"`
	ServiceId  string `json:"service_id"`
	UserId     string `json:"user_id"`
	Type       string `json:"type"`
	MerchantID string `json:"merchant_id"`
	Date       string `json:"date"`
}

type RequestGetListResponse struct {
	Count    int        `json:"count"`
	Requests []*Request `json:"requests"`
}

type UpdateStatus struct {
	ID         string `json:"-"`
	Status     string `json:"-"`
	MerchantID string `json:"merchant_id"`
}

type GetByUserID struct {
	UserID string `json:"user_id"`
}

type ReportResponse struct {
	Count         int        `json:"count"`
	FinishedCount int        `json:"finished_count"`
	Sum           int        `json:"sum"`
	RequestList   []*Request `json:"requests"`
	Labels        []*string  `json:"labels,omitempty"`
	Data          []*float64 `json:"data,omitempty"`
}

type ReportList struct {
	Month  string  `json:"month"`
	Data   float64 `json:"data"`
	Result float64 `json:"result"`
}

type ReportDiagram struct {
	ReportList    []*ReportList `json:"report_list"`
	TotalSum      float64       `json:"total_sum,omitempty"`
	Count         int           `json:"count,omitempty"`
	FinishedCount int           `json:"finished_count,omitempty"`
}
