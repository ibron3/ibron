package postgres

import (
	"context"
	"database/sql"
	"ibron/models"
	"ibron/pkg/helper"
	"ibron/pkg/logger"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type clientRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewClientRepo(db *pgxpool.Pool, log logger.LoggerI) *clientRepo {
	return &clientRepo{
		db:  db,
		log: log,
	}
}

func (u *clientRepo) Create(ctx context.Context, req *models.ClientCreate) (*models.Client, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO "clients"(id, merchant_id, first_name, last_name, phone_number)
		VALUES ($1, $2, $3, $4, $5) 
	`

	if _, err := u.db.Exec(ctx, query,
		id,
		req.MerchantID,
		req.FirstName,
		req.LastName,
		req.PhoneNumber,
	); err != nil {
		u.log.Error("insert error" + err.Error())
		return nil, err
	}

	resp, err := u.GetByID(context.Background(), &models.ClientPrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id client" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *clientRepo) GetByID(ctx context.Context, req *models.ClientPrimaryKey) (*models.Client, error) {
	var (
		query string
		where = " deleted_at is NULL "

		id          sql.NullString
		merchantID  sql.NullString
		firstName   sql.NullString
		lastName    sql.NullString
		phoneNumber sql.NullString
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	query = `
		SELECT 
			id,
			merchant_id, 
			first_name, 
			last_name, 
			phone_number,
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "clients" 

	`

	if len(req.Id) > 0 {
		where += " id = " + "'" + req.Id + "'"
	} else if len(req.PhoneNumber) > 0 {
		where += " phone_number = " + "'" + req.PhoneNumber + "'" + " AND merchant_id =" + "'" + req.MerchantId + "'"
	}

	err := u.db.QueryRow(ctx, query).Scan(
		&id,
		&merchantID,
		&firstName,
		&lastName,
		&phoneNumber,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		u.log.Error("error while scanning data" + err.Error())
		return nil, err
	}

	return &models.Client{
		Id:          id.String,
		MerchantID:  merchantID.String,
		FirstName:   firstName.String,
		LastName:    lastName.String,
		PhoneNumber: phoneNumber.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}, nil
}

func (u *clientRepo) GetList(ctx context.Context, req *models.ClientGetListRequest) (*models.ClientGetListResponse, error) {
	var (
		resp  = &models.ClientGetListResponse{}
		query string
		where = " WHERE TRUE AND deleted_at is null"
		// offset  = " OFFSET 0"
		// limit   = " LIMIT 10"
		filter  = " ORDER BY c.created_at DESC"
		groupBy = " GROUP BY c.id  "
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			merchant_id, 
			first_name, 
			last_name, 
			phone_number,
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "clients" AS c
	`

	// if req.Offset > 0 {
	// 	offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	// }

	// if req.Limit > 0 {
	// 	limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	// }

	if len(req.Search) > 0 {
		where += " AND first_name ILIKE " + "'%" + req.Search + "%'  OR last_name ILIKE " + "'%" + req.Search + "%'"
	}

	query += where + groupBy + filter
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting client list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			merchantID  sql.NullString
			firstName   sql.NullString
			lastName    sql.NullString
			phoneNumber sql.NullString
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&merchantID,
			&firstName,
			&lastName,
			&phoneNumber,
			&createdAt,
			&updatedAt,
		)
		if err != nil {
			u.log.Error("error is while getting client list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Clients = append(resp.Clients, &models.Client{
			Id:          id.String,
			MerchantID:  merchantID.String,
			FirstName:   firstName.String,
			LastName:    lastName.String,
			PhoneNumber: phoneNumber.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}
	return resp, nil
}

func (u *clientRepo) GetListByMerchantID(ctx context.Context, req *models.ClientGetListRequest) (*models.ClientGetListResponse, error) {
	var (
		resp    = &models.ClientGetListResponse{}
		query   string
		where   = " WHERE TRUE AND deleted_at is null AND merchant_id = " + "'" + req.MerchantID + "'"
		filter  = " ORDER BY created_at DESC"
		groupBy = " GROUP BY id  "
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			merchant_id, 
			first_name, 
			last_name, 
			phone_number,
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "clients"
	`

	query += where + groupBy + filter
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting client list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			merchantID  sql.NullString
			firstName   sql.NullString
			lastName    sql.NullString
			phoneNumber sql.NullString
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&merchantID,
			&firstName,
			&lastName,
			&phoneNumber,
			&createdAt,
			&updatedAt,
		)
		if err != nil {
			u.log.Error("error is while getting client list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.Clients = append(resp.Clients, &models.Client{
			Id:          id.String,
			MerchantID:  merchantID.String,
			FirstName:   firstName.String,
			LastName:    lastName.String,
			PhoneNumber: phoneNumber.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}
	return resp, nil
}

func (u *clientRepo) Update(ctx context.Context, req *models.ClientUpdate) (int64, error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"clients"
		SET
			merchant_id = :merchant_id, 
			first_name = :first_name, 
			last_name = :last_name, 
			phone_number = :phone_number,
			updated_at = NOW()
		WHERE id = :id
	`
	params = map[string]interface{}{
		"id":           req.Id,
		"merchant_id":  req.MerchantID,
		"first_name":   req.FirstName,
		"last_name":    req.LastName,
		"phone_number": req.PhoneNumber,
	}

	query, args := helper.ReplaceQueryParams(query, params)
	result, err := u.db.Exec(ctx, query, args...)
	if err != nil {
		u.log.Error("error is while updating client data", logger.Error(err))
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (u *clientRepo) Delete(ctx context.Context, req *models.ClientPrimaryKey) error {
	_, err := u.db.Exec(ctx, `UPDATE clients set deleted_at = now()  WHERE id = $1`, req.Id)
	if err != nil {
		u.log.Error("error is while deleting clients", logger.Error(err))
		return err
	}

	return nil
}
