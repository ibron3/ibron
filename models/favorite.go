package models

type Favorite struct {
	UserId    string `json:"user_id,omitempty"`
	ServiceId string `json:"service_id,omitempty"`
	CreatedAt string `json:"created_at,omitempty"`
}

type FavoriteCreate struct {
	UserId    string `json:"user_id"`
	ServiceId string `json:"service_id"`
}

type FavoritePrimaryKey struct {
	Id string `json:"id"`
}

type DeleteFavorite struct{
	UserId    string `json:"user_id"`
	ServiceId string `json:"service_id"`
}

type FavoriteGetListRequest struct {
	Offset int    `json:"offset"`
	Limit  int    `json:"limit"`
	Filter string `json:"filter"`
	Search string `json:"search"`
}

type FavoriteGetListResponse struct {
	Count    int         `json:"count"`
	Favorite []*Favorite `json:"Favorites"`
}
