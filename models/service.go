package models

type Service struct {
	Id                 string          `json:"id,omitempty"`
	CategoryId         string          `json:"category_id,omitempty"`
	BusinessMerchantID string          `json:"business_merchant_id,omitempty"`
	Name               string          `json:"name,omitempty"`
	Description        string          `json:"description"`
	Duration           float64         `json:"duration,omitempty"`
	Price              float64         `json:"price,omitempty"`
	Address            string          `json:"address,omitempty"`
	Latitude           float64         `json:"latitude,omitempty"`
	Longitude          float64         `json:"longitude,omitempty"`
	Distance           float64         `json:"distance"`
	Url                []*Url          `json:"url,omitempty"`
	Thumbnail          string          `json:"thumbnail,omitempty"`
	Amenity            []*Amenity      `json:"amenities,omitempty"`
	WorkingHours       []*WorkingHours `json:"working_hours,omitempty"`
	View               int64           `json:"view"`
	Book               bool            `json:"book"`
	CreatedAt          string          `json:"created_at,omitempty"`
	UpdatedAt          string          `json:"updated_at,omitempty"`
}

type ServiceCreate struct {
	MerchantId         string       `json:"merchant_id"`
	CategoryId         string       `json:"category_id"`
	BusinessMerchantID string       `json:"business_merchant_id"`
	Name               string       `json:"name"`
	Description        string       `json:"description"`
	Duration           float64      `json:"duration"`
	Price              float64      `json:"price"`
	Address            string       `json:"address"`
	Latitude           float64      `json:"latitude"`
	Longitude          float64      `json:"longitude"`
	Url                []*Url       `json:"url"`
	Thumbnail          string       `json:"thumbnail"`
	Amenity            []*Amenities `json:"amenities"`
	Days               []*Days      `json:"day"`
	Book               bool         `json:"-"`
}

type ServiceUpdate struct {
	Id          string  `json:"id"`
	Name        string  `json:"name"`
	Description string  `json:"description"`
	Duration    float64 `json:"duration"`
	Price       float64 `json:"price"`
	Address     string  `json:"-"`
	Latitude    float64 `json:"-"`
	Longitude   float64 `json:"-"`
	View        int64   `json:"-"`
	Urls        []*Url  `json:"urls"`
	Days        []*Days `json:"day"`
	Thumbnail   string  `json:"thumbnail"`
	Book        bool    `json:"book"`
}

type Days struct {
	Day       string `json:"day"`
	StartTime string `json:"start_time"`
	EndTime   string `json:"end_time"`
}

type ServicePrimaryKey struct {
	Id string `json:"id"`
}

type ServiceGetListRequest struct {
	Offset     int     `json:"offset"`
	Limit      int     `json:"limit"`
	Filter     string  `json:"filter"`
	Search     string  `json:"search"`
	MerchantId string  `json:"merchant_id"`
	Longitude  float64 `json:"longitude"`
	Latitude   float64 `json:"latitude"`
}

type ServiceGetListResponse struct {
	Count    int        `json:"count"`
	Services []*Service `json:"services"`
}

type Urls struct {
	Url []*Url `json:"url"`
}

type Amenities struct {
	AmenityId string `json:"amenity_id"`
}
