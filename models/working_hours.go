package models

type WorkingHours struct {
	Id                 string `json:"id,omitempty"`
	BusinessMerchantId string `json:"business_merchant_id,omitempty"`
	ServiceId          string `json:"service_id,omitempty"`
	Day                string `json:"day,omitempty"`
	StartTime          string `json:"start_time,omitempty"`
	EndTime            string `json:"end_time,omitempty"`
	CreatedAt          string `json:"created_at,omitempty"`
	UpdatedAt          string `json:"updated_at,omitempty"`
}

type WorkingHoursCreate struct {
	BusinessMerchantId string `json:"business_merchant_id"`
	ServiceId          string `json:"service_id"`
	Day                string `json:"day"`
	StartTime          string `json:"start_time"`
	EndTime            string `json:"end_time"`
}

type FreeTime struct {
	StartTime string `json:"start_time"`
	EndTime   string `json:"end_time"`
	Date      string `json:"date"`
	ServiceId string `json:"service_id"`
}

type Time struct {
	StartTime string   `json:"start_time"`
	EndTime   string   `json:"end_time"`
	Status    bool     `json:"status"`
	Request   *Request `json:"request,omitempty"`
}

type FreeTimeResponse struct {
	Count int     `json:"count"`
	Times []*Time `json:"free_times"`
}

type WorkingHoursUpdate struct {
	Id        string `json:"-"`
	ServiceID string `json:"-"`
	Day       string `json:"day"`
	StartTime string `json:"start_time"`
	EndTime   string `json:"end_time"`
}

type WorkingHoursPrimaryKey struct {
	Id        string `json:"id"`
	ServiceId string `json:"service_id"`
	Day       string `json:"day"`
}

type WorkingHoursGetListRequest struct {
	Offset    int    `json:"offset"`
	Limit     int    `json:"limit"`
	Filter    string `json:"filter"`
	Search    string `json:"search"`
	ServiceId string `json:"service_id"`
}

type WorkingHoursGetListResponse struct {
	Count        int             `json:"count"`
	WorkingHours []*WorkingHours `json:"working_hours"`
}
