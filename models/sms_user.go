package models

type Sms struct {
	SenderID  string `json:"sender_id"`
	Name      string `json:"name"`
	Count     int64  `json:"count"`
	CreatedAt string `json:"created_at"`
}

type SmsCreate struct {
	SenderID string `json:"sender_id"`
	Name     string `json:"name"`
	Count    int64  `json:"count"`
}

type SmsPrimaryKey struct {
	SenderID string `json:"sender_id"`
}
