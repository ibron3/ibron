package models

type ServiceAmenity struct {
	Id        string `json:"id"`
	ServiceId string `json:"service_id"`
	AmenityId string `json:"amenity_id"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

type ServiceAmenityCreate struct {
	ServiceId string `json:"service_id"`
	AmenityId string `json:"amenity_id"`
}

type ServiceAmenityUpdate struct {
	Id        string `json:"id"`
	ServiceId string `json:"service_id"`
	AmenityId string `json:"amenity_id"`
}

type ServiceAmenityPrimaryKey struct {
	Id string `json:"id"`
}

type ServiceAmenityGetListRequest struct {
	Offset int    `json:"offset"`
	Limit  int    `json:"limit"`
	Filter string `json:"filter"`
	Search string `json:"search"`
}

type ServiceAmenityGetListResponse struct {
	Count          int               `json:"count"`
	ServiceAmenity []*ServiceAmenity `json:"service_amenities"`
}
