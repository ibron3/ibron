package handler

import (
	"fmt"
	"ibron/models"
	"ibron/pkg/helper"
	"ibron/pkg/jwt"
	"ibron/pkg/security"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Create Merchants godoc
// @ID create_merchants
// @Router /ibron/api/v1/merchant [POST]
// @Summary Create Merchants
// @Description Create Merchants
// @Tags Merchant
// @Accept json
// @Procedure json
// @Param user body models.MerchantCreate true "CreateMerchantRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) CreateMerchant(c *gin.Context) {
	var (
		createMerchant models.MerchantCreate
	)

	err := c.ShouldBindJSON(&createMerchant)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Merchant Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	if !helper.IsValidPhone(createMerchant.PhoneNumber) {
		h.logger.Error("Invalid Phone Number!")
		c.JSON(http.StatusBadRequest, models.ErrorMessage{Phone: "Invalid Phone Number"})
		return
	}

	resp, err := h.storage.Merchant().Create(c.Request.Context(), &createMerchant)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Merchant.Create")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Create Merchant Successfully!!")
	c.JSON(http.StatusCreated, resp)
}

// GetByID Merchants godoc
// @ID get_by_id_merchants
// @Router /ibron/api/v1/merchant/{id} [GET]
// @Summary Get By ID Merchants
// @Description Get By ID Merchants
// @Tags Merchant
// @Accept json
// @Procedure json
// @Param id path string false "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetByIdMerchant(c *gin.Context) {
	id := c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	user, err := h.storage.Merchant().GetByID(c.Request.Context(), &models.MerchantPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Merchant.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetByID Merchant Response!")
	c.JSON(http.StatusOK, user)
}

// GetList Merchants godoc
// @ID get_list_merchants
// @Router /ibron/api/v1/merchants [GET]
// @Summary Get List Merchants
// @Description Get List Merchants
// @Tags Merchant
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param filter query string false "filter"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) GetListMerchant(c *gin.Context) {
	offset, err := h.getOffsetQuery(c.Query("offset"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListUser INVALID OFFSET!")
		c.JSON(http.StatusBadRequest, "INVALID OFFSET")
		return
	}

	limit, err := h.getLimitQuery(c.Query("limit"))
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "GetListUser INVALID LIMIT!")
		c.JSON(http.StatusBadRequest, "INVALID LIMIT")
		return
	}

	filter := c.Query("filter")
	search := c.Query("search")

	resp, err := h.storage.Merchant().GetList(c.Request.Context(), &models.MerchantGetListRequest{
		Offset: offset,
		Limit:  limit,
		Filter: filter,
		Search: search,
	})

	if err != nil && err.Error() != "no rows in result set" {
		h.logger.Error(err.Error() + "  :  " + "storage.Merchant.GetList!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("GetListMerchant Response!")
	c.JSON(http.StatusOK, resp)
}

// Update Merchants godoc
// @ID update_merchants
// @Router /ibron/api/v1/merchant/{id} [PUT]
// @Summary Update Merchants
// @Description Update Merchants
// @Tags Merchant
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Branch body models.MerchantUpdate true "UpdateMerchantRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) UpdateMerchant(c *gin.Context) {
	var (
		id             = c.Param("id")
		updateMerchant models.MerchantUpdate
	)

	if !helper.IsValidUUID(id) {
		h.logger.Error("is invalid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id")
		return
	}

	err := c.ShouldBindJSON(&updateMerchant)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Merchant Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	updateMerchant.Id = id
	rowsAffected, err := h.storage.Merchant().Update(c.Request.Context(), &updateMerchant)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Merchant.Update!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	if rowsAffected <= 0 {
		h.logger.Error("storage.Merchant.Update!")
		c.JSON(http.StatusBadRequest, "Unable to update data. Please try again later!")
		return
	}

	resp, err := h.storage.Merchant().GetByID(c.Request.Context(), &models.MerchantPrimaryKey{Id: updateMerchant.Id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Merchant.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}

	h.logger.Info("Update Merchant Successfully!")
	c.JSON(http.StatusAccepted, resp)
}

// Delete Merchants godoc
// @ID delete_merchants
// @Router /ibron/api/v1/merchant/{id} [DELETE]
// @Summary Delete Merchants
// @Description Delete Merchants
// @Tags Merchant
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) DeleteMerchant(c *gin.Context) {
	var id = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.logger.Error("is not valid uuid!")
		c.JSON(http.StatusBadRequest, "invalid id!")
		return
	}

	err := h.storage.Merchant().Delete(c.Request.Context(), &models.MerchantPrimaryKey{Id: id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Merchant.Delete!")
		c.JSON(http.StatusInternalServerError, "Unable to delete data, please try again later!")
		return
	}

	h.logger.Info("Merchant Deleted Successfully!")
	c.JSON(http.StatusNoContent, nil)
}

// Merchant Login godoc
// @ID login_merchants
// @Router /ibron/api/v1/merchant/login [POST]
// @Summary Login Merchants
// @Description Login Merchants
// @Tags Merchant
// @Accept json
// @Procedure json
// @Param login body models.MerchantLoginRequest true "login"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *handler) MerchantLogin(c *gin.Context) {
	loginRequest := models.MerchantLoginRequest{}

	if err := c.ShouldBindJSON(&loginRequest); err != nil {
		h.logger.Error(err.Error() + "  :  " + "error Merchant Should Bind Json!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	merchantData, err := h.storage.Merchant().GetMerchantCredentials(c.Request.Context(), &loginRequest)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Merchant.Login!")
		c.JSON(http.StatusInternalServerError, "Unable to login, please try again later!")
		return
	}

	isTrue := security.CompareHashAndPassword(merchantData.Password, loginRequest.Password)
	if !isTrue {
		fmt.Println("request pass", loginRequest.Password, " merchant pass", merchantData.Password)
		h.logger.Error("error Merchant password incorrect!")
		c.JSON(http.StatusBadRequest, "Please, Enter Valid Data!")
		return
	}

	m := make(map[interface{}]interface{})

	m["merchant_id"] = merchantData.Id
	accessToken, refreshToken, err := jwt.GenerateJWT(m)
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "error jwt.GenerateJWT!")
		c.JSON(http.StatusInternalServerError, "Unable generate token!")
		return
	}

	merchant, err := h.storage.Merchant().GetByID(c.Request.Context(), &models.MerchantPrimaryKey{Id: merchantData.Id})
	if err != nil {
		h.logger.Error(err.Error() + "  :  " + "storage.Merchant.GetByID!")
		c.JSON(http.StatusInternalServerError, "Server Error!")
		return
	}
	merchant.Password = ""
	merchant.Login = ""

	resp := models.LoginResponse{
		Merchant:     merchant,
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}

	h.logger.Info("Merchant login Successfully!")
	c.JSON(http.StatusAccepted, resp)
}
