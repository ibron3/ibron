package models

type Schedule struct {
	Id        string `json:"id"`
	ServiceID string `json:"service_id"`
	Day       string `json:"day"`
	StartTime string `json:"start_time"`
	EndTime   string `json:"end_time"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

type ScheduleCreate struct {
	ServiceID string `json:"service_id"`
	Day       string `json:"day"`
	StartTime string `json:"start_time"`
	EndTime   string `json:"end_time"`
}

type ScheduleUpdate struct {
	Id        string `json:"id"`
	ServiceID string `json:"service_id"`
	Day       string `json:"day"`
	StartTime string `json:"start_time"`
	EndTime   string `json:"end_time"`
}

type SchedulePrimaryKey struct {
	Id string `json:"id"`
}

type ScheduleGetListRequest struct {
	Offset    int    `json:"offset"`
	Limit     int    `json:"limit"`
	ServiceId string `json:"service_id"`
}

type ScheduleGetListResponse struct {
	Count    int         `json:"count"`
	Schedule []*Schedule `json:"schedule"`
}
