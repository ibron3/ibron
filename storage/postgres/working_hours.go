package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"ibron/models"
	"ibron/pkg/helper"
	"ibron/pkg/logger"

	uuid "github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type workingHoursRepo struct {
	db  *pgxpool.Pool
	log logger.LoggerI
}

func NewWorkingHoursRepo(db *pgxpool.Pool, log logger.LoggerI) *workingHoursRepo {
	return &workingHoursRepo{
		db:  db,
		log: log,
	}
}

func (u *workingHoursRepo) Create(ctx context.Context, req *models.WorkingHoursCreate) (*models.WorkingHours, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	if len(req.BusinessMerchantId) > 0 {
		query = `
			INSERT INTO "working_hours"(id, business_merchant_id,service_id,day,start_time,end_time)
			VALUES ($1, $2,$3,$4,$5,$6) 
		`
		_, err := u.db.Exec(ctx, query,
			id,
			req.BusinessMerchantId,
			req.ServiceId,
			req.Day,
			req.StartTime,
			req.EndTime,
		)

		if err != nil {
			u.log.Error("insert error" + err.Error())
			return nil, err
		}

	}

	if len(req.ServiceId) > 0 {
		query = `
			INSERT INTO "working_hours"(id, service_id,day,start_time,end_time)
			VALUES ($1, $2,$3,$4,$5) 
		`
		_, err := u.db.Exec(ctx, query,
			id,
			req.ServiceId,
			req.Day,
			req.StartTime,
			req.EndTime,
		)

		if err != nil {
			u.log.Error("insert error" + err.Error())
			return nil, err
		}
	}

	resp, err := u.GetByID(context.Background(), &models.WorkingHoursPrimaryKey{Id: id})
	if err != nil {
		u.log.Error("error get by id working hours" + err.Error())
		return nil, err
	}

	return resp, nil
}

func (u *workingHoursRepo) GetByID(ctx context.Context, req *models.WorkingHoursPrimaryKey) (*models.WorkingHours, error) {
	var (
		query string

		id                   sql.NullString
		service_id           sql.NullString
		business_merchant_id sql.NullString
		day                  sql.NullString
		start_time           sql.NullString
		end_time             sql.NullString
		createdAt            sql.NullString
		updatedAt            sql.NullString
	)

	if len(req.Id) > 0 {
		query = `
			SELECT 
				id,
				service_id,
				business_merchant_id,
				day,
				TO_CHAR(start_time,'HH24:MI'), 
				TO_CHAR(end_time,'HH24:MI'),
				TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
				TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
			FROM "working_hours" 
			WHERE id = $1

		`
		err := u.db.QueryRow(ctx, query, req.Id).Scan(
			&id,
			&service_id,
			&business_merchant_id,
			&day,
			&start_time,
			&end_time,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			u.log.Error("error while scanning working hours data" + err.Error())
			return nil, err
		}

	} else if len(req.ServiceId) > 0 && len(req.Day) > 0 {
		query = `
			SELECT 
				id,
				service_id,
				business_merchant_id,
				day,
				TO_CHAR(start_time,'HH24:MI'), 
				TO_CHAR(end_time,'HH24:MI'),
				TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
				TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
			FROM "working_hours" 
			WHERE service_id = $1 AND day = $2

		`
		err := u.db.QueryRow(ctx, query, req.ServiceId, req.Day).Scan(
			&id,
			&service_id,
			&business_merchant_id,
			&day,
			&start_time,
			&end_time,
			&createdAt,
			&updatedAt,
		)

		if err != nil && err.Error() != "no rows in result set" {
			u.log.Error("error while scanning working hours data" + err.Error())
			return nil, err
		}

	}

	return &models.WorkingHours{
		Id:                 id.String,
		ServiceId:          service_id.String,
		BusinessMerchantId: business_merchant_id.String,
		Day:                day.String,
		StartTime:          start_time.String,
		EndTime:            end_time.String,
		CreatedAt:          createdAt.String,
		UpdatedAt:          updatedAt.String,
	}, nil
}

func (u *workingHoursRepo) GetList(ctx context.Context, req *models.WorkingHoursGetListRequest) (*models.WorkingHoursGetListResponse, error) {
	var (
		resp   = &models.WorkingHoursGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			service_id,
			business_merchant_id,
			day,
			TO_CHAR(start_time, 'HH24:MI'), 
			TO_CHAR(end_time, 'HH24:MI'),
			TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
			TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
		FROM "working_hours"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if len(req.ServiceId) > 0 {
		where += " AND service_id = " + "'" + req.ServiceId + "'"
	}

	query += where + filter + offset + limit
	fmt.Println("query", query)
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting working hours list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			id                   sql.NullString
			service_id           sql.NullString
			business_merchant_id sql.NullString
			day                  sql.NullString
			start_time           sql.NullString
			end_time             sql.NullString
			createdAt            sql.NullString
			updatedAt            sql.NullString
		)

		err = rows.Scan(
			&resp.Count,
			&id,
			&service_id,
			&business_merchant_id,
			&day,
			&start_time,
			&end_time,
			&createdAt,
			&updatedAt,
		)
		if err != nil {
			u.log.Error("error is while getting banner list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.WorkingHours = append(resp.WorkingHours, &models.WorkingHours{
			Id:                 id.String,
			ServiceId:          service_id.String,
			BusinessMerchantId: business_merchant_id.String,
			Day:                day.String,
			StartTime:          start_time.String,
			EndTime:            end_time.String,
			CreatedAt:          createdAt.String,
			UpdatedAt:          updatedAt.String,
		})
	}
	return resp, nil
}

func (u *workingHoursRepo) GetListByServiceId(ctx context.Context, req *models.WorkingHoursGetListRequest) (*models.WorkingHoursGetListResponse, error) {
	var (
		resp   = &models.WorkingHoursGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		filter = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			day,
			TO_CHAR(start_time, 'HH24:MI'), 
			TO_CHAR(end_time, 'HH24:MI')
		FROM "working_hours"
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if len(req.ServiceId) > 0 {
		where += " AND service_id = " + "'" + req.ServiceId + "'"
	}

	query += where + filter + offset + limit
	rows, err := u.db.Query(ctx, query)
	if err != nil {
		u.log.Error("error is while getting working hours list" + err.Error())
		return nil, err
	}

	for rows.Next() {
		var (
			day        sql.NullString
			start_time sql.NullString
			end_time   sql.NullString
		)

		err = rows.Scan(
			&day,
			&start_time,
			&end_time,
		)
		if err != nil {
			u.log.Error("error is while getting banner list (scanning data)", logger.Error(err))
			return nil, err
		}

		resp.WorkingHours = append(resp.WorkingHours, &models.WorkingHours{
			Day:       day.String,
			StartTime: start_time.String,
			EndTime:   end_time.String,
		})
	}
	return resp, nil
}

func (u *workingHoursRepo) UpdateServiceTime(ctx context.Context, req *models.WorkingHoursUpdate) (int64, error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"working_hours"
		SET
			start_time = :start_time,
			end_time = :end_time,
			updated_at = NOW()
		WHERE id = :id
	`
	params = map[string]interface{}{
		"service_id": req.ServiceID,
		"start_time": req.StartTime,
		"end_time":   req.EndTime,
		"day":        req.Day,
	}

	query, args := helper.ReplaceQueryParams(query, params)
	result, err := u.db.Exec(ctx, query, args...)
	if err != nil {
		u.log.Error("error is while updating working hours data", logger.Error(err))
		return 0, err
	}

	return result.RowsAffected(), nil
}

func (u *workingHoursRepo) Delete(ctx context.Context, req *models.WorkingHoursPrimaryKey) error {

	_, err := u.db.Exec(ctx, `DELETE FROM working_hours  WHERE service_id = $1`, req.ServiceId)
	if err != nil {
		u.log.Error("error is while deleting working hours", logger.Error(err))
		return err
	}

	return nil
}

func (u *workingHoursRepo) GetFreeTimeByServiceId(ctx context.Context, req *models.FreeTime) (bool, error) {

	var (
		id sql.NullString
	)

	query := `
		SELECT
			id
		FROM requests
		WHERE date = $1 AND service_id = $2 AND $3 BETWEEN TO_CHAR(start_time, 'HH24:MI') AND TO_CHAR(end_time, 'HH24:MI')  AND $4 BETWEEN TO_CHAR(start_time, 'HH24:MI') AND TO_CHAR(end_time, 'HH24:MI') AND deleted_at is null AND status = 'approved'
	`

	err := u.db.QueryRow(ctx, query, req.Date, req.ServiceId, req.StartTime, req.EndTime).Scan(&id)
	if err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error while scanning free time data" + err.Error())
		return false, err
	}

	if id.String != "" {
		return false, nil
	}

	return true, nil
}

func (u *workingHoursRepo) GetByDate(ctx context.Context, req *models.WorkingHoursPrimaryKey) (*models.WorkingHours, error) {
	var (
		id                 sql.NullString
		serviceID          sql.NullString
		businessMerchantID sql.NullString
		day                sql.NullString
		startTime          sql.NullString
		endTime            sql.NullString
		createdAt          sql.NullString
		updatedAt          sql.NullString
	)

	query := `SELECT 
				id,
				service_id,
				business_merchant_id,
				day,
				TO_CHAR(start_time,'HH24:MI'), 
				TO_CHAR(end_time,'HH24:MI'),
				TO_CHAR(created_at, 'dd/mm/yyyy HH24:MI:SS'), 
				TO_CHAR(updated_at, 'dd/mm/yyyy HH24:MI:SS')
			FROM "working_hours" 
			WHERE service_id = $1 AND day = $2`

	if err := u.db.QueryRow(ctx, query, req.ServiceId, req.Day).Scan(
		&id,
		&serviceID,
		&businessMerchantID,
		&day,
		&startTime,
		&endTime,
		&createdAt,
		&updatedAt,
	); err != nil && err.Error() != "no rows in result set" {
		u.log.Error("error is while getting working hours by day and service id" + err.Error())
		return nil, err
	}
	return &models.WorkingHours{
		Id:                 id.String,
		BusinessMerchantId: businessMerchantID.String,
		ServiceId:          serviceID.String,
		Day:                day.String,
		StartTime:          startTime.String,
		EndTime:            endTime.String,
		CreatedAt:          createdAt.String,
		UpdatedAt:          updatedAt.String,
	}, nil
}
